import React, { Component } from 'react';
import styled from 'styled-components';
import connect from "react-redux/es/connect/connect";
import store from "../../reducers";

class ApplicationsAddCustomerNewFiz extends Component {
  constructor(props) {
    super(props);
    this.state = {data: {}};
    this.inputChange = this.inputChange.bind(this);
  }
  inputChange(e) {
      let storeCustomer = this.props.applications.applicationsAddCustomer;
      storeCustomer.new[e.target.name] = e.target.value;
      store.dispatch({ type: 'APPLICATIONS_ADD_CUSTOMER', applicationsAddCustomer: storeCustomer });
  }
  render() {
    return (
      <Wrapper>
        <Container>
            <InputCont>
                <InputLabel>ФИО</InputLabel>
                <StyledInput onChange={this.inputChange} required name='name' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>Телефон</InputLabel>
                <StyledInput onChange={this.inputChange} name='phone' type='tel'/>
            </InputCont>
            <InputCont>
                <InputLabel>Почта</InputLabel>
                <StyledInput onChange={this.inputChange} name='mail' type='email'/>
            </InputCont>
            <InputCont>
                <InputLabel>Примечания</InputLabel>
                <StyledInput style={{width: '272px'}} onChange={this.inputChange} name='other' type='text'/>
            </InputCont>
        </Container>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
`;
const Container = styled.div`
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;

const mapStateToProps = (store) => {return {applications: store.applications}};
export default connect(mapStateToProps)(ApplicationsAddCustomerNewFiz);
