import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from "react-redux";
import store from '../../reducers';

class ApplicationsDetailToExecData extends Component {
    constructor(props) {
        super(props);
        this.state = {lookRequiredDate: '', lookRequiredTime: '', lookRequiredDateTime: ''};
        this.inputChange = this.inputChange.bind(this);
        this.setDate = this.setDate.bind(this);
        this.setTime = this.setTime.bind(this);
        this.setDateTime = this.setDateTime.bind(this);
    }
    inputChange(e) {
        let data = this.props.applications.applicationsDetailToExecData;
        data[e.target.name] = e.target.value;
        store.dispatch({type: 'APPLICATIONS_DETAIL_TO_EXEC_DATA', applicationsDetailToExecData: data});
    }
    setDate(e) {
        let value = e.target.value;
        this.setState({lookRequiredDate: value});
        this.setDateTime(value, null);
    }
    setTime(e) {
        let value = e.target.value;
        this.setState({lookRequiredTime: value});
        this.setDateTime(null, value);
    }
    setDateTime(date, time) {
        let lookRequiredDate = date ? date : this.state.lookRequiredDate;
        let lookRequiredTime = time ? time : this.state.lookRequiredTime;
        let newDate;
        if (lookRequiredDate) {
            if (lookRequiredTime) newDate = new Date(lookRequiredDate + " " + lookRequiredTime);
            else newDate = new Date(lookRequiredDate);
        }
        this.setState({lookRequiredDateTime: newDate});
        let data = this.props.applications.applicationsDetailToExecData;
        data.startDate = newDate;
        store.dispatch({type: 'APPLICATIONS_DETAIL_TO_EXEC_DATA', applicationsDetailToExecData: data});
    }
    render() {
        return (
            <Wrapper>
                <Container>
                    <InputCont>
                        <InputLabel>Дата</InputLabel>
                        <StyledInput onChange={this.setDate} name='date' type='date'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Время</InputLabel>
                        <StyledInput onChange={this.setTime} name='time' type='time'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Стоимость</InputLabel>
                        <StyledInput onChange={this.inputChange} value={this.props.applications.applicationsDetailToExecData.customerCost} name='customerCost' type='number'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Оплата исполнителю</InputLabel>
                        <StyledInput onChange={this.inputChange} value={this.props.applications.applicationsDetailToExecData.performerPay} name='performerPay' type='number'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Примечания</InputLabel>
                        <StyledInput style={{width: '272px'}} onChange={this.inputChange} name='other' type='text'/>
                    </InputCont>
                </Container>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
`;
const Container = styled.div`
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps)(ApplicationsDetailToExecData);
