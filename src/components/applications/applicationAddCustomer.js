import React, { Component } from 'react';
import styled from 'styled-components';
import store from '../../reducers';
import connect from "react-redux/es/connect/connect";
import ApplicationAddCustomerDb from './applicationAddCustomerDb';
import ApplicationAddCustomerNew from './applicationAddCustomerNew';

class ApplicationsAddCustomer extends Component {
    constructor(props) {
        super(props);
        this.customerSourceSelect = this.customerSourceSelect.bind(this);
    }
    customerSourceSelect(e) {
        let storeCustomer = this.props.applications.applicationsAddCustomer;
        storeCustomer.source = e.target.value;
        store.dispatch({ type: 'APPLICATIONS_ADD_CUSTOMER', applicationsAddCustomer: storeCustomer });
    }
    render() {
        return (
            <Wrapper>
                <Separator>Заказчик</Separator>
                {/*<InputCont>
                    <select onChange={this.customerSourceSelect} value={this.props.applications.applicationsAddCustomer.source} style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}} name='customerSource'>
                        <option value='db'>Из базы</option>
                        <option value='new'>Новый</option>
                    </select>
                </InputCont>*/}
                {this.props.applications.applicationsAddCustomer.source === 'db' ? <ApplicationAddCustomerDb/> :null}
                {this.props.applications.applicationsAddCustomer.source === 'new' ? <ApplicationAddCustomerNew/> :null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
`;
/*const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;*/
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 18px;
`;

const mapStateToProps = (store) => {return {applications: store.applications}};
export default connect(mapStateToProps)(ApplicationsAddCustomer);
