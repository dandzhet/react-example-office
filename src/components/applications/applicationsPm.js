import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

class ApplicationsPm extends Component {
  constructor(props) {
    super(props);
    this.state = {
        createDate: '- - -',
        updateDate: '- - -',
    };
    this.getDate = this.getDate.bind(this);
  }
  componentDidMount() {
     this.getDate();
  }
  getDate() {
    let createDate = '- - -';
    let updateDate = '- - -';
    if (this.props.dateOfCreateon) {
        let dateObj = new Date(this.props.dateOfCreateon);
        let minutes = dateObj.getMinutes().toString().length === 1 ? '0' + dateObj.getMinutes() : dateObj.getMinutes();
        createDate = dateObj.toISOString().substring(0, 10) + ' ' + dateObj.getHours() + ':' + minutes;
    }
    if (this.props.updateDate) {
        let dateObj = new Date(this.props.updateDate);
        let minutes = dateObj.getMinutes().toString().length === 1 ? '0' + dateObj.getMinutes() : dateObj.getMinutes();
        updateDate = dateObj.toISOString().substring(0, 10) + ' ' + dateObj.getHours() + ':' + minutes;
    }
    this.setState({
        createDate:createDate,
        updateDate:updateDate,
    });
  }
  render() {
    let toPath = '/applications/' + this.props.id;
    return (
      <Wrapper exact={true} to={{ pathname: toPath }}>
        <Container>
            <Customer>{this.props.customer}</Customer>
            <CustomerObject>{this.props.customerObject}</CustomerObject>
            <Stage>
                {this.props.currentStageName === 'lookInit' ? 'Подготовка к осмотру' : null}
                {this.props.currentStageName === 'lookReadyToStart' ? 'Готов к осмотру' : null}
                {this.props.currentStageName === 'lookStart' ? 'Идет осмотр' : null}
                {this.props.currentStageName === 'lookComplete' ? 'Осмотр завершен' : null}
                {this.props.currentStageName === 'execInit' ? 'Подготовка к выполнению' : null}
                {this.props.currentStageName === 'execReadyToStart' ? 'Готов к выполнению' : null}
                {this.props.currentStageName === 'execStart' ? 'Идет выполнение' : null}
                {this.props.currentStageName === 'execComplete' ? 'Выполнение завершено' : null}
            </Stage>
            <CreateDate>{this.state.createDate}</CreateDate>
            <UpdateDate>{this.state.updateDate}</UpdateDate>
        </Container>
      </Wrapper>
    );
  }
}

const Wrapper = styled(NavLink)`
    width: calc(100% - 40px);
    padding: 20px;
    cursor: pointer;
    text-decoration: none;
    &:hover {
        background-color: #e0e0e0;
    }
`;
const Container = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
`;
const Customer = styled.div`
    width: 20%;
    font-weight: 900;
    font-size: 14px;
    color: #212121;
`;
const CustomerObject = styled.div`
    color: #757575;
    width: 20%;
    font-size: 12px;
`;
const Stage = styled.div`
    color: #757575;
    width: 20%;
    font-size: 12px;
`;
const CreateDate = styled.div`
    color: #757575;
    width: 20%;
    font-size: 12px;
`;
const UpdateDate = styled.div`
    color: #757575;
    width: 20%;
    font-size: 12px;
`;

export default ApplicationsPm;
