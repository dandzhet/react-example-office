import React, { Component } from 'react';
import { connect } from "react-redux";
import styled from 'styled-components';
import ApplicationsAddCustomer from './applicationAddCustomer';
import ApplicationsAddObject from './applicationAddObject';
import ApplicationsAddLookWork from './applicationAddLookWork';
import ApplicationsAddPerformer from './applicationAddPerformer';
import ApplicationsAddDetail from './applicationAddDetail';
import StartButton from './../buttons/startButton';
import {Redirect} from "react-router-dom";
import store from "../../reducers";

class ApplicationsAddLook extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.dataPreparationForSending = this.dataPreparationForSending.bind(this);
        this.send = this.send.bind(this);
    }
    dataPreparationForSending() {
        let applicationData = {};
        applicationData.startingStage = 'look';
        if (this.props.applications.applicationsAddCustomer.source === 'db') applicationData.customer = this.props.applications.applicationsAddCustomer.db;
        if (this.props.applications.applicationsAddObject.source === 'db') applicationData.object = this.props.applications.applicationsAddObject.db;
        applicationData.work = this.props.applications.applicationsAddLookWork;
        Object.assign(applicationData, this.props.applications.applicationsAddDetail);
        return applicationData;
    }
    send() {
        store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: true});
        let applicationData = this.dataPreparationForSending();
        console.log(applicationData);
        fetch('/api/applications/add', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(applicationData)})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => {
                console.log(resJson);
                store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false});
                if (resJson.status === 'ok') this.setState({sendResStatus: resJson.status});
            });
    }
    render() {
        return (
            <Wrapper>
                <ApplicationsAddCustomer/>
                <ApplicationsAddObject/>
                <ApplicationsAddLookWork/>
                <ApplicationsAddPerformer/>
                <ApplicationsAddDetail/>
                <StartButton callback={this.send}/>
                {this.state.sendResStatus === 'ok' ? <Redirect to={'/applications'}/> : null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
`;

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps)(ApplicationsAddLook);
