import React, { Component } from 'react';
import styled from 'styled-components';
import store from "../../reducers";

class ApplicationsAddExecutionWork extends Component {
    constructor(props) {
        super(props);
        this.state = {
            snowRoofActive: false,
            snowApronsActive: false,
            snowPerimeterActive: false,
            inputValue: [],
        };
    }
    workNameSelect(e, stateActiveName) {
        let state = this.state;
        state[stateActiveName] = !this.state[stateActiveName];
        let activeNames = [];
        let obj = {};
        if (state.snowRoofActive) {
            activeNames.push('roof');
            obj.roof = {
                area: 0,
                height: 0,
                roofType: 'pitched'
            };
        }
        if (state.snowApronsActive) {
            activeNames.push('aprons');
            obj.aprons = {
                area: 0,
                apronsType: 'entry'
            };
        }
        if (state.snowPerimeterActive) {
            activeNames.push('perimeter');
            obj.perimeter = {
                area: 0,
                distance: 0,
            };
        }
        state.inputValue = activeNames;
        store.dispatch({type: 'APPLICATIONS_ADD_EXEC_WORK', applicationsAddExecWork: obj});
        this.setState(state);
    }
    render() {
        return (
            <Wrapper>
                <input name='work' type='hidden' value={JSON.stringify(this.state.inputValue)}/>
                <Separator>Виды работ</Separator>
                <SelectCont>
                    <SelectCollectionCont>
                        <SelectCollectionItemsCont>
                            {this.state.snowRoofActive ? <SelectCollectionItemActive onClick={(e) => {this.workNameSelect(e, 'snowRoofActive')}}>Кровля</SelectCollectionItemActive> : <SelectCollectionItem onClick={(e) => {this.workNameSelect(e, 'snowRoofActive')}}>Кровля</SelectCollectionItem>}
                            {this.state.snowApronsActive ? <SelectCollectionItemActive onClick={(e) => {this.workNameSelect(e, 'snowApronsActive')}}>Козырьки</SelectCollectionItemActive> : <SelectCollectionItem onClick={(e) => {this.workNameSelect(e, 'snowApronsActive')}}>Козырьки</SelectCollectionItem>}
                            {this.state.snowPerimeterActive ? <SelectCollectionItemActive onClick={(e) => {this.workNameSelect(e, 'snowPerimeterActive')}}>Периметр</SelectCollectionItemActive> : <SelectCollectionItem onClick={(e) => {this.workNameSelect(e, 'snowPerimeterActive')}}>Периметр</SelectCollectionItem>}
                        </SelectCollectionItemsCont>
                    </SelectCollectionCont>
                </SelectCont>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div``;
const SelectCont = styled.div`
    padding: 5px 0;
    position: relative;
    width: 284px;
    height: 121px;
    background-color: #ffffff;
    border: 1px solid #eeeeee;
    border-radius: 5px;
    overflow: scroll;
`;
const SelectCollectionCont = styled.div``;
const SelectCollectionItemsCont = styled.div``;
const SelectCollectionItem = styled.div`
    padding: 5px;
    padding-left: 10px;
    font-size: 12px;
    color: #212121;
    cursor: pointer;
    &:hover {
        background-color: #eeeeee;
    }
`;
const SelectCollectionItemActive = styled.div`
    padding: 5px;
    padding-left: 10px;
    font-size: 12px;
    color: #eeeeee;
    cursor: pointer;
    background-color: #bdbdbd;
`;
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 18px;
`;

export default ApplicationsAddExecutionWork;

