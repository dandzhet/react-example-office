import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../../reducers';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

import ApplicationsPm from '../applicationsPm';

class Applications extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.getData = this.getData.bind(this);
    this.toStore = this.toStore.bind(this);
    Applications.toNav = this.toNav.bind(this);
  }
  componentDidMount() {
    this.toNav();
    this.getData();
  }
  getData() {
    fetch('/api/applications', { method: 'POST' })
    .then(res => {
      if (res.status !== 200) {
          console.log('Err: ' + res.statusText + ' ' + res.status);
          return;
      };
      res.json().then((json) => {
        console.log(json);
        this.toStore('APPLICATIONS_DATA', 'applicationsData', json.data);
      });
    });
  }
  toNav() {
    let location = [
      { path: '/applications', name: 'Заявки' },
    ];
    this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
  }
  toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
  render() {
    return (
      <Wrapper>
        <Container>
          {this.props.applications.applicationsData && this.props.applications.applicationsData.length ? this.props.applications.applicationsData.map((el) => {
              return <ApplicationsPm key={el._id} id={el._id} name={el.customer.name} work={el.work} currentStageName={el.stages[el.stages.current].name} currentStageStatus={el.stages[el.stages.current].status.name}/>
          }) : <Empty>Пусто</Empty>}
          <AddBtn exact={true} to={{ pathname: '/applications/add' }}>+</AddBtn>
        </Container>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;
const Container = styled.div`

`;
const AddBtn = styled(NavLink)`
  position: fixed;
  width: 50px;
  height: 50px;
  bottom: 20px;
  right: 20px;
  background-color: #212121;
  border-radius: 50px;
  cursor: pointer;
  box-shadow: rgba(0,0,0,0.5) 0px 0px 8px 1px;
  color: #ffffff;
  font-size: 36px;
  text-decoration: none;
  text-align: center;
  line-height: 44px;
  transition: .2s;
  &:hover {
    transform: scale(1.2);
  }
`;

const Empty = styled.div`
  padding: 20px;
  color: #757575;
`;

const mapStateToProps = (store) => {
  return {
    applications: store.applications
  };
};

export default connect(mapStateToProps)(Applications);
