import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../../reducers';
import styled from 'styled-components';

class ApplicationsAddPerformerInput extends Component {
    constructor(props) {
        super(props);
        this.state = {performerSelectMethod: 'auto'};
        this.performerSelectMethodChange = this.performerSelectMethodChange.bind(this);
        this.performerSelect = this.performerSelect.bind(this);
        this.getPerformersData = this.getPerformersData.bind(this);
        this.toStore = this.toStore.bind(this);
    }
    componentDidMount() {}
    /*getSnapshotBeforeUpdate(prevProps, prevState) {
        //if (this.state.performerSelectMethod !== prevState.performerSelectMethod) {
            const selectRef = this.selectRef;
            return selectRef;
        //}
        //return null;
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot !== null) {
            const selectRef = this.selectRef;
            selectRef.value = snapshot.value;
        }
    }*/
    performerSelectMethodChange(e) {
        if (e.target.value === 'manual') {
            this.toStore('APPLICATIONS_ADD_PERFORMER_INPUT_DATA', 'applicationsAddPerformerInputData', []);
            this.getPerformersData();
        }
        this.setState({performerSelectMethod: e.target.value});
    }
    performerSelect(e, id) {
        let data = this.props.applications.applicationsAddPerformerInputData;
        let newData = data.map((el) => {
            if (el._id === id) el.active = !el.active;
            return el;
        });
        let selectedPerformers = newData.filter((el) => {
            return el.active;
        });
        let inputValue = selectedPerformers.map((el) => {
            return {id: el._id};
        });
        this.toStore('APPLICATIONS_ADD_PERFORMER_INPUT_DATA', 'applicationsAddPerformerInputData', newData);
        this.toStore('APPLICATIONS_ADD_PERFORMER_INPUT_VALUE', 'applicationsAddPerformerInputValue', inputValue);
    }
    getPerformersData() {
        let path = '/api/performers';
        fetch(path, {method: 'POST'})
        .then(res => {
            if (res.status !== 200) {
                console.log('Err: ' + res.statusText + ' ' + res.status);
                return;
            };
            res.json().then((json) => {
                console.log(json);
                json.data = json.data.map(el => {
                    el.active = false;
                    return el;
                });
                this.toStore('APPLICATIONS_ADD_PERFORMER_INPUT_DATA', 'applicationsAddPerformerInputData', json.data);
            });
        });
    }
    toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
    render() {
        return (
            <Wrapper>
                <input name='performer' type='hidden' value={JSON.stringify(this.props.applications.applicationsAddPerformerInputValue)}></input>
                <InputCont>
                    <InputLabel>Способ подбора</InputLabel>
                    <select ref={el => this.selectRef = el} onChange={this.performerSelectMethodChange} style={{width: '150px'}} name='performerSelectionMethod'>
                        <option value='auto'>Авто</option>
                        <option value='all'>Все</option>
                        <option value='manual'>Вручную</option>
                    </select>
                </InputCont>
                {this.state.performerSelectMethod === 'auto' ? <HelpText>Поиск исполнителей подходящих под указанные виды работ</HelpText> : null}
                {this.state.performerSelectMethod === 'all' ? <HelpText>Выбор всех исполнителей не зависимо от указанных видов работ</HelpText> : null}
                {this.state.performerSelectMethod === 'manual' ? 
                    <SelectCont>
                        {this.props.applications.applicationsAddPerformerInputData.map((el) => {
                            return (
                                <SelectCollectionCont key={el._id}>
                                    {el.active ? 
                                        <SelectCollectionTitleActive onClick={(e) => {this.performerSelect(e, el._id)}}>
                                            {el.name} <SelectCollectionTitleActivePerformerStatus>{el.performerStatus}</SelectCollectionTitleActivePerformerStatus>
                                        </SelectCollectionTitleActive>
                                        :
                                        <SelectCollectionTitle onClick={(e) => {this.performerSelect(e, el._id)}}>
                                            {el.name} <SelectCollectionTitlePerformerStatus>{el.performerStatus}</SelectCollectionTitlePerformerStatus>
                                        </SelectCollectionTitle>}
                                </SelectCollectionCont>
                            );
                        })}
                    </SelectCont>
                : null}
                <InputCont style={{marginTop: '10px'}}>
                    <InputLabel>Оповестить по смс</InputLabel>
                    <StyledInputCheckbox name='smsNotification' type='checkbox'></StyledInputCheckbox>
                </InputCont>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
`;
const InputLabel = styled.div`
    width: 150px;
    font-size: 14px;
    color: #757575;
`;
const HelpText = styled.div`
    font-size: 12px;
    color: #bdbdbd;
`;
const SelectCont = styled.div`
    position: relative;
    width: 400px;
    height: 200px;
    background-color: #ffffff;
    border: 1px solid #eeeeee;
    overflow: scroll;
`;
const SelectCollectionCont = styled.div``;
const SelectCollectionTitle = styled.div`
    padding: 5px;
    font-size: 14px;
    color: #212121;
    font-weight: 900;
    cursor: pointer;
    &:hover {
        background-color: #eeeeee;
    }
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
`;
const SelectCollectionTitlePerformerStatus = styled.div`
    padding-left: 10px;
    font-size: 12px;
    color: #757575;
    font-weight: 400;
`;
const SelectCollectionTitleActive = styled.div`
    padding: 5px;
    font-size: 14px;
    color: #ffffff;
    font-weight: 900;
    cursor: pointer;
    background-color: #bdbdbd;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
`;
const SelectCollectionTitleActivePerformerStatus = styled.div`
    padding-left: 10px;
    font-size: 12px;
    color: #eeeeee;
    font-weight: 400;
`;
const StyledInputCheckbox = styled.input`
    margin: 0;
`;

const mapStateToProps = (store) => {return {applications: store.applications}};
export default connect(mapStateToProps)(ApplicationsAddPerformerInput);