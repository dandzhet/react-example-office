import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../../reducers';
import { withRouter, NavLink } from 'react-router-dom';
import styled from 'styled-components';

import ApplicationsAddLook from './applicationsAddLook';
import ApplicationsAddExecution from './applicationsAddExecution';

const ApplicationsAddWithRouter = withRouter(props => <ApplicationsAddOld {...props}/>);

class ApplicationsAddOld extends Component {
  constructor(props) {
    super(props);
    this.state = {customerData: []};
    this.applicationTypeSelect = this.applicationTypeSelect.bind(this);
    this.toNav = this.toNav.bind(this);
    this.toStore = this.toStore.bind(this);
  }
  componentDidMount() {
    this.toNav();
    this.toStore('APPLICATIONS_ADD_TYPE', 'applicationsAddType', 'look');
  }
  applicationTypeSelect(e) {
    let type = this.props.applications.applicationsAddType;
    let newType = e.target.value;
    if (newType !== type) this.toStore('APPLICATIONS_ADD_TYPE', 'applicationsAddType', newType);
  }
  toNav() {
    let location = [
      { path: '/applications', name: 'Заявки' },
      { path: '/applications/add', name: 'Добавление' }
    ];
    let controls = {
      addBtn: {
        display: false,
        name: '',
        path: '',
      },
      search: {
        display: true,
      }
    };
    this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
    this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
  }
  toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
  render() {
    return (
      <Wrapper>
        <Container>
        <NavLink style={{textDecoration: 'none', color: '#212121'}} exact={true} to={{ pathname: '/applications' }}>
          <i className="fas fa-angle-left" style={{marginRight: '10px'}}/>
          Новая заявка
        </NavLink>
          <Separator style={{marginTop: '20px'}}>Начальный этап</Separator>
          <InputCont>
              <select onChange={this.applicationTypeSelect} value={this.props.applications.applicationsAddType} style={{width: '150px'}} name='stage'>
                <option value='look'>Осмотр объекта</option>
                <option value='execution'>Выполнение работ</option>
              </select>
          </InputCont>
          {this.props.applications.applicationsAddType === 'look' ? <ApplicationsAddLook/> : null}
          {this.props.applications.applicationsAddType === 'execution' ? <ApplicationsAddExecution/> : null}
        </Container>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  width: calc(100% - 8px);
  padding: 4px;
  position: relative;
`;
const Container = styled.div`
  padding: 10px;
  background-color: #ffffff;
`;
const InputCont = styled.div`
  margin-right: 10px;
  margin-bottom: 10px;
`;
const Separator = styled.div`
  width: 100%;
  padding: 10px 0;
  font-weight: 900;
  color: #212121;
  font-size: 16px;
`;

const mapStateToProps = (store) => {return {applications: store.applications}};
export default connect(mapStateToProps)(ApplicationsAddWithRouter);
