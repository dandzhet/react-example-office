import React, { Component } from 'react';
//import { connect } from "react-redux";
import store from '../../../reducers';
import { Redirect } from 'react-router-dom';
import styled from 'styled-components';

import ApplicationsAddCustomerInput from './applicationsAddCustomerInput';
import ApplicationsAddExecutionWorkInput from './applicationsAddExecutionWorkInput';
import ApplicationsAddPerformerInput from './applicationsAddPerformerInput';

class ApplicationsAddExecution extends Component {
  constructor(props) {
    super(props);
    this.state = {sendResStatus: 'wait', customerFromBase: true, defaultLookRequiredDate: '', lookRequiredDate: '', lookRequiredTime: '', lookRequiredDateTime: '', customerData: []};
    this.getDefaultDate = this.getDefaultDate.bind(this);
    this.setDate = this.setDate.bind(this);
    this.setTime = this.setTime.bind(this);
    this.send = this.send.bind(this);
    this.toStore = this.toStore.bind(this);
  }
  componentDidMount() {
    this.getDefaultDate();
  }
  getDefaultDate() {
    var curr = new Date();
    curr.setDate(curr.getDate() + 1);
    var date = curr.toISOString().substr(0,10);
    this.setState({defaultLookRequiredDate: date, lookRequiredDateTime: new Date(`${date} 12:00`)});
  }
  setDate(e) {
    let value = e.target.value;
    this.setState({lookRequiredDate: value});
    this.setDateTime(value, null);
  }
  setTime(e) {
    let value = e.target.value;
    this.setState({lookRequiredTime: value});
    this.setDateTime(null, value);
  }
  setDateTime(date, time) {
    let lookRequiredDate = date ? date : this.state.lookRequiredDate;
    let lookRequiredTime = time ? time : this.state.lookRequiredTime;
    let newDate;
    if (lookRequiredDate) {
      if (lookRequiredTime) newDate = new Date(lookRequiredDate + " " + lookRequiredTime);
      else newDate = new Date(lookRequiredDate);
    }
    this.setState({lookRequiredDateTime: newDate});
    console.log(newDate);
  }
  send(e) {
    e.preventDefault();
    let dataFromInputs = {};
    for (let i = 0; i < e.target.length - 1; i++) {
        if (e.target[i].type === 'checkbox') dataFromInputs[e.target[i].name] = e.target[i].checked;
        else dataFromInputs[e.target[i].name] = e.target[i].value;
        if (e.target[i].name === 'work') {
            console.log(e.target[i].value);
        }
    };
    let customer = {id: dataFromInputs.customer};
    let performer = dataFromInputs.performerSelectionMethod === 'manual' ? {
        selectionMethod: dataFromInputs.performerSelectionMethod,
        listForSelection: JSON.parse(dataFromInputs.performer),
    } : {
        selectionMethod: dataFromInputs.performerSelectionMethod,
    };
    let objForSend = {
        stage: 'execution',
        toDraft: false,
        customer: customer,
        work: JSON.parse(dataFromInputs.work),
        performer: performer,
        requiredStartDate: new Date(dataFromInputs.date + ' ' + dataFromInputs.time),
        payToPerformer: dataFromInputs.payToPerformer,
        costForCustomer: dataFromInputs.costForCustomer,
        other: dataFromInputs.other,
    };
    console.log(objForSend);
    let path = '/api/applications/add';
    fetch(path, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(objForSend)
    }).then(res => {
      if (res.status !== 200) {
          console.log('Err: ' + res.statusText + ' ' + res.status);
          return;
      };
      res.json().then((data) => {
        console.log(data);
        if (data.status === 'ok') {
            this.setState({sendResStatus: data.status});
            //this.toStore('DIALOG_ACTIVE', 'dialogActive', true);
        }
      });
    });
  }
  toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
  render() {
    return (
      <Wrapper>
          <StyledForm onSubmit={this.send}>
            <Separator>Информация о заказчике</Separator>
            <ApplicationsAddCustomerInput/>
            <Separator>Информация о видах работ</Separator>
            <InputCont>
                <InputLabel>Виды работ</InputLabel>
                <ApplicationsAddExecutionWorkInput/>
            </InputCont>
            <Separator>Информация о исполнителях</Separator>
            <InputCont>
                <ApplicationsAddPerformerInput/>
            </InputCont>
            <Separator>Детали</Separator>
            <InputCont>
              <InputLabel>Дата</InputLabel>
              <StyledInput onChange={this.setDate} defaultValue={this.state.defaultLookRequiredDate} name='date' type='date'></StyledInput>
            </InputCont>
            <InputCont>
              <InputLabel>Время</InputLabel>
              <StyledInput onChange={this.setTime} defaultValue='12:00' type="time" name="time"/>
            </InputCont>
            <InputCont>
              <InputLabel style={{fontSize: '10px', height: '18px'}}>Оплата исполнителю</InputLabel>
              <StyledInput name='payToPerformer' type='text'></StyledInput>
            </InputCont>
            <InputCont>
              <InputLabel style={{fontSize: '10px', height: '18px'}}>Стоимость для заказчика</InputLabel>
              <StyledInput name='costForCustomer' type='text'></StyledInput>
            </InputCont>
            <Separator>Дополнительная информация</Separator>
            <InputCont>
                <InputLabel>Примечания</InputLabel>
                <textarea style={{width: '300px', height: '200px', padding: '0', border: '1px solid #eeeeee', resize: 'none'}} name='other' type='text'></textarea>
            </InputCont>
            <Separator><SendBtn type='submit'>Приступить</SendBtn><ToDraftBtn type='submit'>В черновики</ToDraftBtn></Separator>
          </StyledForm>
          {this.state.sendResStatus === 'ok' ? <Redirect to="/applications"/> : null}
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
    position: relative;
`;
const StyledForm = styled.form`
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const StyledInput = styled.input``;
const InputLabel = styled.div`
    font-size: 14px;
    color: #757575;
`;
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 16px;
`;
const SendBtn = styled.button`
    width: min-content;
    border-radius: 50px;
    font-size: 12px;
    padding: 10px;
    color: #ffffff;
    text-align: center;
    background-color: rgb(230,61,50);
    cursor: pointer;
`;
const ToDraftBtn = styled.button`
    margin-left: 10px;
    border-radius: 50px;
    font-size: 12px;
    padding: 10px;
    color: #212121;
    text-align: center;
    border: 1px solid #212121;
    background-color: rgb(255,255,255);
    cursor: pointer;
`;

export default ApplicationsAddExecution;
