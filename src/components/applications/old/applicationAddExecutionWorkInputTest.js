import React, { Component } from 'react';
import styled from 'styled-components';

class ApplicationsAddExecutionWorkInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            snowRoofActive: false,
            snowApronsActive: false,
            snowPerimeterActive: false,
            snowRoofCalcData: {
                base: '8',
                baseCustomer: '20',
                volume: '0',
                height: '0',
                type: 's1',
                motivation: 's1',
                night: 's1',
                difficult: 's1',
                summ: '0',
                summCustomer: '0',
            },
            snowApronsCalcData: {
                type: 's1',
                volume: '0',
                summ: '0',
                summCustomer: '0',
            },
            snowPerimeterCalcData: {
                base: '20',
                baseCustomer: '40',
                volume: '0',
                distance: '0',
                summ: '0',
                summCustomer: '0',
            },
            inputValue: {},
        };
        this.workNameSelect = this.workNameSelect.bind(this);
        this.snowRoofCalcInputChange = this.snowRoofCalcInputChange.bind(this);
        this.snowRoofCalculate = this.snowRoofCalculate.bind(this);
        this.snowApronsCalcInputChange = this.snowApronsCalcInputChange.bind(this);
        this.snowApronsCalculate = this.snowApronsCalculate.bind(this);
        this.snowPerimeterCalcInputChange = this.snowPerimeterCalcInputChange.bind(this);
        this.snowPerimeterCalculate = this.snowPerimeterCalculate.bind(this);
    }
    workNameSelect(e, stateActiveName) {
        let newStateActiveName = !this.state[stateActiveName];
        this.setState({[stateActiveName]: newStateActiveName});
    }
    snowRoofCalcInputChange(e, name) {
        let snowRoofCalcData = this.state.snowRoofCalcData;
        if (name === 'volume' || name === 'height' || name === 'type') {
            if (name === 'height' && e.target.value === '') {
                snowRoofCalcData[name] = '0';
            } else {
                snowRoofCalcData[name] = e.target.value;
            }
        } else if (name === 'motivation' || name === 'night' || name === 'difficult') {
            if (e.target.checked) {
                snowRoofCalcData[name] = 's2';
            } else {
                snowRoofCalcData[name] = 's1';
            }
        }
        this.setState({snowRoofCalcData:snowRoofCalcData});
        this.snowRoofCalculate();
    }
    snowRoofCalculate() {
        let getVolumeRatio = (vol) => {
            let ratio;
            if (vol < 100) ratio = 1.3;
            else if (vol < 500) ratio = 1.2;
            else if (vol < 1000) ratio = 1.1;
            else ratio = 1;
            return ratio;
        };
        let getHeightRatio = (height) => {
            let ratio;
            if (height === 0) {
                ratio = 0;
                return ratio;
            }
            if (height < 20) ratio = 1;
            else if (height < 50) ratio = 1.1;
            else if (height < 100) ratio = 1.2;
            else if (height < 150) ratio = 1.3;
            else ratio = 1.4;
            return ratio;
        };
        let getTypeRatio = (val) => {
            let ratio;
            if (val === 's1') ratio = 1.1;
            else if (val === 's2') ratio = 1;
            return ratio;
        };
        let getMotivationRatio = (val) => {
            let ratio;
            if (val === 's1') ratio = 1;
            else if (val === 's2') ratio = 1.1;
            return ratio;
        };
        let getNightRatio = (val) => {
            let ratio;
            if (val === 's1') ratio = 1;
            else if (val === 's2') ratio = 1.1;
            return ratio;
        };
        let getDifficultRatio = (val) => {
            let ratio;
            if (val === 's1') ratio = 1;
            else if (val === 's2') ratio = 1.2;
            return ratio;
        };
        let s1 = this.state.snowRoofCalcData;
        let base = parseInt(s1.base, 10);
        let volume = parseInt(s1.volume, 10);
        let volumeRatio = getVolumeRatio(volume);
        let heightRatio = getHeightRatio(parseInt(s1.height, 10));
        let typeRatio = getTypeRatio(s1.type);
        let motivationRatio = getMotivationRatio(s1.motivation);
        let nightRatio = getNightRatio(s1.night);
        let difficultRatio = getDifficultRatio(s1.difficult);
        let summ = base*volume*volumeRatio*heightRatio*typeRatio*motivationRatio*nightRatio*difficultRatio;
        let baseCustomer = parseInt(s1.baseCustomer, 10);
        let summCustomer = baseCustomer*volume*volumeRatio*heightRatio*typeRatio*motivationRatio*nightRatio*difficultRatio;
        s1.summ = !Number.isNaN(summ) ? Math.round(summ) : 0;
        s1.summCustomer = !Number.isNaN(summCustomer) ? Math.round(summCustomer) : 0;
        this.setState({snowRoofCalcData:s1});
        let summAll = Math.round(summ) + parseInt(this.state.snowApronsCalcData.summ, 10) + parseInt(this.state.snowPerimeterCalcData.summ, 10);
        let summCustomerAll = Math.round(summCustomer) + parseInt(this.state.snowApronsCalcData.summCustomer, 10) + parseInt(this.state.snowPerimeterCalcData.summCustomer, 10);
        this.props.priceChange(!Number.isNaN(summAll) ? summAll : 0, !Number.isNaN(summCustomerAll) ? summCustomerAll : 0);
    }
    snowApronsCalcInputChange(e, name) {
        let snowApronsCalcData = this.state.snowApronsCalcData;
        snowApronsCalcData[name] = e.target.value;
        this.setState({snowApronsCalcData:snowApronsCalcData});
        this.snowApronsCalculate();
    }
    snowApronsCalculate() {
        let s2 = this.state.snowApronsCalcData;
        let type = s2.type;
        let volume = parseInt(s2.volume, 10);
        let summ;
        let summCustomer;
        if (type === 's1') {
            if (volume === 0 || Number.isNaN(volume)) {
                summ = 0;
                summCustomer = 0;
            }
            else if (volume < 5) {
                summ = 100;
                summCustomer = 200;
            }
            else if (volume < 10) {
                summ = 200;
                summCustomer = 400;
            }
            else if (volume < 20) {
                summ = 300;
                summCustomer = 600;
            }
            else {
                summ = 300;
                summCustomer = 600;
            }
        } else if (type === 's2') {
            if (volume === 0 || Number.isNaN(volume)) {
                summ = 0;
                summCustomer = 0;
            }
            else if (volume < 5) {
                summ = 200;
                summCustomer = 500;
            }
            else if (volume < 10) {
                summ = 300;
                summCustomer = 700;
            }
            else if (volume < 20) {
                summ = 400;
                summCustomer = 800;
            }
            else {
                summ = 400;
                summCustomer = 800;
            }
        }
        s2.summ = summ;
        s2.summCustomer = summCustomer;
        this.setState({snowApronsCalcData:s2});
        let summAll = parseInt(this.state.snowRoofCalcData.summ, 10) + summ + parseInt(this.state.snowPerimeterCalcData.summ, 10);
        let summCustomerAll = parseInt(this.state.snowRoofCalcData.summCustomer, 10) + summCustomer + parseInt(this.state.snowPerimeterCalcData.summCustomer, 10);
        this.props.priceChange(!Number.isNaN(summAll) ? summAll : 0, !Number.isNaN(summCustomerAll) ? summCustomerAll : 0);
    }
    snowPerimeterCalcInputChange(e, name) {
        let snowPerimeterCalcData = this.state.snowPerimeterCalcData;
        snowPerimeterCalcData[name] = e.target.value;
        this.setState({snowPerimeterCalcData:snowPerimeterCalcData});
        this.snowPerimeterCalculate();
    }
    snowPerimeterCalculate() {
        let getVolumeRatio = (vol) => {
            let ratio;
            if (vol < 10) ratio = 1.3;
            else if (vol < 50) ratio = 1.2;
            else if (vol < 100) ratio = 1.1;
            else ratio = 1;
            return ratio;
        };
        let getDistanceRatio = (dist) => {
            let ratio;
            if (dist < 0.5 || Number.isNaN(dist)) {
                ratio = 0;
            }
            else if (dist < 0.5) ratio = 1;
            else if (dist < 1) ratio = 1.2;
            else if (dist < 1.5) ratio = 1.3;
            else ratio = 1.3;
            return ratio;
        };
        let s3 = this.state.snowPerimeterCalcData;
        let base = parseInt(s3.base, 10);
        let baseCustomer = parseInt(s3.baseCustomer, 10);
        let volume = parseInt(s3.volume, 10);
        let volumeRatio = getVolumeRatio(volume);
        let distanceRatio = getDistanceRatio(parseInt(s3.distance, 10));
        let summ = base*volume*volumeRatio*distanceRatio;
        let summCustomer = baseCustomer*volume*volumeRatio*distanceRatio;
        s3.summ = !Number.isNaN(summ) ? summ : 0;
        s3.summCustomer = !Number.isNaN(summCustomer) ? summCustomer : 0;
        this.setState({snowPerimeterCalcData:s3});
        let summAll = parseInt(this.state.snowRoofCalcData.summ, 10) + parseInt(this.state.snowApronsCalcData.summ, 10) + summ;
        let summCustomerAll = parseInt(this.state.snowRoofCalcData.summCustomer, 10) + parseInt(this.state.snowApronsCalcData.summCustomer, 10) + summCustomer;
        this.props.priceChange(!Number.isNaN(summAll) ? summAll : 0, !Number.isNaN(summCustomerAll) ? summCustomerAll : 0);
    }
    render() {
        return (
            <Wrapper>
                <input name='work' type='hidden' value={JSON.stringify(this.state.inputValue)}/>
                <SelectCont>
                    <SelectCollectionCont>
                        <SelectCollectionTitle>Снег</SelectCollectionTitle>
                        <SelectCollectionItemsCont>
                            {this.state.snowRoofActive ? <SelectCollectionItemActive onClick={(e) => {this.workNameSelect(e, 'snowRoofActive')}}>Кровля</SelectCollectionItemActive> : <SelectCollectionItem onClick={(e) => {this.workNameSelect(e, 'snowRoofActive')}}>Кровля</SelectCollectionItem>}
                            {this.state.snowApronsActive ? <SelectCollectionItemActive onClick={(e) => {this.workNameSelect(e, 'snowApronsActive')}}>Козырьки</SelectCollectionItemActive> : <SelectCollectionItem onClick={(e) => {this.workNameSelect(e, 'snowApronsActive')}}>Козырьки</SelectCollectionItem>}
                            {this.state.snowPerimeterActive ? <SelectCollectionItemActive onClick={(e) => {this.workNameSelect(e, 'snowPerimeterActive')}}>Периметр</SelectCollectionItemActive> : <SelectCollectionItem onClick={(e) => {this.workNameSelect(e, 'snowPerimeterActive')}}>Периметр</SelectCollectionItem>}
                        </SelectCollectionItemsCont>
                    </SelectCollectionCont>
                </SelectCont>
                <WorkValueCont>
                    {this.state.snowRoofActive ?
                        <CalcCont style={{marginRight: '5px'}}>
                            <CalcTitle>Кровля</CalcTitle>
                            <InputCont>
                                <InputLabel>Объем (м2)</InputLabel>
                                <StyledInput onChange={(e) => {this.snowRoofCalcInputChange(e, 'volume')}} defaultValue='0' type='number'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Высота снежного покрова (см)</InputLabel>
                                <StyledInput onChange={(e) => {this.snowRoofCalcInputChange(e, 'height')}} defaultValue='0' type='number'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Тип кровли</InputLabel>
                                <select onChange={(e) => {this.snowRoofCalcInputChange(e, 'type')}} style={{width: '130px'}}>
                                    <option value='s1'>Плоская</option>
                                    <option value='s2'>Скатная</option>
                                </select>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Мотивирующий коэффицент</InputLabel>
                                <input onChange={(e) => {this.snowRoofCalcInputChange(e, 'motivation')}} type='checkbox'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Ночной коэффицент</InputLabel>
                                <input onChange={(e) => {this.snowRoofCalcInputChange(e, 'night')}} type='checkbox'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Сложная крыша</InputLabel>
                                <input onChange={(e) => {this.snowRoofCalcInputChange(e, 'difficult')}} type='checkbox'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel style={{width: '100px'}}>Стоимость</InputLabel>
                                <InputLabel style={{width: '100px'}}>{this.state.snowRoofCalcData.summCustomer}</InputLabel>
                            </InputCont>
                            <InputCont>
                                <InputLabel style={{width: '100px'}}>Зарплата</InputLabel>
                                <InputLabel style={{width: '100px'}}>{this.state.snowRoofCalcData.summ}</InputLabel>
                            </InputCont>
                        </CalcCont>
                    :null}
                    {this.state.snowApronsActive ?
                        <CalcCont>
                            <CalcTitle>Козырьки</CalcTitle>
                            <InputCont>
                                <InputLabel>Тип козырька</InputLabel>
                                <select onChange={(e) => {this.snowApronsCalcInputChange(e, 'type')}} style={{width: '130px'}}>
                                    <option value='s1'>Входной</option>
                                    <option value='s2'>Балконный</option>
                                </select>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Объем (м2)</InputLabel>
                                <StyledInput onChange={(e) => {this.snowApronsCalcInputChange(e, 'volume')}} defaultValue='0' type='number'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel style={{width: '100px'}}>Стоимость</InputLabel>
                                <InputLabel style={{width: '100px'}}>{this.state.snowApronsCalcData.summCustomer}</InputLabel>
                            </InputCont>
                            <InputCont>
                                <InputLabel style={{width: '100px'}}>Зарплата</InputLabel>
                                <InputLabel style={{width: '100px'}}>{this.state.snowApronsCalcData.summ}</InputLabel>
                            </InputCont>
                        </CalcCont>
                    :null}
                    {this.state.snowPerimeterActive ?
                        <CalcCont>
                            <CalcTitle>Периметр</CalcTitle>
                            <InputCont>
                                <InputLabel>Объем (м2)</InputLabel>
                                <StyledInput onChange={(e) => {this.snowPerimeterCalcInputChange(e, 'volume')}} defaultValue='0' type='number'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Расстояние от края (м)</InputLabel>
                                <StyledInput onChange={(e) => {this.snowPerimeterCalcInputChange(e, 'distance')}} defaultValue='0' type='number'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel style={{width: '100px'}}>Стоимость</InputLabel>
                                <InputLabel style={{width: '100px'}}>{this.state.snowPerimeterCalcData.summCustomer}</InputLabel>
                            </InputCont>
                            <InputCont>
                                <InputLabel style={{width: '100px'}}>Зарплата</InputLabel>
                                <InputLabel style={{width: '100px'}}>{this.state.snowPerimeterCalcData.summ}</InputLabel>
                            </InputCont>
                        </CalcCont>
                    :null}
                </WorkValueCont>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: row;
`;
const SelectCont = styled.div`
    position: relative;
    width: 400px;
    height: 200px;
    background-color: #ffffff;
    border: 1px solid #eeeeee;
    overflow: scroll;
    flex-shrink: 0;
`;
const SelectCollectionCont = styled.div``;
const SelectCollectionTitle = styled.div`
    padding: 5px;
    font-size: 14px;
    color: #212121;
    font-weight: 900;
    cursor: pointer;
`;
const SelectCollectionItemsCont = styled.div``;
const SelectCollectionItem = styled.div`
    padding: 5px;
    padding-left: 10px;
    font-size: 14px;
    color: #757575;
    cursor: pointer;
    &:hover {
        background-color: #eeeeee;
    }
`;
const SelectCollectionItemActive = styled.div`
    padding: 5px;
    padding-left: 10px;
    font-size: 14px;
    color: #eeeeee;
    cursor: pointer;
    background-color: #bdbdbd;
`;
const WorkValueCont = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    margin-left: 20px;
`;
const CalcCont = styled.div`
    padding: 5px;
    border: 1px solid #eeeeee;
    margin-bottom: 5px;
    height: min-content;
`;
const CalcTitle = styled.div`
    padding-bottom: 10px;
    font-size: 12px;
    font-weight: 900;
`;
const InputCont = styled.div`
    display: flex;
    flex-direction: row;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
    width: 180px;
`;
const StyledInput = styled.input``;

export default ApplicationsAddExecutionWorkInput;