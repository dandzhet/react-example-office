import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../../reducers';
import {NavLink, withRouter} from 'react-router-dom';
import styled from 'styled-components';

import DeleteButton from '../buttons/deleteButton';
import EditButton from '../buttons/editButton';

const ApplicationsDetailWithRouter = withRouter(props => <ApplicationDetail {...props}/>);

class ApplicationDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dateOfCreateon: '- - -',
            lookDataLoaded: false,
            lookData: {},
            lookReportDataLoaded: false,
            lookReportData: {},
            lookReportImgs: [],
            executionDataLoaded: false,
            executionData: {},
            executionReportDataLoaded: false,
            executionReportData: {},
            executionReportImgsBefore: [],
            executionReportImgsAfter: [],
        };
        this.getData = this.getData.bind(this);
        this.getLookApplicationData = this.getLookApplicationData.bind(this);
        this.getLookApplicationReportData = this.getLookApplicationReportData.bind(this);
        this.getExecutionApplicationData = this.getExecutionApplicationData.bind(this);
        this.delete = this.delete.bind(this);
        this.toStore = this.toStore.bind(this);
        this.toNav = this.toNav.bind(this);
    }
    componentDidMount() {
        this.toStore('APPLICATIONS_DETAIL_DATA', 'applicationsDetailData', {});
        this.getData();
    }
    getData() {
        let path = '/api/applications/' + this.props.match.params.id;
        fetch(path, {method: 'POST'})
            .then(res => {
                if (res.status !== 200) return console.log('Err: ' + res.statusText + ' ' + res.status);
                res.json().then((data) => {
                    console.log(data);
                    this.toStore('APPLICATIONS_DETAIL_DATA', 'applicationsDetailData', data);
                    this.getDateOfCreation(data.dateOfCreateon);
                    if (data.stages.hasOwnProperty('look')) {
                        this.getLookApplicationData(data.stages.look.application.id);
                        this.getLookApplicationReportData(data.stages.look.reportFromPerformer.id);
                    }
                    if (data.stages.hasOwnProperty('execution')) {
                        this.getExecutionApplicationData(data.stages.execution.application.id);
                        this.getExecutionApplicationReportData(data.stages.execution.reportFromPerformer.id);
                    }
                    this.toNav();
                });
            });
    }
    getLookApplicationData(id) {
        let path = '/api/applications/look/' + id;
        fetch(path, {method: 'POST'})
            .then(res => {
                if (res.status !== 200) return console.log('Err: ' + res.statusText + ' ' + res.status);
                res.json().then(data => {
                    console.log(data);
                    this.setState({lookDataLoaded: true, lookData: data.data});
                });
            });
    }
    getLookApplicationReportData(id) {
        let path = '/api/applications/look/report/' + id;
        fetch(path, {method: 'POST'})
            .then(res => {
                if (res.status !== 200) return console.log('Err: ' + res.statusText + ' ' + res.status);
                res.json().then((data) => {
                    console.log(data);
                    if (data.data.hasOwnProperty('photo') && data.data.photo && data.data.photo.length) {
                        let imgUrls = data.data.photo.map((photoEl) => {
                            let arrayBufferView = new Uint8Array( photoEl.data.data );
                            let blob = new Blob( [ arrayBufferView ], { type: photoEl.contentType } );
                            let urlCreator = window.URL || window.webkitURL;
                            return urlCreator.createObjectURL(blob);
                        });
                        this.setState({lookReportDataLoaded: true, lookReportData: data.data, lookReportImgs: imgUrls});
                    }
                });
            });
    }
    getExecutionApplicationData(id) {
        let path = '/api/applications/execution/' + id;
        fetch(path, {method: 'POST'})
            .then(res => {
                if (res.status !== 200) return console.log('Err: ' + res.statusText + ' ' + res.status);
                res.json().then(data => {
                    console.log(data);
                    this.setState({executionDataLoaded: true, executionData: data.data});
                });
            });
    }
    getExecutionApplicationReportData(id) {
        let path = '/api/applications/execution/report/' + id;
        fetch(path, {method: 'POST'})
            .then(res => {
                if (res.status !== 200) return console.log('Err: ' + res.statusText + ' ' + res.status);
                res.json().then(data => {
                    console.log(data);
                    if (data.data.hasOwnProperty('photo') && data.data.photo) {
                        let imgUrlsBefore = data.data.photo.before.map((photoEl) => {
                            let arrayBufferView = new Uint8Array( photoEl.data.data );
                            let blob = new Blob( [ arrayBufferView ], { type: photoEl.contentType } );
                            let urlCreator = window.URL || window.webkitURL;
                            return urlCreator.createObjectURL(blob);
                        });
                        let imgUrlsAfter = data.data.photo.after.map((photoEl) => {
                            let arrayBufferView = new Uint8Array( photoEl.data.data );
                            let blob = new Blob( [ arrayBufferView ], { type: photoEl.contentType } );
                            let urlCreator = window.URL || window.webkitURL;
                            return urlCreator.createObjectURL(blob);
                        });
                        this.setState({
                            executionReportDataLoaded: true,
                            executionReportData: data.data,
                            executionReportImgsBefore: imgUrlsBefore,
                            executionReportImgsAfter: imgUrlsAfter
                        });
                    }
                });
            });
    }
    delete(e) {
        e.preventDefault();
        console.log('del: ' + this.props.applications.applicationsDetailData._id);
    }
    getDateOfCreation(date) {
        if (date) {
            let dateObj = new Date(date);
            let minutes = dateObj.getMinutes().toString().length === 1 ? '0' + dateObj.getMinutes() : dateObj.getMinutes();
            let dateStr = dateObj.toISOString().substring(0, 10) + ' ' + dateObj.getHours() + ':' + minutes;
            this.setState({dateOfCreateon:dateStr});
        }
    }
    toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
    toNav() {
        let location = [
            { path: '/applications', name: 'Заявки' },
            { path: this.props.location.pathname, name: this.props.applications.applicationsDetailData.customer.name }
        ];
        let controls = {
            addBtn: {
                display: true,
                name: 'Добавить заявку +',
                path: '/applications/add',
            },
            search: {
                display: true,
            }
        };
        this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
        this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
    }
    render() {
        return (
            <Wrapper>
                {this.props.applications.applicationsDetailData ?
                    <Container>
                        <NavLink style={{textDecoration: 'none', color: '#212121'}} exact={true} to={{ pathname: '/applications' }}>
                            <i className="fas fa-angle-left" style={{marginRight: '10px'}}/>
                            {this.props.applications.applicationsDetailData.hasOwnProperty('customer') ?
                                'Заявка ' + this.props.applications.applicationsDetailData.customer.name + ' #' + this.props.applications.applicationsDetailData._id
                                : '- - -'}
                        </NavLink>
                        <Separator style={{marginTop: '20px'}}>Основная информация</Separator>
                        <ItemCont>
                            <ItemName>Заказчик</ItemName>
                            {this.props.applications.applicationsDetailData.hasOwnProperty('customer') ?
                                <StyledValueLink exact={true} to={{ pathname: '/customers/' + this.props.applications.applicationsDetailData.customer.id }}>{this.props.applications.applicationsDetailData.customer.name}</StyledValueLink>
                                : '- - -'}
                        </ItemCont>
                        <ItemCont>
                            <ItemName>Текущий этап</ItemName>
                            <ItemValue>
                                {this.props.applications.applicationsDetailData.hasOwnProperty('stages') ? this.props.applications.applicationsDetailData.stages[this.props.applications.applicationsDetailData.stages.current].name : '- - -'}
                            </ItemValue>
                        </ItemCont>
                        <ItemCont>
                            <ItemName>Статус</ItemName>
                            <ItemValue>
                                {this.props.applications.applicationsDetailData.hasOwnProperty('stages') ? this.props.applications.applicationsDetailData.stages[this.props.applications.applicationsDetailData.stages.current].status.name : '- - -'}
                            </ItemValue>
                        </ItemCont>
                        <ItemCont>
                            <ItemName>Дата создания</ItemName>
                            <ItemValue>
                                {this.props.applications.applicationsDetailData.hasOwnProperty('dateOfCreateon') ? this.state.dateOfCreateon : '- - -'}
                            </ItemValue>
                        </ItemCont>
                        <Separator style={{marginTop: '40px'}}>Информация о видах работ</Separator>
                        <ItemCont style={{width: '200px'}}>
                            <ItemName>Виды работ</ItemName>
                            {this.props.applications.applicationsDetailData.hasOwnProperty('work') ?
                                this.props.applications.applicationsDetailData.work.map(el => {
                                    return (
                                        <ItemValue key={el.id}>
                                            {el.name} {el.items.map(elNames => {
                                            /*return (
                                              <DataItemAdditionalValue key={elNames.id}>
                                                - {elNames.name} ({elNames.value} {elNames.unit})
                                              </DataItemAdditionalValue>
                                            )*/
                                            return (
                                                <DataItemAdditionalValue key={elNames.id}>
                                                    - {elNames.name}
                                                </DataItemAdditionalValue>
                                            )
                                        })}
                                        </ItemValue>
                                    )
                                })
                                : '- - -'}
                        </ItemCont>


                        {this.state.lookDataLoaded ?
                            <DataCont>
                                <Separator style={{marginTop: '40px'}}>Осмотр</Separator>
                                <ItemCont>
                                    <ItemName>Статус</ItemName>
                                    <ItemValue>
                                        {this.state.lookData.hasOwnProperty('status') && this.state.lookData.status.hasOwnProperty('name') ? this.state.lookData.status.name : '- - -'}
                                    </ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Исполнитель</ItemName>
                                    <ItemValue>
                                        {this.state.lookData.hasOwnProperty('performer') && this.state.lookData.performer.hasOwnProperty('name') ? this.state.lookData.performer.name : '- - -'}
                                    </ItemValue>
                                </ItemCont>
                                <ItemCont style={{width: '200px'}}>
                                    <ItemName>Виды работ</ItemName>
                                    {this.state.lookReportData.hasOwnProperty('work') ?
                                        this.state.lookReportData.work.map(el => {
                                            return (
                                                <ItemValue key={el.id}>
                                                    {el.name} {el.items.map(elNames => {
                                                    return (
                                                        <DataItemAdditionalValue key={elNames.id}>
                                                            - {elNames.name} ({elNames.value} {elNames.unit})
                                                        </DataItemAdditionalValue>
                                                    )
                                                })}
                                                </ItemValue>
                                            )
                                        })
                                        : '- - -'}
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Примечания</ItemName>
                                    <ItemValue>
                                        {this.state.lookReportData.hasOwnProperty('other') ? this.state.lookReportData.other : '- - -'}
                                    </ItemValue>
                                </ItemCont>
                                {this.state.lookReportDataLoaded ?
                                    <DataCont style={{marginTop: '20px'}}>
                                        {this.state.lookReportImgs.length ?
                                            this.state.lookReportImgs.map((imgEl, imgElI) => {
                                                return <img key={imgElI} style={{width: '100px', marginRight: '10px', cursor: 'pointer', borderRadius: '10px'}} src={imgEl} alt='performer look report'/>
                                            })
                                            : null}
                                    </DataCont>
                                    : null}
                            </DataCont>
                            : null}


                        {this.state.executionDataLoaded ?
                            <DataCont>
                                <Separator style={{marginTop: '40px'}}>Выполнение работ</Separator>
                                <ItemCont>
                                    <ItemName>Статус</ItemName>
                                    <ItemValue>
                                        {this.state.executionData.hasOwnProperty('status') && this.state.executionData.status.hasOwnProperty('name') ? this.state.executionData.status.name : '- - -'}
                                    </ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Исполнитель</ItemName>
                                    <ItemValue>
                                        {this.state.executionData.hasOwnProperty('performer') && this.state.executionData.performer.hasOwnProperty('name') ? this.state.executionData.performer.name : '- - -'}
                                    </ItemValue>
                                </ItemCont>
                                <ItemCont style={{width: '200px'}}>
                                    <ItemName>Виды работ</ItemName>
                                    {this.state.executionReportData.hasOwnProperty('work') ?
                                        this.state.executionReportData.work.map(el => {
                                            return (
                                                <ItemValue key={el.id}>
                                                    {el.name} {el.items.map(elNames => {
                                                    return (
                                                        <DataItemAdditionalValue key={elNames.id}>
                                                            - {elNames.name} ({elNames.value} {elNames.unit})
                                                        </DataItemAdditionalValue>
                                                    )
                                                })}
                                                </ItemValue>
                                            )
                                        })
                                        : '- - -'}
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Примечания</ItemName>
                                    <ItemValue>
                                        {this.state.executionReportData.hasOwnProperty('other') ? this.state.lookReportData.other : '- - -'}
                                    </ItemValue>
                                </ItemCont>
                                {this.state.executionReportDataLoaded ?
                                    <DataCont style={{marginTop: '20px'}}>
                                        <ItemName style={{width: '100px'}}>До</ItemName>
                                        {this.state.executionReportImgsBefore.length ?
                                            this.state.executionReportImgsBefore.map((imgEl, imgElI) => {
                                                return <img key={imgElI} style={{width: '100px', marginRight: '10px', cursor: 'pointer', borderRadius: '10px'}} src={imgEl} alt='performer look report'/>
                                            })
                                            : null}
                                    </DataCont>
                                    : null}
                                {this.state.executionReportDataLoaded ?
                                    <DataCont style={{marginTop: '20px'}}>
                                        <ItemName style={{width: '100px'}}>Псоле</ItemName>
                                        {this.state.executionReportImgsAfter.length ?
                                            this.state.executionReportImgsAfter.map((imgEl, imgElI) => {
                                                return <img key={imgElI} style={{width: '100px', marginRight: '10px', cursor: 'pointer', borderRadius: '10px'}} src={imgEl} alt='performer look report'/>
                                            })
                                            : null}
                                    </DataCont>
                                    : null}
                            </DataCont>
                            : null}


                        <BtnsCont>
                            <EditButton path='/'/>
                            <DeleteButton callback={this.delete}/>
                        </BtnsCont>


                    </Container>
                    : '- - -'}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  padding: 4px;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`;
const Container = styled.div`
  background-color: #ffffff;
  width: calc(100% - 20px);
  padding: 10px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  flex-wrap: wrap;
`;
const DataCont = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  flex-wrap: wrap;
`;
const Separator = styled.div`
  width: 100%;
  font-size: 16px;
  color: #212121;
  font-weight: 900;
  margin-bottom: 20px;
`;
const ItemCont = styled.div`
  width: 200px;
`;
const ItemName = styled.div`
  font-size: 12px;
  color: #757575;
`;
const ItemValue = styled.div`
  font-size: 14px;
  color: #212121;
`;
const StyledValueLink = styled(NavLink)`
  font-size: 14px;
  color: #212121;
  &:hover {
    color: #757575;
  }
`;
const DataItemAdditionalValue = styled.div`
  padding-left: 10px;
  font-size: 12px;
`;
const BtnsCont = styled.div`
  width: 100%;
  margin-top: 20px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps, null, null, { pure: false })(ApplicationsDetailWithRouter);
