import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../../reducers';
import {NavLink, withRouter} from 'react-router-dom';
import styled from 'styled-components';
//import DeleteButton from '../buttons/deleteButton';
//import EditButton from '../buttons/editButton';

const ApplicationsDetailWithRouter = withRouter(props => <ApplicationDetail {...props}/>);

class ApplicationDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {dataReady: false};
        this.getData = this.getData.bind(this);
        this.preparingDataForDisplay = this.preparingDataForDisplay.bind(this);
        this.toNav = this.toNav.bind(this);
    }

    componentDidMount() {
        store.dispatch({type: 'APPLICATIONS_DETAIL_DATA', applicationsDetailData: {}});
        this.getData();
    }

    getData() {
        let path = '/api/applications/' + this.props.match.params.id;
        fetch(path, {method: 'POST'})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => {
                console.log(resJson);
                this.preparingDataForDisplay(resJson);
            });
    }

    preparingDataForDisplay(rawData) {
        console.log(rawData);
        if (rawData.data.startingStage === 'look') {
            let getDataPromises = [ApplicationDetail.getCustomerData(rawData.data.stages.lookInit.customer), ApplicationDetail.getObjectData(rawData.data.stages.lookInit.object)];
            if (rawData.data.stages.hasOwnProperty('lookReadyToStart')) getDataPromises.push(ApplicationDetail.getPerformerData(rawData.data.stages.lookReadyToStart.performer));
            Promise.all(getDataPromises)
                .then(res => {
                    if (rawData.data.stages.hasOwnProperty('lookComplete')) rawData.data.stages.lookComplete.date = ApplicationDetail.getStringFromDate(rawData.data.stages.lookComplete.date);
                    let obj = {
                        id: rawData.data._id,
                        customer: res[0],
                        object: res[1],
                        performer: rawData.data.stages.hasOwnProperty('lookReadyToStart') ? res[2] : null,
                        startingStage: rawData.data.startingStage,
                        currentStage: rawData.data.currentStage,
                        stages: rawData.data.stages,
                        dateOfCreateon: ApplicationDetail.getStringFromDate(rawData.data.dates.create),
                        updateDate: ApplicationDetail.getStringFromDate(rawData.data.dates.update)
                    };
                    console.log(obj);
                    store.dispatch({type: 'APPLICATIONS_DETAIL_DATA', applicationsDetailData: obj});
                    this.setState({dataReady: true});
                    this.toNav();
                })
        }
        else if (rawData.data.startingStage === 'exec') {
            let getDataPromises = [ApplicationDetail.getCustomerData(rawData.data.stages.execInit.customer), ApplicationDetail.getObjectData(rawData.data.stages.execInit.object)];
            if (rawData.data.stages.hasOwnProperty('execReadyToStart')) getDataPromises.push(ApplicationDetail.getPerformerData(rawData.data.stages.execReadyToStart.performer));
            Promise.all(getDataPromises)
                .then(res => {
                    let obj = {
                        id: rawData.data._id,
                        customer: res[0],
                        object: res[1],
                        performer: rawData.data.stages.hasOwnProperty('execReadyToStart') ? res[2] : null,
                        startingStage: rawData.data.startingStage,
                        currentStage: rawData.data.currentStage,
                        stages: rawData.data.stages,
                        dateOfCreateon: ApplicationDetail.getStringFromDate(rawData.data.dates.create),
                        updateDate: ApplicationDetail.getStringFromDate(rawData.data.dates.update)
                    };
                    console.log(obj);
                    store.dispatch({type: 'APPLICATIONS_DETAIL_DATA', applicationsDetailData: obj});
                    this.setState({dataReady: true});
                    this.toNav();
                })
        }
    }

    static getCustomerData(id) {return fetch('/api/customers/' + id, {method: 'POST'}).then(customerRes => customerRes.json()).then(customerResJson => customerResJson)}
    static getObjectData(id) {return fetch('/api/objects/' + id, {method: 'POST'}).then(objectRes => objectRes.json()).then(objectResJson => objectResJson)}
    static getPerformerData(id) {return fetch('/api/performers/' + id, {method: 'POST'}).then(performerRes => performerRes.json()).then(performerResJson => performerResJson.data)}

    static getStringFromDate(date) {
        let dateObj = new Date(date);
        let minutes = dateObj.getMinutes().toString().length === 1 ? '0' + dateObj.getMinutes() : dateObj.getMinutes();
        return dateObj.toISOString().substring(0, 10) + ' ' + dateObj.getHours() + ':' + minutes;
    }

    toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/applications', name: 'Заявки'}, {path: this.props.location.pathname, name: this.props.applications.applicationsDetailData.customer.data.name}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: true, name: 'Добавить заявку', path: '/applications/add'}, search: {display: true}}});
    }

    render() {
        return (
            <Wrapper>
                {
                    this.state.dataReady ?
                        <Container>

                            <DataCont>
                                <ItemCont>
                                    <ItemName>Заказчик</ItemName>
                                    <ItemValue>{this.props.applications.applicationsDetailData.customer.data.name}</ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Объект</ItemName>
                                    <ItemValue>{this.props.applications.applicationsDetailData.object.data.address}</ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Текущий этап</ItemName>
                                    <ItemValue>
                                        {this.props.applications.applicationsDetailData.currentStage === 'lookInit' ? 'Подготовка к осмотру' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'lookReadyToStart' ? 'Готов к осмотру' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'lookStart' ? 'Идет осмотр' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'lookComplete' ? 'Осмотр завершен' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'execInit' ? 'Подготовка к выполнению' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'execReadyToStart' ? 'Готов к выполнению' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'execStart' ? 'Идет выполнение' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'execComplete' ? 'Выполнение завершено' : null}
                                    </ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Начальный этап</ItemName>
                                    <ItemValue>
                                        {this.props.applications.applicationsDetailData.startingStage === 'look' ? 'Осмотр' : null}
                                        {this.props.applications.applicationsDetailData.startingStage === 'exec' ? 'Выполнение' : null}
                                    </ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Дата добавления</ItemName>
                                    <ItemValue>{this.props.applications.applicationsDetailData.dateOfCreateon}</ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Дата обновления</ItemName>
                                    <ItemValue>{this.props.applications.applicationsDetailData.updateDate}</ItemValue>
                                </ItemCont>
                            </DataCont>

                            {
                                this.props.applications.applicationsDetailData.stages.hasOwnProperty('lookReadyToStart') ?
                                    <DataCont>
                                        <DataContTitle>Осмотр</DataContTitle>
                                        {
                                            this.props.applications.applicationsDetailData.performer ?
                                                <ItemCont>
                                                    <ItemName>Исполнитель</ItemName>
                                                    <ItemValue>{this.props.applications.applicationsDetailData.performer.name}</ItemValue>
                                                </ItemCont>
                                                : null
                                        }
                                        {
                                            this.props.applications.applicationsDetailData.stages.hasOwnProperty('lookComplete') ?
                                                <ItemCont>
                                                    <ItemName>Дата завершения</ItemName>
                                                    <ItemValue>{this.props.applications.applicationsDetailData.stages.lookComplete.date}</ItemValue>
                                                </ItemCont>
                                                : null
                                        }
                                    </DataCont>
                                    :null
                            }

                        </Container>
                        : null
                }
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  padding: 4px;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`;
const Container = styled.div`
  background-color: #ffffff;
  width: calc(100% - 20px);
  padding: 10px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  flex-wrap: wrap;
`;
const DataCont = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  flex-wrap: wrap;
`;
const DataContTitle = styled.div`
    padding: 20px 0;
    width: 100%;
    font-size: 16px;
    font-weight: 900;
    color: #212121;
`;
const Separator = styled.div`
  width: 100%;
  font-size: 16px;
  color: #212121;
  font-weight: 900;
  margin-bottom: 20px;
`;
const ItemCont = styled.div`
  width: 200px;
`;
const ItemName = styled.div`
  font-size: 12px;
  color: #757575;
`;
const ItemValue = styled.div`
  font-size: 14px;
  color: #212121;
`;
const StyledValueLink = styled(NavLink)`
  font-size: 14px;
  color: #212121;
  &:hover {
    color: #757575;
  }
`;
const DataItemAdditionalValue = styled.div`
  padding-left: 10px;
  font-size: 12px;
`;
const BtnsCont = styled.div`
  width: 100%;
  margin-top: 20px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps, null, null, {pure: false})(ApplicationsDetailWithRouter);
