import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../../reducers';
import styled from 'styled-components';

class ApplicationsAddCustomerInput extends Component {
    constructor(props) {
        super(props);
        this.state = {customerFromBase: true, customerName: '', customerAddress: '', customerContactName: '', customerContactPhone: '', customerContactPost: ''};
        this.getCustomerData = this.getCustomerData.bind(this);
        this.customerSelect = this.customerSelect.bind(this);
        this.setInputValue = this.setInputValue.bind(this);
        this.selectCustomerSource = this.selectCustomerSource.bind(this);
        this.innerCustomerData = this.innerCustomerData.bind(this);
        this.customerTextInputChange = this.customerTextInputChange.bind(this);
        this.toStore = this.toStore.bind(this);
    }
    componentDidMount() {this.getCustomerData()}
    getCustomerData() {
        let path = '/api/customers';
        fetch(path, {method: 'POST'})
        .then(res => {
            if (res.status !== 200) {
                console.log('Err: ' + res.statusText + ' ' + res.status);
                return;
            };
            res.json().then((json) => {
                json.data = json.data.map((el) => {
                    el.active = false;
                    return el;
                });
                this.toStore('APPLICATIONS_ADD_CUSTOMER_INPUT_DATA', 'applicationsAddCustomerInputData', json.data);
            });
        });
    }
    customerSelect(e, id) {
        let data = this.props.applications.applicationsAddCustomerInputData;
        let newData = data.map((el) => {
            if (id === el._id) el.active = !el.active;
            else el.active = false;
            return el;
        });
        this.toStore('APPLICATIONS_ADD_CUSTOMER_INPUT_DATA', 'applicationsAddCustomerInputData', newData);
        this.setInputValue(newData);
    }
    setInputValue(data) {
        let cleanData = data.filter((el) => {return el.active});
        let objForInputs = {};
        let value = '';
        if (cleanData.length) {
            objForInputs = {
                name: cleanData[0].name,
                factAddress: cleanData[0].factAddress,
                contact: {
                    name: cleanData[0].contact.name,
                    phone: cleanData[0].contact.phone,
                    post: cleanData[0].contact.post,
                },
            };
            value = cleanData[0]._id;
        }
        this.innerCustomerData(objForInputs);
        this.toStore('APPLICATIONS_ADD_CUSTOMER_INPUT_VALUE', 'applicationsAddCustomerInputValue', value);
    }
    selectCustomerSource(e) {
        let customerFromBase;
        if (e.target.value === 'true') customerFromBase = true;
        else if (e.target.value === 'false') {
            let data = this.props.applications.applicationsAddCustomerInputData;
            let newData = data.map((el) => {
                el.active = false;
                return el;
            });
            this.toStore('APPLICATIONS_ADD_CUSTOMER_INPUT_DATA', 'applicationsAddCustomerInputData', newData);
            customerFromBase = false;
        }
        this.innerCustomerData({});
        this.setState({customerFromBase:customerFromBase});
      }
    innerCustomerData(data) {
        this.setState({
            customerName: data.name ? data.name : '',
            customerAddress: data.factAddress ? data.factAddress : '',
            customerContactName: data.contact && data.contact.name ? data.contact.name : '',
            customerContactPhone: data.contact && data.contact.phone ? data.contact.phone : '',
            customerContactPost: data.contact && data.contact.post ? data.contact.post : '',
        });
    }
    customerTextInputChange(e, inputName) {this.setState({[inputName]: e.target.value})}
    toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
    render() {
        return (
            <Wrapper>
                <input name='customer' type='hidden' value={this.props.applications.applicationsAddCustomerInputValue}></input>
                {/*<InputCont>
                    <select style={{width: '150px'}} name='customerFromDB' onChange={this.selectCustomerSource}>
                    <option value='true'>Существующий</option>
                    <option value='false'>Новый</option>
                    </select>
                </InputCont>
                <Separator></Separator>*/}
                {/*this.state.customerFromBase ?
                    <CustomerConts>
                        <InputCont>
                            <InputLabel>Заказчик</InputLabel>
                            <SelectCont>
                                {this.props.applications.applicationsAddCustomerInputData.map((el, i) => {
                                    return (
                                        <SelectCollectionCont key={i}>
                                            {el.active ? <SelectCollectionTitleActive onClick={(e) => {this.customerSelect(e, el._id)}}>{el.name}</SelectCollectionTitleActive> : <SelectCollectionTitle onClick={(e) => {this.customerSelect(e, el._id)}}>{el.name}</SelectCollectionTitle>}
                                        </SelectCollectionCont>
                                    );
                                })}
                            </SelectCont>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Название</InputLabel>
                            <StyledInput type='text' readOnly value={this.state.customerName}></StyledInput>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Адрес</InputLabel>
                            <StyledInput type='text' readOnly value={this.state.customerAddress}></StyledInput>
                        </InputCont>
                        <Separator>Контактное лицо</Separator>
                        <InputCont>
                            <InputLabel>ФИО</InputLabel>
                            <StyledInput type='text' readOnly value={this.state.customerContactName}></StyledInput>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Телефон</InputLabel>
                            <StyledInput type='text' readOnly value={this.state.customerContactPhone}></StyledInput>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Должность</InputLabel>
                            <StyledInput type='text' readOnly value={this.state.customerContactPost}></StyledInput>
                        </InputCont>
                    </CustomerConts> 
                : 
                    <CustomerConts>
                        <InputCont>
                            <InputLabel>Название</InputLabel>
                            <StyledInput name='customerName' onChange={(e) => {this.customerTextInputChange(e, 'customerName')}} type='text' value={this.state.customerName}></StyledInput>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Адрес</InputLabel>
                            <StyledInput name='customerAddress' onChange={(e) => {this.customerTextInputChange(e, 'customerAddress')}} type='text' value={this.state.customerAddress}></StyledInput>
                        </InputCont>
                        <Separator>Контактное лицо</Separator>
                        <InputCont>
                            <InputLabel>ФИО</InputLabel>
                            <StyledInput name='customerContactName' onChange={(e) => {this.customerTextInputChange(e, 'customerContactName')}} type='text' value={this.state.customerContactName}></StyledInput>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Телефон</InputLabel>
                            <StyledInput name='customerContactPhone' onChange={(e) => {this.customerTextInputChange(e, 'customerContactPhone')}} type='text' value={this.state.customerContactPhone}></StyledInput>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Должность</InputLabel>
                            <StyledInput name='customerContactPost' onChange={(e) => {this.customerTextInputChange(e, 'customerContactPost')}} type='text' value={this.state.customerContactPost}></StyledInput>
                        </InputCont>
                    </CustomerConts>
                            */}
                <CustomerConts>
                        <InputCont>
                            <InputLabel>Заказчик</InputLabel>
                            <SelectCont>
                                {this.props.applications.applicationsAddCustomerInputData.map((el, i) => {
                                    return (
                                        <SelectCollectionCont key={i}>
                                            {el.active ? <SelectCollectionTitleActive onClick={(e) => {this.customerSelect(e, el._id)}}>{el.name}</SelectCollectionTitleActive> : <SelectCollectionTitle onClick={(e) => {this.customerSelect(e, el._id)}}>{el.name}</SelectCollectionTitle>}
                                        </SelectCollectionCont>
                                    );
                                })}
                            </SelectCont>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Название</InputLabel>
                            <StyledInput type='text' readOnly value={this.state.customerName}></StyledInput>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Адрес</InputLabel>
                            <StyledInput type='text' readOnly value={this.state.customerAddress}></StyledInput>
                        </InputCont>
                        <Separator>Контактное лицо</Separator>
                        <InputCont>
                            <InputLabel>ФИО</InputLabel>
                            <StyledInput type='text' readOnly value={this.state.customerContactName}></StyledInput>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Телефон</InputLabel>
                            <StyledInput type='text' readOnly value={this.state.customerContactPhone}></StyledInput>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Должность</InputLabel>
                            <StyledInput type='text' readOnly value={this.state.customerContactPost}></StyledInput>
                        </InputCont>
                    </CustomerConts> 
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    position: relative;
`;
const SelectCont = styled.div`
    position: relative;
    width: 400px;
    height: 200px;
    background-color: #ffffff;
    border: 1px solid #eeeeee;
    overflow: scroll;
`;
const SelectCollectionCont = styled.div``;
const SelectCollectionTitle = styled.div`
    padding: 5px;
    font-size: 14px;
    color: #212121;
    font-weight: 900;
    cursor: pointer;
    &:hover {
        background-color: #eeeeee;
    }
`;
const SelectCollectionTitleActive = styled.div`
    padding: 5px;
    font-size: 14px;
    color: #ffffff;
    font-weight: 900;
    cursor: pointer;
    background-color: #bdbdbd;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const StyledInput = styled.input``;
const InputLabel = styled.div`
    font-size: 14px;
    color: #757575;
`;
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 16px;
`;
const CustomerConts = styled.div`
    width: 100%;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;

const mapStateToProps = (store) => {return {applications: store.applications}};
export default connect(mapStateToProps)(ApplicationsAddCustomerInput);
