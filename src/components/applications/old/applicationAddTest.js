import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../../reducers';
import { withRouter, NavLink } from 'react-router-dom';
import styled from 'styled-components';

import ApplicationsAddCustomerInput from './applicationsAddCustomerInputTest';
import ApplicationsAddLookWorkInput from './applicationAddLookWorkInputTest';
import ApplicationsAddExecutionWorkInput from './applicationAddExecutionWorkInputTest';
import ApplicationsAddPerformerInput from './applicationsAddPerformerInput';

import ApplicationsAddUrCustomer from '../applicationAddCustomerNewUr';
import ApplicationsAddFizCustomer from '../applicationAddCustomerNewFiz';

const ApplicationsAddWithRouter = withRouter(props => <ApplicationsAdd {...props}/>);

class ApplicationsAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startingStage: '- - -',
            //customerData: [],
            customerType: 'fiz', 
            customerSource: 'new', 
            customerData: {},
            performerNextStage: false,
            objectSource: 'new',
            summ: '0',
            summCustomer: '0',
            // ========== DATA FOR SEND
            dataForSend: {
                startFrom: 0,
                // ========== CUSTOMER DATA
                customer: {
                    dataSource: 'new',
                    new: {
                        type: 'fiz',
                        fiz: {
                            name: '',
                            phone: '',
                            mail: '',
                        },
                        ur: {},
                    },
                    db: {id: ''},
                },
                // ========== OBJECT DATA
                object: {
                    dataSource: 'new',
                    new: {
                        city: '',
                        address: '',
                        other: '',
                    },
                    db: {id: ''},
                },
                // ========== LOOK DATA
                look: {
                    toExecutionByPerformer: false,
                    work: [],
                    other: '',
                    startDate: '',
                    costCustomer: '500',
                    payToPerformer: '250',
                },
                // ========== EXECUTION DATA
                execution: {
                    dataSource: 'new',
                    new: {
                        work: [],
                        photo: [],
                        other: '',
                    },
                    db: {
                        id: '',
                        other: '',
                    },
                    startDate: '',
                    costCustomer: '0',
                    payToPerformer: '0',
                },
                // ========== PERFORMER DATA
                performer: {
                    selectionMethod: 'auto',
                    selectionList: [],
                },
            },
        };
        this.urCustomerInputChange = this.urCustomerInputChange.bind(this);
        this.fizCustomerInputChange = this.fizCustomerInputChange.bind(this);
        this.applicationStartingStageSelect = this.applicationStartingStageSelect.bind(this);
        this.customerTypeSelect = this.customerTypeSelect.bind(this);
        this.customerSourceSelect = this.customerSourceSelect.bind(this);
        this.performerNextStageSelect = this.performerNextStageSelect.bind(this);
        this.objectSourceSelect = this.objectSourceSelect.bind(this);
        this.priceChange = this.priceChange.bind(this);
        this.lookWorkInputSelect = this.lookWorkInputSelect.bind(this);
        this.ojectInputChange = this.ojectInputChange.bind(this);
        this.otherChange = this.otherChange.bind(this);
        this.send = this.send.bind(this);
        this.toDraft = this.toDraft.bind(this);
        this.toNav = this.toNav.bind(this);
        this.toStore = this.toStore.bind(this);
    }
    componentDidMount() {this.toNav()}
    urCustomerInputChange(data) {
        let dataForSend = this.state.dataForSend;
        dataForSend.customer.new.ur = data;
        this.setState({
            customerData: data,
            dataForSend: dataForSend
        })
    }
    fizCustomerInputChange(data) {
        let dataForSend = this.state.dataForSend;
        dataForSend.customer.new.fiz = data;
        this.setState({
            customerData: data,
            dataForSend: dataForSend,
        })
    }
    applicationStartingStageSelect(e) {
        let dataForSend = this.state.dataForSend;
        if (e.target.value === 'look') {
            dataForSend.startFrom = 1;
            dataForSend.execution.dataSource = 'new';
        } else if (e.target.value === 'execution') {
            dataForSend.startFrom = 4;
            dataForSend.execution.dataSource = 'new';
        } else if (e.target.value === 'executionFromLook') {
            dataForSend.startFrom = 4;
            dataForSend.execution.dataSource = 'db';
        }
        this.setState({
            startingStage: e.target.value,
            dataForSend: dataForSend,
        });
    }
    customerTypeSelect(e) {
        let dataForSend = this.state.dataForSend;
        dataForSend.customer.new.type = e.target.value;
        this.setState({
            customerType:e.target.value,
            dataForSend: dataForSend,
        })
    }
    customerSourceSelect(e) {
        let dataForSend = this.state.dataForSend;
        dataForSend.customer.dataSource = e.target.value;
        this.setState({
            customerSource:e.target.value,
            dataForSend: dataForSend,
        })
    }
    performerNextStageSelect(e) {
        let dataForSend = this.state.dataForSend;
        dataForSend.look.toExecutionByPerformer = e.target.checked;
        this.setState({
            performerNextStage:e.target.checked,
            dataForSend: dataForSend,
        });
    }
    objectSourceSelect(e) {
        this.setState({
            objectSource: e.target.value,
        });
    }
    priceChange(summ, summCustomer) {
        let dataForSend = this.state.dataForSend;
        dataForSend.execution.payToPerformer = summ;
        dataForSend.execution.costCustomer = summCustomer;
        this.setState({
            dataForSend: dataForSend,
            summ: summ,
            summCustomer: summCustomer,
        });
    }
    lookWorkInputSelect(items) {
        let dataForSend = this.state.dataForSend;
        dataForSend.look.work = items;
        this.setState({dataForSend:dataForSend});
    }
    ojectInputChange(e, name) {
        let dataForSend = this.state.dataForSend;
        dataForSend.object.new[name] = e.target.value;
        this.setState({dataForSend:dataForSend});
    }
    otherChange(e) {
        let dataForSend = this.state.dataForSend;
        if (this.state.startingStage === 'look') dataForSend.look.other = e.target.value;
        else if (this.state.startingStage === 'execution' || this.state.startingStage === 'executionFromLook') dataForSend.execution.other = e.target.value;
        this.setState({dataForSend:dataForSend});
    }
    send(e) {
        e.preventDefault();
        console.log(this.state.dataForSend);
        let dataForSend = this.state.dataForSend;
        let path = '/api/applications/add';
        fetch(path, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dataForSend)
        }).then(res => {
            if (res.status !== 200) {
                console.log('Err: ' + res.statusText + ' ' + res.status);
                return;
            }
            res.json().then((data) => {
                console.log(data);
                if (data.status === 'ok') this.setState({sendResStatus: data.status})
            });
        });
    }
    toDraft(e) {
        e.preventDefault();
        console.log(this.state.dataForSend);
    }
    toNav() {
        let location = [
        { path: '/applications', name: 'Заявки' },
        { path: '/applications/add', name: 'Добавление' }
        ];
        let controls = {
        addBtn: {
            display: false,
            name: '',
            path: '',
        },
        search: {
            display: true,
        }
        };
        this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
        this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
    }
    toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
    render() {
    return (
        <Wrapper>
            <Container>
                <NavLink style={{textDecoration: 'none', color: '#212121'}} exact={true} to={{ pathname: '/applications' }}>
                    <i className="fas fa-angle-left" style={{marginRight: '10px'}}/> Новая заявка
                </NavLink>
                <Separator>#1 Выберите начальный этап</Separator>
                <InputCont>
                    <select onChange={this.applicationStartingStageSelect} value={this.state.startingStage} style={{width: '270px'}} name='stage'>
                        <option value='- - -'>- - -</option>
                        <option value='look'>Осмотр</option>
                        <option value='execution'>Выполнение работ</option>
                        <option value='executionFromLook'>Выполнение работ на основе Осмотра</option>
                    </select>
                </InputCont>
                {this.state.startingStage === 'look' ?
                    <InputCont style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                        <StyledInputCheckbox onChange={this.performerNextStageSelect} checked={this.state.performerNextStage} name='performerNextStage' type='checkbox'/>
                        <InputLabel>Разрешить Исполнителю приступить к Выполнению работ после проведения Осмотра</InputLabel>
                    </InputCont>
                :null}
                {this.state.startingStage === 'look' || this.state.startingStage === 'execution' ?
                    <Block>
                        <Separator>#2 Внесите данные о заказчике</Separator>
                        <InputCont>
                            <select onChange={this.customerSourceSelect} value={this.state.customerSource} style={{width: '270px'}} name='type'>
                                <option value='new'>Новый</option>
                                <option value='db'>Существующий</option>
                            </select>
                        </InputCont>
                        {this.state.customerSource === 'new' ?
                            <Block>
                                <InputCont>
                                    <InputLabel>Тип заказчика</InputLabel>
                                    <select onChange={this.customerTypeSelect} value={this.state.customerType} style={{width: '270px'}} name='type'>
                                        <option value='fiz'>Физ. лицо</option>
                                        <option value='ur'>Юр. лицо</option>
                                    </select>
                                </InputCont>
                                {this.state.customerType === 'fiz' ? <ApplicationsAddFizCustomer callback={this.fizCustomerInputChange}/> :null}
                                {this.state.customerType === 'ur' ? <ApplicationsAddUrCustomer callback={this.urCustomerInputChange}/> :null}
                            </Block>
                        :null}
                        {this.state.customerSource === 'db' ? <ApplicationsAddCustomerInput/> :null}
                        <Separator style={{marginTop: '20px'}}>#3 Внесите данные о объекте</Separator>
                        <InputCont>
                            <select onChange={this.objectSourceSelect} value={this.state.objectSource} style={{width: '270px'}} name='type'>
                                <option value='new'>Новый</option>
                                <option value='db'>Существующий</option>
                            </select>
                        </InputCont>
                        {this.state.objectSource === 'new' ?
                            <Block style={{width: '100%', display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                                <InputCont>
                                    <InputLabel>Город</InputLabel>
                                    <StyledInput onChange={(e) => this.ojectInputChange(e, 'city')} name='city' type='text'/>
                                </InputCont>
                                <InputCont>
                                    <InputLabel>Адрес</InputLabel>
                                    <StyledInput onChange={(e) => this.ojectInputChange(e, 'address')} name='address' type='text'/>
                                </InputCont>
                                <InputCont>
                                    <InputLabel>Примечания</InputLabel>
                                    <StyledInput onChange={(e) => this.ojectInputChange(e, 'other')} style={{width: '270px'}} name='other' type='text'/>
                                </InputCont>
                            </Block>
                        :null}
                        {this.state.objectSource === 'db' ?
                            <Block>
                                <InputCont>
                                    <InputLabel>Выберите из списка</InputLabel>
                                </InputCont>
                            </Block>
                        :null}
                    </Block>
                :null}
                {this.state.startingStage === 'look' ?
                    <Block style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                        <Separator>#4 Укажите виды работ</Separator>
                        <ApplicationsAddLookWorkInput select={this.lookWorkInputSelect}/>
                        <Separator>#5 Выберите исполнителя</Separator>
                        <ApplicationsAddPerformerInput/>
                    </Block>
                :null}
                {this.state.startingStage === 'execution' ?
                    <Block style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                        <Separator>#4 Укажите виды работ и их объем</Separator>
                        <ApplicationsAddExecutionWorkInput priceChange={this.priceChange}/>
                        <Separator>#5 Выберите исполнителя</Separator>
                        <ApplicationsAddPerformerInput/>
                    </Block>
                :null}
                {this.state.startingStage === 'look' || this.state.startingStage === 'execution' || this.state.startingStage === 'executionFromLook' ?
                    <Block style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                        <Separator>#6 Укажите детали</Separator>
                        <InputCont>
                            <InputLabel>Дата</InputLabel>
                            <StyledInput onChange={this.setDate} defaultValue={this.state.defaultLookRequiredDate} name='date' type='date'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Время</InputLabel>
                            <StyledInput onChange={this.setTime} defaultValue='12:00' type="time" name="time"/>
                        </InputCont>
                        {this.state.startingStage === 'look' || this.state.startingStage === 'executionFromLook' ?
                            <Block style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                                <InputCont>
                                    <InputLabel style={{fontSize: '10px', height: '18px'}}>Оплата исполнителю</InputLabel>
                                    <StyledInput defaultValue='250' name='payToPerformer' type='text'/>
                                </InputCont>
                                <InputCont>
                                    <InputLabel style={{fontSize: '10px', height: '18px'}}>Стоимость для заказчика</InputLabel>
                                    <StyledInput defaultValue='500' name='costForCustomer' type='text'/>
                                </InputCont>
                            </Block>
                        :null}
                        {this.state.startingStage === 'execution' ?
                            <Block style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                                <InputCont>
                                    <InputLabel style={{fontSize: '10px', height: '18px'}}>Оплата исполнителю</InputLabel>
                                    <StyledInput readOnly value={this.state.summ} name='payToPerformer' type='text'/>
                                </InputCont>
                                <InputCont>
                                    <InputLabel style={{fontSize: '10px', height: '18px'}}>Стоимость для заказчика</InputLabel>
                                    <StyledInput readOnly value={this.state.summCustomer} name='costForCustomer' type='text'/>
                                </InputCont>
                            </Block>
                        :null}
                        <Separator>Дополнительно</Separator>
                        <InputCont>
                            <InputLabel>Примечания</InputLabel>
                            <StyledInput onChange={this.otherChange} style={{width: '270px'}} name='applicationOther' type='text'/>
                        </InputCont>
                        <Separator><SendBtn onClick={this.send} type='submit'>Приступить</SendBtn><ToDraftBtn onClick={this.toDraft} type='submit'>В черновики</ToDraftBtn></Separator>
                    </Block>
                :null}
            </Container>
        </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: calc(100% - 8px);
    padding: 4px;
    position: relative;
`;
const Container = styled.div`
    padding: 10px;
    background-color: #ffffff;
`;
const Block = styled.div``;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const StyledInput = styled.input``;
const StyledInputCheckbox = styled.input`
    width: 24px;
`;
const Separator = styled.div`
    margin-top: 40px;
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 18px;
`;
const SendBtn = styled.button`
    width: min-content;
    border-radius: 50px;
    font-size: 12px;
    padding: 10px;
    color: #ffffff;
    text-align: center;
    background-color: rgb(230,61,50);
    cursor: pointer;
`;
const ToDraftBtn = styled.button`
    margin-left: 10px;
    border-radius: 50px;
    font-size: 12px;
    padding: 10px;
    color: #212121;
    text-align: center;
    background-color: transparent;
    border: 1px solid #212121;
    outline: none;
    cursor: pointer;
    &:hover {
        border: 1px solid #757575;
        color: #757575;
    }
`;

const mapStateToProps = (store) => {return {applications: store.applications}};
export default connect(mapStateToProps)(ApplicationsAddWithRouter);
