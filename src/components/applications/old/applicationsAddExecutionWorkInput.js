import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../../reducers';
import styled from 'styled-components';

class ApplicationsAddExecutionWorkInput extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.workValueChange = this.workValueChange.bind(this);
        this.workNameSelect = this.workNameSelect.bind(this);
        this.setInputValue = this.setInputValue.bind(this);
        this.getWorkNamesData = this.getWorkNamesData.bind(this);
        this.getWorkTypesData = this.getWorkTypesData.bind(this);
        ApplicationsAddExecutionWorkInput.toStore = this.toStore.bind(this);
    }
    componentDidMount() {
        this.toStore('APPLICATIONS_ADD_WORK_INPUT_DATA', 'applicationsAddWorkInputData', []);
        this.getWorkTypesData();
    }
    workValueChange(e, id) {
        let inputData = this.props.applications.applicationsAddWorkInputData;
        let updatedInputData = inputData.map((inputDataEl) => {
            inputDataEl.workNames = inputDataEl.workNames.map((workNamesEl) => {
                if (workNamesEl.id === id) workNamesEl.value = parseInt(e.target.value, 10);
                return workNamesEl;
            });
            return inputDataEl;
        });
        this.toStore('APPLICATIONS_ADD_WORK_INPUT_DATA', 'applicationsAddWorkInputData', updatedInputData);
        console.log(updatedInputData);
        this.setInputValue(updatedInputData);
    }
    workNameSelect(e, workTypeId, id) {
        let data = this.props.applications.applicationsAddWorkInputData;
        data = data.map((el) => {
            if (el._id === workTypeId) {
                el.workNames = el.workNames.map((elItems) => {
                    if (elItems.id === id) elItems.active = !elItems.active;
                    return elItems;
                });
            }
            return el;
        });
        this.toStore('APPLICATIONS_ADD_WORK_INPUT_DATA', 'applicationsAddWorkInputData', data);
        this.setInputValue(data);
    }
    setInputValue(inptData) {
        /*let newCleanInptData = inptData.map(inptDataEl => {
            let newItems = inptDataEl.workNames.map(workNamesEl => {
                return {id: workNamesEl.id, active: workNamesEl.active, value: workNamesEl.value}
            });
            return {
                id: inptDataEl._id,
                items: newItems,
            }
        });*/
        let cleanInptData = inptData.map((el) => {
            return { 
                id: el._id, 
                items: el.workNames.map((elItems) => {
                    return { active: elItems.active, id: elItems.id, value: elItems.value };
                }),
            }
        });
        let filteredInptData = cleanInptData.filter((el) => {
            el.items = el.items.filter((elItems) => {
                return elItems.active;
            });
            return el.items.length;
        });
        let inputValue = filteredInptData.map((el) => {
            el.items = el.items.map((elItems) => {
                return {id: elItems.id, value: elItems.value}
            });
            return {id: el.id, items: el.items}
        });
        this.toStore('APPLICATIONS_ADD_WORK_INPUT_VALUE', 'applicationsAddWorkInputValue', inputValue);
    }
    getWorkNamesData() {
        let path = '/api/work/names';
        fetch(path, {method: 'POST'})
        .then(res => {
            if (res.status !== 200) {
                console.log('Err: ' + res.statusText + ' ' + res.status);
                return;
            };
            res.json().then((json) => {
                console.log(json);
                let workInputData = this.props.applications.applicationsAddWorkInputData;
                workInputData = workInputData.map((workInputDataEl) => {
                    workInputDataEl.workNames = workInputDataEl.workNames.map((workNamesEl) => {
                        json.data.map((jsonDataEl) => {
                            if (workNamesEl.id === jsonDataEl._id && jsonDataEl.hasOwnProperty('unit')) workNamesEl.unit = jsonDataEl.unit;
                            return jsonDataEl;
                        });
                        return workNamesEl;
                    });
                    return workInputDataEl;
                });
                this.toStore('APPLICATIONS_ADD_WORK_INPUT_DATA', 'applicationsAddWorkInputData', workInputData);
            });
        });
    }
    getWorkTypesData() {
        let path = '/api/work';
        fetch(path, {method: 'POST'})
        .then(res => {
            if (res.status !== 200) {
                console.log('Err: ' + res.statusText + ' ' + res.status);
                return;
            };
            res.json().then((json) => {
                if (json.data) {
                    json.data = json.data.map((el) => {
                        if (el.hasOwnProperty('workNames')) {
                            if (el.workNames.length) {
                                el.workNames = el.workNames.map((elData) => {
                                    elData.active = false;
                                    elData.value = 0;
                                    return elData;
                                });
                            }
                        }
                        return el;
                    });
                }
                this.toStore('APPLICATIONS_ADD_WORK_INPUT_DATA', 'applicationsAddWorkInputData', json.data);
                this.getWorkNamesData();
            });
        });
    }
    toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
    render() {
        return (
            <Wrapper>
                <input name='work' type='hidden' value={JSON.stringify(this.props.applications.applicationsAddWorkInputValue)}></input>
                <SelectCont>
                    {this.props.applications.applicationsAddWorkInputData.map((el) => {
                        return (
                            <SelectCollectionCont key={el._id}>
                                <SelectCollectionTitle>{el.name}</SelectCollectionTitle>
                                <SelectCollectionItemsCont>
                                    {el.workNames.map((elItems) => {
                                        return elItems.active ? <SelectCollectionItemActive onClick={(e) => {this.workNameSelect(e, el._id, elItems.id)}} key={elItems.id}>{elItems.name}</SelectCollectionItemActive> : <SelectCollectionItem onClick={(e) => {this.workNameSelect(e, el._id, elItems.id)}} key={elItems.id}>{elItems.name}</SelectCollectionItem>;
                                    })}
                                </SelectCollectionItemsCont>
                            </SelectCollectionCont>
                        );
                    })}
                </SelectCont>
                <WorkValueCont>
                {this.props.applications.applicationsAddWorkInputData.map((el) => {
                    return el.workNames.map((workNamesEl, workNamesI) => {
                        return workNamesEl.active ? (
                            <InputCont key={workNamesI}>
                                <InputLabel>{workNamesEl.name}</InputLabel>
                                <StyledInput onChange={(e) => {this.workValueChange(e, workNamesEl.id)}} defaultValue='0' type='number'/>
                                <InputUnitType>{workNamesEl.unit}</InputUnitType>
                            </InputCont>
                        ) 
                        : null;
                    });
                })}
                </WorkValueCont>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: row;
`;
const SelectCont = styled.div`
    position: relative;
    width: 400px;
    height: 200px;
    background-color: #ffffff;
    border: 1px solid #eeeeee;
    overflow: scroll;
`;
const SelectCollectionCont = styled.div``;
const SelectCollectionTitle = styled.div`
    padding: 5px;
    font-size: 14px;
    color: #212121;
    font-weight: 900;
    cursor: pointer;
`;
const SelectCollectionItemsCont = styled.div``;
const SelectCollectionItem = styled.div`
    padding: 5px;
    padding-left: 10px;
    font-size: 14px;
    color: #757575;
    cursor: pointer;
    &:hover {
        background-color: #eeeeee;
    }
`;
const SelectCollectionItemActive = styled.div`
    padding: 5px;
    padding-left: 10px;
    font-size: 14px;
    color: #eeeeee;
    cursor: pointer;
    background-color: #bdbdbd;
`;
const WorkValueCont = styled.div`
    margin-left: 20px;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
    display: flex;
    flex-firection: row;
`;
const InputLabel = styled.div`
    font-size: 14px;
    color: #757575;
    width: 150px;
`;
const StyledInput = styled.input``;
const InputUnitType = styled.div`
    font-size: 14px;
    color: #757575;
    margin-left: 5px;
`;

const mapStateToProps = (store) => {return {applications: store.applications}};
export default connect(mapStateToProps, null, null, { pure: false })(ApplicationsAddExecutionWorkInput);
