import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../../reducers';
import {/*NavLink,*/ withRouter} from 'react-router-dom';
import styled from 'styled-components';
import ToExecButton from '../buttons/toExecButton';
import ApplicationsDetailToExecData from './applicationDetailToExecData';

const ApplicationsDetailWithRouter = withRouter(props => <ApplicationDetail {...props}/>);

class ApplicationDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataReady: false,
            lookPhoto: [],
            execPhoto: [],
            calc: {
                roof: {customerCost: '0', performerPay: '0'},
                aprons: {customerCost: '0', performerPay: '0'},
                perimeter: {customerCost: '0', performerPay: '0'}
            },
        };
        this.getData = this.getData.bind(this);
        this.getPhoto = this.getPhoto.bind(this);
        this.toExec = this.toExec.bind(this);
        this.calc = this.calc.bind(this);
        this.preparingDataForDisplay = this.preparingDataForDisplay.bind(this);
        this.toNav = this.toNav.bind(this);
    }

    componentDidMount() {
        store.dispatch({type: 'APPLICATIONS_DETAIL_DATA', applicationsDetailData: {}});
        this.getData();
    }

    getData() {fetch('/api/applications/' + this.props.match.params.id, {method: 'POST'}).then(res => res.json()).then(resJson => this.preparingDataForDisplay(resJson))}
    static getCustomerData(id) {return fetch('/api/customers/' + id, {method: 'POST'}).then(customerRes => customerRes.json()).then(customerResJson => customerResJson)}
    static getObjectData(id) {return fetch('/api/objects/' + id, {method: 'POST'}).then(objectRes => objectRes.json()).then(objectResJson => objectResJson)}
    static getPerformerData(id) {return fetch('/api/performers/' + id, {method: 'POST'}).then(performerRes => performerRes.json()).then(performerResJson => performerResJson.data)}
    getPhoto(ids, state) {ids.map(el => fetch('/api/photo/' + el, {method: 'POST'}).then(res => res.blob()).then(blob => this.setState({[state]: [...this.state[state], (window.URL || window.webkitURL).createObjectURL(blob)]})))}
    toExec() {
        console.log(this.props.applications.applicationsDetailToExecData);
        fetch('/api/applications/' + this.props.match.params.id + '/toexec', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(this.props.applications.applicationsDetailToExecData)}).then(res => res.json()).then(resJson => this.getData())
    }

    static getStringFromDate(date) {
        let dateObj = new Date(date);
        let minutes = dateObj.getMinutes().toString().length === 1 ? '0' + dateObj.getMinutes() : dateObj.getMinutes();
        return dateObj.toISOString().substring(0, 10) + ' ' + dateObj.getHours() + ':' + minutes;
    }

    calc(rawData) {
        if (rawData.data.stages.lookComplete.work.hasOwnProperty('roof')) {
            let roofCalcData = {
                type: 'roof', // Тип работ (roof (кровля) || aprons (козырьки) || perimeter (периметр))
                baseCustomer: 20, // Базовая стоимость для заказчика за м2 (руб)
                basePerformer: 8, // Базовая зарплата для исполнителя за м2 (руб)
                area: rawData.data.stages.lookComplete.work.roof.area ? parseInt(rawData.data.stages.lookComplete.work.roof.area, 10) : 0, // Площадь (м2)
                height: rawData.data.stages.lookComplete.work.roof.height ? parseInt(rawData.data.stages.lookComplete.work.roof.height, 10) : 0, // Высота снежного покрова (см)
                roofType: rawData.data.stages.lookComplete.work.roof.roofType || 'pitched', // Тип кровли (pitched (скатная) || flat (плоская))
                motivation: false, // Мотивационный коэффицент
                night: false, // Ночной коэффицент
                difficult: false, // Коэффицент сложности
            };
            fetch('/api/calc/snow', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(roofCalcData)})
                .then(res => res.json())
                .then(resJson => this.setState(prevState => ({...prevState, calc: {...prevState.calc, roof: {...prevState.calc.roof, customerCost: resJson.data.summ, performerPay: resJson.data.performerSumm}}})));
        }
        if (rawData.data.stages.lookComplete.work.hasOwnProperty('aprons')) {
            let apronsCalcData = {
                type: 'aprons', // Тип работ (roof (кровля) || aprons (козырьки) || perimeter (периметр))
                area: rawData.data.stages.lookComplete.work.aprons.area ? parseInt(rawData.data.stages.lookComplete.work.aprons.area, 10) : 0, // Площадь (м2)
                apronsType: rawData.data.stages.lookComplete.work.aprons.apronsType || 'entry', // Тип козырька (entry (входной) || balcony (балконный))
            };
            fetch('/api/calc/snow', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(apronsCalcData)})
                .then(res => res.json())
                .then(resJson => this.setState(prevState => ({...prevState, calc: {...prevState.calc, aprons: {...prevState.calc.aprons, customerCost: resJson.data.summ, performerPay: resJson.data.performerSumm}}})));
        }
        if (rawData.data.stages.lookComplete.work.hasOwnProperty('perimeter')) {
            let perimeterCalcData = {
                type: 'perimeter', // Тип работ (roof (кровля) || aprons (козырьки) || perimeter (периметр))
                baseCustomer: 40, // Базовая стоимость для заказчика за м2 (руб)
                basePerformer: 20, // Базовая зарплата для исполнителя за м2 (руб)
                area: rawData.data.stages.lookComplete.work.perimeter.area ? parseInt(rawData.data.stages.lookComplete.work.perimeter.area, 10) : 0, // Площадь (м2)
                distance: rawData.data.stages.lookComplete.work.perimeter.distance ? parseInt(rawData.data.stages.lookComplete.work.perimeter.distance, 10) : 0, // Расстояние от края (м)
            };
            fetch('/api/calc/snow', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(perimeterCalcData)})
                .then(res => res.json())
                .then(resJson => this.setState(prevState => ({...prevState, calc: {...prevState.calc, perimeter: {...prevState.calc.perimeter, customerCost: resJson.data.summ, performerPay: resJson.data.performerSumm}}})));
        }
    }

    preparingDataForDisplay(rawData) {
        console.log(rawData);
        if (rawData.data.startingStage === 'look') {
            let getDataPromises = [ApplicationDetail.getCustomerData(rawData.data.stages.lookInit.customer), ApplicationDetail.getObjectData(rawData.data.stages.lookInit.object)];
            if (rawData.data.stages.hasOwnProperty('lookReadyToStart')) getDataPromises.push(ApplicationDetail.getPerformerData(rawData.data.stages.lookReadyToStart.performer));
            if (rawData.data.stages.hasOwnProperty('lookComplete')) {
                this.getPhoto(rawData.data.stages.lookComplete.photo, 'lookPhoto');
                this.calc(rawData);
            }
            Promise.all(getDataPromises)
                .then(res => {
                    if (rawData.data.stages.hasOwnProperty('lookComplete')) rawData.data.stages.lookComplete.date = ApplicationDetail.getStringFromDate(rawData.data.stages.lookComplete.date);
                    rawData.data.stages.lookInit.customer = res[0];
                    rawData.data.stages.lookInit.object = res[1];
                    if (rawData.data.stages.hasOwnProperty('lookReadyToStart')) rawData.data.stages.lookReadyToStart.performer = res[2];
                    rawData.data.dates.create = ApplicationDetail.getStringFromDate(rawData.data.dates.create);
                    rawData.data.dates.update = ApplicationDetail.getStringFromDate(rawData.data.dates.update);
                    console.log(rawData);
                    store.dispatch({type: 'APPLICATIONS_DETAIL_DATA', applicationsDetailData: rawData.data});
                    this.setState({dataReady: true});
                    this.toNav();
                })
        }
        else if (rawData.data.startingStage === 'exec') {
            let getDataPromises = [ApplicationDetail.getCustomerData(rawData.data.stages.execInit.customer), ApplicationDetail.getObjectData(rawData.data.stages.execInit.object)];
            if (rawData.data.stages.hasOwnProperty('execReadyToStart')) getDataPromises.push(ApplicationDetail.getPerformerData(rawData.data.stages.execReadyToStart.performer));
            if (rawData.data.stages.hasOwnProperty('execComplete')) this.getPhoto(rawData.data.stages.execComplete.photo, 'execPhoto');
            Promise.all(getDataPromises)
                .then(res => {
                    if (rawData.data.stages.hasOwnProperty('execComplete')) rawData.data.stages.execComplete.date = ApplicationDetail.getStringFromDate(rawData.data.stages.execComplete.date);
                    rawData.data.stages.execInit.customer = res[0];
                    rawData.data.stages.execInit.object = res[1];
                    if (rawData.data.stages.hasOwnProperty('execReadyToStart')) rawData.data.stages.execReadyToStart.performer = res[2];
                    rawData.data.dates.create = ApplicationDetail.getStringFromDate(rawData.data.dates.create);
                    rawData.data.dates.update = ApplicationDetail.getStringFromDate(rawData.data.dates.update);
                    console.log(rawData);
                    store.dispatch({type: 'APPLICATIONS_DETAIL_DATA', applicationsDetailData: rawData.data});
                    this.setState({dataReady: true});
                    this.toNav();
                })
        }
    }

    toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/applications', name: 'Заявки'}, {path: this.props.location.pathname, name: this.props.applications.applicationsDetailData.stages.lookInit.customer.data.name}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: true, name: 'Добавить заявку', path: '/applications/add'}, search: {display: true}}});
    }

    render() {
        return (
            <Wrapper>
                {
                    this.state.dataReady ?
                        <Container>
                            <DataCont>
                                <ItemCont>
                                    <ItemName>Заказчик</ItemName>
                                    <ItemValue>{this.props.applications.applicationsDetailData.stages.lookInit.customer.data.name}</ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Объект</ItemName>
                                    <ItemValue>{this.props.applications.applicationsDetailData.stages.lookInit.object.data.address}</ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Текущий этап</ItemName>
                                    <ItemValue>
                                        {this.props.applications.applicationsDetailData.currentStage === 'lookInit' ? 'Подготовка к осмотру' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'lookReadyToStart' ? 'Готов к осмотру' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'lookStart' ? 'Идет осмотр' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'lookComplete' ? 'Осмотр завершен' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'execInit' ? 'Подготовка к выполнению' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'execReadyToStart' ? 'Готов к выполнению' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'execStart' ? 'Идет выполнение' : null}
                                        {this.props.applications.applicationsDetailData.currentStage === 'execComplete' ? 'Выполнение завершено' : null}
                                    </ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Начальный этап</ItemName>
                                    <ItemValue>
                                        {this.props.applications.applicationsDetailData.startingStage === 'look' ? 'Осмотр' : null}
                                        {this.props.applications.applicationsDetailData.startingStage === 'exec' ? 'Выполнение' : null}
                                    </ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Дата добавления</ItemName>
                                    <ItemValue>{this.props.applications.applicationsDetailData.dates.create}</ItemValue>
                                </ItemCont>
                                <ItemCont>
                                    <ItemName>Дата обновления</ItemName>
                                    <ItemValue>{this.props.applications.applicationsDetailData.dates.update}</ItemValue>
                                </ItemCont>
                            </DataCont>


                            {
                                this.props.applications.applicationsDetailData.stages.hasOwnProperty('lookInit') ?
                                    <DataCont>
                                        <DataContTitle>Осмотр</DataContTitle>
                                        {this.props.applications.applicationsDetailData.currentStage === 'lookInit' ? <WorkPriceValue>Поиск исполнителя...</WorkPriceValue> : null}
                                        {
                                            this.props.applications.applicationsDetailData.stages.hasOwnProperty('lookReadyToStart') ?
                                                <ItemCont>
                                                    <ItemName>Исполнитель</ItemName>
                                                    <ItemValue>{this.props.applications.applicationsDetailData.stages.lookReadyToStart.performer.name}</ItemValue>
                                                </ItemCont>
                                                :null
                                        }
                                        {
                                            this.props.applications.applicationsDetailData.stages.hasOwnProperty('lookComplete') ?
                                                <ItemCont>
                                                    <ItemName>Дата завершения</ItemName>
                                                    <ItemValue>{this.props.applications.applicationsDetailData.stages.lookComplete.date}</ItemValue>
                                                </ItemCont>
                                                : null
                                        }
                                        {
                                            this.props.applications.applicationsDetailData.stages.hasOwnProperty('lookComplete') ?
                                                <DataCont style={{marginTop: '20px'}}>
                                                    {
                                                        this.props.applications.applicationsDetailData.stages.lookComplete.work.hasOwnProperty('roof') ?
                                                            <ItemCont>
                                                                <ItemName>Кровля</ItemName>
                                                                <WorkValue>Площадь: {this.props.applications.applicationsDetailData.stages.lookComplete.work.roof.area} м<sup>2</sup></WorkValue>
                                                                <WorkValue>Глубина: {this.props.applications.applicationsDetailData.stages.lookComplete.work.roof.height} см</WorkValue>
                                                                <WorkValue>
                                                                    Тип:
                                                                    {this.props.applications.applicationsDetailData.stages.lookComplete.work.roof.roofType === 'pitched' ? ' Скатная' : null}
                                                                    {this.props.applications.applicationsDetailData.stages.lookComplete.work.roof.roofType === 'flat' ? ' Плоская' : null}
                                                                </WorkValue>
                                                                <WorkPriceValue>Стоимость: {this.state.calc.roof.customerCost} руб</WorkPriceValue>
                                                                <WorkPriceValue>Исполнителю: {this.state.calc.roof.performerPay} руб</WorkPriceValue>
                                                            </ItemCont>
                                                            :null
                                                    }
                                                    {
                                                        this.props.applications.applicationsDetailData.stages.lookComplete.work.hasOwnProperty('aprons') ?
                                                            <ItemCont>
                                                                <ItemName>Козырьки</ItemName>
                                                                <WorkValue>Площадь: {this.props.applications.applicationsDetailData.stages.lookComplete.work.aprons.area} м<sup>2</sup></WorkValue>
                                                                <WorkValue>
                                                                    Тип:
                                                                    {this.props.applications.applicationsDetailData.stages.lookComplete.work.aprons.apronsType === 'entry' ? ' Входной' : null}
                                                                    {this.props.applications.applicationsDetailData.stages.lookComplete.work.aprons.apronsType === 'balcony' ? ' Балконный' : null}
                                                                </WorkValue>
                                                                <WorkPriceValue>Стоимость: {this.state.calc.aprons.customerCost} руб</WorkPriceValue>
                                                                <WorkPriceValue>Исполнителю: {this.state.calc.aprons.performerPay} руб</WorkPriceValue>
                                                            </ItemCont>
                                                            :null
                                                    }
                                                    {
                                                        this.props.applications.applicationsDetailData.stages.lookComplete.work.hasOwnProperty('perimeter') ?
                                                            <ItemCont>
                                                                <ItemName>Периметр</ItemName>
                                                                <WorkValue>Площадь: {this.props.applications.applicationsDetailData.stages.lookComplete.work.perimeter.area} м<sup>2</sup></WorkValue>
                                                                <WorkValue>От края: {this.props.applications.applicationsDetailData.stages.lookComplete.work.perimeter.distance} м</WorkValue>
                                                                <WorkPriceValue>Стоимость: {this.state.calc.perimeter.customerCost} руб</WorkPriceValue>
                                                                <WorkPriceValue>Исполнителю: {this.state.calc.perimeter.performerPay} руб</WorkPriceValue>
                                                            </ItemCont>
                                                            :null
                                                    }
                                                    {
                                                        this.state.lookPhoto.length ?
                                                            <ImgsCont>
                                                                {this.state.lookPhoto.map((imgEl, imgElI) => <img key={imgElI} style={{width: '100px', marginRight: '10px', cursor: 'pointer'}} src={imgEl} alt='application'/>)}
                                                            </ImgsCont>
                                                            :
                                                            null
                                                    }
                                                    <ItemCont style={{width: '100%'}}>
                                                        <ItemName>Примечания</ItemName>
                                                        <ItemValue>{this.props.applications.applicationsDetailData.stages.lookComplete.other ? this.props.applications.applicationsDetailData.stages.lookComplete.other : '- - -'}</ItemValue>
                                                    </ItemCont>
                                                </DataCont>
                                                :null
                                        }
                                    </DataCont>
                                    :null
                            }


                            {
                                this.props.applications.applicationsDetailData.stages.hasOwnProperty('execInit') || this.props.applications.applicationsDetailData.stages.hasOwnProperty('lookComplete') ?
                                    <DataCont>
                                        <DataContTitle>Выполнение</DataContTitle>
                                        {
                                            this.props.applications.applicationsDetailData.currentStage === 'lookComplete' ?
                                                <DataCont>
                                                    <ApplicationsDetailToExecData/>
                                                    <ToExecButton callback={this.toExec}/>
                                                </DataCont>
                                                : null
                                        }
                                        {this.props.applications.applicationsDetailData.currentStage === 'execInit' ? <WorkPriceValue>Поиск исполнителя...</WorkPriceValue> : null}
                                        {
                                            this.props.applications.applicationsDetailData.stages.hasOwnProperty('execReadyToStart') ?
                                                <ItemCont>
                                                    <ItemName>Исполнитель</ItemName>
                                                    <ItemValue>{this.props.applications.applicationsDetailData.stages.execReadyToStart.performer.name}</ItemValue>
                                                </ItemCont>
                                                : null
                                        }
                                    </DataCont>
                                    : null
                            }

                        </Container>
                        : null
                }
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    padding: 4px;
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
`;
const Container = styled.div`
    background-color: #ffffff;
    width: calc(100% - 20px);
    padding: 10px;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const DataCont = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const DataContTitle = styled.div`
    padding: 40px 0;
    width: 100%;
    font-size: 20px;
    font-weight: 900;
    color: #212121;
`;
const ItemCont = styled.div`
    width: 200px;
`;
const ItemName = styled.div`
    font-size: 12px;
    color: #757575;
`;
const ItemValue = styled.div`
    font-size: 14px;
    color: #212121;
`;
const WorkValue = styled.div`
    font-size: 12px;
    color: #212121
`;
const WorkPriceValue = styled.div`
    font-size: 12px;
    font-weight: 900;
    color: #212121
`;
const ImgsCont = styled.div`
    width: 100%;
    padding: 20px 0;
`;
/*const StyledValueLink = styled(NavLink)`
    font-size: 14px;
    color: #212121;
    &:hover {
        color: #757575;
    }
`;*/
/*const BtnsCont = styled.div`
    width: 100%;
    margin-top: 20px;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
`;*/

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps, null, null, {pure: false})(ApplicationsDetailWithRouter);
