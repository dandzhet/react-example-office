import React, { Component } from 'react';
import { connect } from "react-redux";
import styled from 'styled-components';
import ApplicationsAddCustomer from './applicationAddCustomer';
import ApplicationsAddObject from './applicationAddObject';
import ApplicationsAddExecutionWork from './applicationAddExecutionWork';
import ApplicationsAddPerformer from './applicationAddPerformer';
import ApplicationsAddDetail from './applicationAddDetail';
import StartButton from "../buttons/startButton";

class ApplicationsAddExecution extends Component {
    constructor(props) {
        super(props);
        this.dataPreparationForSending = this.dataPreparationForSending.bind(this);
        this.send = this.send.bind(this);
    }
    dataPreparationForSending() {
        let applicationData = {};
        applicationData.startingStage = 'exec';
        if (this.props.applications.applicationsAddCustomer.source === 'db') applicationData.customer = this.props.applications.applicationsAddCustomer.db;
        if (this.props.applications.applicationsAddObject.source === 'db') applicationData.object = this.props.applications.applicationsAddObject.db;
        applicationData.work = this.props.applications.applicationsAddExecWork;
        Object.assign(applicationData, this.props.applications.applicationsAddDetail);
        return applicationData;
    }
    send() {
        let applicationData = this.dataPreparationForSending();
        console.log(applicationData);
        fetch('/api/applications/add', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(applicationData)})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => {
                console.log(resJson);
                if (resJson.status === 'ok') this.setState({sendResStatus: resJson.status});
            });
    }
    render() {
        return (
            <Wrapper>
                <ApplicationsAddCustomer/>
                <ApplicationsAddObject/>
                <ApplicationsAddExecutionWork/>
                <ApplicationsAddPerformer/>
                <ApplicationsAddDetail/>
                <StartButton callback={this.send}/>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
`;

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps)(ApplicationsAddExecution);
