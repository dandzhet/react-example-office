import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import ApplicationsAddLook from './applicationAddLook';
import ApplicationsAddExecution from './applicationAddExecution';
import ApplicationsAddExecutionFromLook from './applicationAddExecutionFromLook';

const ApplicationsAddWithRouter = withRouter(props => <ApplicationAdd {...props}/>);

class ApplicationAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {startFrom: 'look'};
        this.startFromSelect = this.startFromSelect.bind(this);
        ApplicationAdd.toNav = ApplicationAdd.toNav.bind(this);
    }
    componentDidMount() {ApplicationAdd.toNav()}
    startFromSelect(e) {this.setState({startFrom: e.target.value})}
    static toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/applications', name: 'Заявки'}, {path: '/applications/add', name: 'Новая'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: false}, search: {display: true}}});
    }
    render() {
        return (
            <Wrapper>
                <Container>
                    <InputCont>
                        <select onChange={this.startFromSelect} value={this.state.startFrom} style={{width: '274px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}} name='stage'>
                            <option value='look'>Осмотр</option>
                            <option value='execution'>Выполнение работ</option>
                            {/*<option value='executionFromLook'>Выполнение работ на основе Осмотра</option>*/}
                        </select>
                    </InputCont>
                    {this.state.startFrom === 'look' ? <ApplicationsAddLook/> :null}
                    {this.state.startFrom === 'execution' ? <ApplicationsAddExecution/> :null}
                    {this.state.startFrom === 'executionFromLook' ? <ApplicationsAddExecutionFromLook/> :null}
                </Container>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: calc(100% - 8px);
    padding: 4px;
    position: relative;
`;
const Container = styled.div`
    padding: 10px;
    background-color: #ffffff;
`;
const InputCont = styled.div`
    margin: 10px 0;
`;

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps)(ApplicationsAddWithRouter);
