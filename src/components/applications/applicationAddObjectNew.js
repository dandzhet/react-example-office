import React, { Component } from 'react';
import styled from 'styled-components';
import connect from "react-redux/es/connect/connect";
import store from "../../reducers";

class ApplicationsAddObjectNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            address: '',
            other: '',
        };
        this.objectInputChange = this.objectInputChange.bind(this);
    }
    objectInputChange(e) {
        this.setState({[e.target.name]: e.target.value});
        let storeObject = this.props.applications.applicationsAddObject;
        storeObject.new[e.target.name] = e.target.value;
        store.dispatch({ type: 'APPLICATIONS_ADD_OBJECT', applicationsAddObject: storeObject });
    }
    render() {
        return (
            <Wrapper>
                <InputCont>
                    <InputLabel>Адрес</InputLabel>
                    <StyledInput onChange={this.objectInputChange} style={{width: '272px'}} name='address' type='text'/>
                </InputCont>
                <InputCont>
                    <InputLabel>Примечания</InputLabel>
                    <StyledInput onChange={this.objectInputChange} style={{width: '272px'}} name='other' type='text'/>
                </InputCont>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;

const mapStateToProps = (store) => {return {applications: store.applications}};
export default connect(mapStateToProps)(ApplicationsAddObjectNew);
