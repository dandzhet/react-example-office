import React, { Component } from 'react';
import styled from 'styled-components';
import store from "../../reducers";
import connect from "react-redux/es/connect/connect";
import ApplicationsAddCustomerNewFiz from './applicationAddCustomerNewFiz';
import ApplicationsAddCustomerNewUr from './applicationAddCustomerNewUr';

class ApplicationAddCustomerNew extends Component {
    constructor(props) {
        super(props);
        this.customerTypeSelect = this.customerTypeSelect.bind(this);
    }
    customerTypeSelect(e) {
        let storeCustomer = this.props.applications.applicationsAddCustomer;
        storeCustomer.new.type = e.target.value;
        store.dispatch({ type: 'APPLICATIONS_ADD_CUSTOMER', applicationsAddCustomer: storeCustomer });
    }
    render() {
        return (
            <Wrapper>
                <InputCont>
                    <select onChange={this.customerTypeSelect} value={this.props.applications.applicationsAddCustomer.new.type} style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}} name='customerType'>
                        <option value='ur'>Юр. лицо</option>
                        <option value='fiz'>Физ. лицо</option>
                    </select>
                </InputCont>
                {this.props.applications.applicationsAddCustomer.new.type === 'ur' ? <ApplicationsAddCustomerNewUr/> :null}
                {this.props.applications.applicationsAddCustomer.new.type === 'fiz' ? <ApplicationsAddCustomerNewFiz/> :null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;

const mapStateToProps = (store) => {return {applications: store.applications}};
export default connect(mapStateToProps)(ApplicationAddCustomerNew);