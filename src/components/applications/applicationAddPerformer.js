import React, {Component} from 'react';
import {connect} from "react-redux";
import styled from 'styled-components';

class ApplicationsAddPerformer extends Component {
    constructor(props) {
        super(props);
        this.selectChange = this.selectChange.bind(this);
    }
    selectChange(e) {}
    render() {
        return (
            <Wrapper>
                <Separator>Исполнитель</Separator>
                {/*<InputLabel>Способ подбора</InputLabel>*/}
                <select onChange={this.selectChange} style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}} name='object'>
                    <option value='auto'>Авто</option>
                    <option value='all'>Все</option>
                    <option value='manual'>Вручную</option>
                </select>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
`;
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 18px;
`;
/*const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;*/

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps)(ApplicationsAddPerformer);
