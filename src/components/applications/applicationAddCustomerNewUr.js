import React, { Component } from 'react';
import styled from 'styled-components';
import connect from "react-redux/es/connect/connect";
import store from "../../reducers";

class ApplicationsAddCustomerNewUr extends Component {
  constructor(props) {
    super(props);
    this.state = {data: {contact:{}}};
    this.inputChange = this.inputChange.bind(this);
  }
  inputChange(e) {
    let storeCustomer = this.props.applications.applicationsAddCustomer;
    storeCustomer.new[e.target.name] = e.target.value;
    store.dispatch({ type: 'APPLICATIONS_ADD_CUSTOMER', applicationsAddCustomer: storeCustomer });
  }
  render() {
    return (
      <Wrapper>
        <Container>
            <InputCont>
                <InputLabel>Название</InputLabel>
                <StyledInput onChange={this.inputChange} required name='name' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>Полное название</InputLabel>
                <StyledInput onChange={this.inputChange} name='fullName' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>Юр. адрес</InputLabel>
                <StyledInput onChange={this.inputChange} name='urAddress' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>Факт. адрес</InputLabel>
                <StyledInput onChange={this.inputChange} name='factAddress' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>Сайт</InputLabel>
                <StyledInput onChange={this.inputChange} name='site' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>ОГРН</InputLabel>
                <StyledInput onChange={this.inputChange} name='ogrn' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>ИНН</InputLabel>
                <StyledInput onChange={this.inputChange} name='inn' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>КПП</InputLabel>
                <StyledInput onChange={this.inputChange} name='kpp' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>ОКПО</InputLabel>
                <StyledInput onChange={this.inputChange} name='okpo' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>Р/с</InputLabel>
                <StyledInput onChange={this.inputChange} name='rs' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>Банк</InputLabel>
                <StyledInput onChange={this.inputChange} name='bank' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>К/с</InputLabel>
                <StyledInput onChange={this.inputChange} name='ks' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>БИК</InputLabel>
                <StyledInput onChange={this.inputChange} name='bik' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>ФИО директора</InputLabel>
                <StyledInput onChange={this.inputChange} name='directorName' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>Примечания</InputLabel>
                <StyledInput style={{width: '272px'}} onChange={this.inputChange} name='other' type='text'/>
            </InputCont>
            <Separator style={{margin: '0', fontSize: '12px', color: '#757575', fontWeight: '900'}}>Контактное лицо</Separator>
            <InputCont>
                <InputLabel>ФИО</InputLabel>
                <StyledInput onChange={this.inputChange} name='contactName' type='text'/>
            </InputCont>
            <InputCont>
                <InputLabel>Телефон</InputLabel>
                <StyledInput onChange={this.inputChange} name='contactPhone' type='tel'/>
            </InputCont>
            <InputCont>
                <InputLabel>Почта</InputLabel>
                <StyledInput onChange={this.inputChange} name='contactMail' type='email'/>
            </InputCont>
            <InputCont>
                <InputLabel>Должность</InputLabel>
                <StyledInput onChange={this.inputChange} name='contactPost' type='text'/>
            </InputCont>
        </Container>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
`;
const Container = styled.div`
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;
const Separator = styled.div`
    margin-top: 40px;
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 18px;
`;

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps)(ApplicationsAddCustomerNewUr);