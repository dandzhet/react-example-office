import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import styled from 'styled-components';

class ApplicationAddCustomerDb extends Component {
    constructor(props) {
        super(props);
        this.state = {customers: []};
        this.selectChange = this.selectChange.bind(this);
    }
    componentDidMount() {
        fetch('/api/customers', { method: 'POST' })
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => this.setState({customers: resJson.data}));
    }
    selectChange(e) {
        let applicationAddCustomerData = this.props.applications.applicationsAddCustomer;
        applicationAddCustomerData.db = e.target.value;
        store.dispatch({type: 'APPLICATIONS_ADD_CUSTOMER', applicationsAddCustomer: applicationAddCustomerData});
    }
    render() {
        return (
            <Wrapper>
                <select onChange={this.selectChange} style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}}>
                    <option value='none'>- - -</option>
                    {this.state.customers.map((el, i) => <option value={el._id} key={i}>{el.data.name}</option>)}
                </select>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps)(ApplicationAddCustomerDb);
