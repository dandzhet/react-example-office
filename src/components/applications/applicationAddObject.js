import React, { Component } from 'react';
import styled from 'styled-components';
import store from "../../reducers";
import connect from "react-redux/es/connect/connect";
import ApplicationsAddObjectDb from './applicationAddObjectDb';
import ApplicationsAddObjectNew from './applicationAddObjectNew';

class ApplicationsAddObject extends Component {
    constructor(props) {
        super(props);
        this.objectSourceSelect = this.objectSourceSelect.bind(this);
    }
    objectSourceSelect(e) {
        let storeObject = this.props.applications.applicationsAddObject;
        storeObject.source = e.target.value;
        store.dispatch({ type: 'APPLICATIONS_ADD_OBJECT', applicationsAddObject: storeObject });
    }
    render() {
        return (
            <Wrapper>
                <Separator>Объект</Separator>
                {/*<InputCont>
                    <select onChange={this.objectSourceSelect} value={this.props.applications.applicationsAddObject.source} style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}} name='customerSource'>
                        <option value='db'>Из базы</option>
                        <option value='new'>Новый</option>
                    </select>
                </InputCont>*/}
                {this.props.applications.applicationsAddObject.source === 'db' ? <ApplicationsAddObjectDb/> :null}
                {this.props.applications.applicationsAddObject.source === 'new' ? <ApplicationsAddObjectNew/> :null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
`;
/*const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;*/
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 18px;
`;

const mapStateToProps = (store) => {return {applications: store.applications}};
export default connect(mapStateToProps)(ApplicationsAddObject);
