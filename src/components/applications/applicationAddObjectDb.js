import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import styled from 'styled-components';

class ApplicationsAddObjectDb extends Component {
    constructor(props) {
        super(props);
        this.state = {dataReady: false, objects: []};
        this.checkData = this.checkData.bind(this);
        this.selectChange = this.selectChange.bind(this);
    }
    checkData(data) {
        this.setState({dataReady: true, objects: data.data})
    }
    componentDidMount() {
        fetch('/api/objects/', {method: 'POST'})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => this.checkData(resJson));
    }
    selectChange(e) {
        let applicationAddObjectData = this.props.applications.applicationsAddObject;
        applicationAddObjectData.db = e.target.value;
        store.dispatch({type: 'APPLICATIONS_ADD_OBJECT', applicationsAddObject: applicationAddObjectData});
    }
    render() {
        return (
            <Wrapper>
                <select onChange={this.selectChange} style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}} name='object'>
                    <option value='none'>- - -</option>
                    {this.state.dataReady ? this.state.objects.map((el, i) => <option value={el._id} key={i}>{el.data.address}</option>) : null}
                </select>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps)(ApplicationsAddObjectDb);
