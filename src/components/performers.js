import React, {Component} from 'react';
import {connect} from "react-redux";
import store from './../reducers';
import styled from 'styled-components';
import PerformersPm from './performers/performersPm';

class Performers extends Component {
    constructor(props) {
        super(props);
        this.getData = this.getData.bind(this);
        Performers.toNav = Performers.toNav.bind(this);
    }
    componentDidMount() {
        store.dispatch({type: 'PERFORMERS_DATA', performersData: {}});
        Performers.toNav();
        this.getData();
    }
    getData() {
        fetch('/api/performers', {method: 'POST'})
            .then(res => res.json())
            .then(resJson => store.dispatch({type: 'PERFORMERS_DATA', performersData: resJson}));
    }
    static toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/performers', name: 'Исполнители'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: true, name: 'Добавить исполнителя +', path: '/performers/add'}, search: {display: true}}});
    }
    render() {
        return (
            <Wrapper>
                {this.props.performers.performersData && this.props.performers.performersData.hasOwnProperty('data') && this.props.performers.performersData.data.length ? this.props.performers.performersData.data.map(el => <PerformersPm key={el._id} id={el._id} name={el.name}/>) : <Empty>Пусто</Empty>}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  width: calc(100% - 8px);
  margin: 4px;
  background-color: #ffffff;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;
const Empty = styled.div`
  padding: 20px;
  color: #757575;
`;

const mapStateToProps = store => ({performers: store.performers});
export default connect(mapStateToProps)(Performers);
