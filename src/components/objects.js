import React, {Component} from 'react';
import {connect} from "react-redux";
import store from './../reducers';
import styled from 'styled-components';
import ObjectsPm from './objects/objectsPm';

class Objects extends Component {
    constructor(props) {
        super(props);
        this.getData = this.getData.bind(this);
        Objects.toNav = Objects.toNav.bind(this);
    }
    componentDidMount() {
        store.dispatch({ type: 'OBJECT_LIST_DATA', objectListData: [] });
        this.getData();
        Objects.toNav();
    }
    getData() {
        fetch('/api/objects', {method: 'POST'})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => store.dispatch({ type: 'OBJECT_LIST_DATA', objectListData: resJson.data }));
    }
    static toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/objects', name: 'Объекты'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: true, name: 'Добавить объект', path: '/objects/add'}, search: {display: true}}});
    }
    render() {
        return (
            <Wrapper>
                {this.props.objects.objectListData.length ? this.props.objects.objectListData.map((el, i) => <ObjectsPm key={i} id={el._id} name={el.data.address}/>) : <Empty>Пусто</Empty>}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  width: calc(100% - 8px);
  margin: 4px;
  background-color: #ffffff;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;
const Empty = styled.div`
  padding: 20px;
  color: #757575;
`;

const mapStateToProps = store => ({objects: store.objects});
export default connect(mapStateToProps)(Objects);
