import React, {Component} from 'react';
import {connect} from "react-redux";
//import store from './../reducers';
import styled from 'styled-components';

class Dialog extends Component {
    toDisplayTimeout;
    fromDisplyaTimeout;
    constructor(props) {
        super(props);
        this.state = {display: 'none', opacity: '0'};
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevProps.dialog.dialogActive !== this.props.dialog.dialogActive) return true;
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot !== null) {
           if (this.props.dialog.dialogActive) {
               this.setState({display: 'flex'});
               this.toDisplayTimeout = setTimeout(() => this.setState({opacity: '1'}), 4);
               clearTimeout(this.fromDisplyaTimeout);
           }
           else {
               this.setState({opacity: '0'});
               this.fromDisplyaTimeout = setTimeout(() => this.setState({display: 'none'}), 450);
               clearTimeout(this.toDisplayTimeout);
           }
        }
    }

    render() {
        return (
            <Wrapper style={{display: this.state.display, opacity: this.state.opacity}}>
                <Content>
                    <i style={{fontSize: '48px', color: '#ffffff'}} className="fas fa-circle-notch fa-spin"/>
                </Content>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    z-index: 3;
    width: 100%;
    height: 100vh;
    background-color: rgba(0,0,0,0.4);
    position: fixed;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    transition: .4s;
`;
const Content = styled.div`
    position: relative;
`;

const mapStateToProps = store => ({dialog: store.dialog});
export default connect(mapStateToProps)(Dialog);
