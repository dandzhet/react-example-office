import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../../reducers';
import {withRouter, Redirect} from 'react-router-dom';
import styled from 'styled-components';
import PerformersAddWorkInput from './performersAddWorkInputTest';
//import PerformersAddBrigadeInput from './performersAddBrigadeInput';
import SaveButton from '../buttons/saveButton';

const PerformersAddWithRouter = withRouter(props => <PerformersAdd {...props}/>);

class PerformersAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {photos: [], sendResStatus: 'wait'};
        PerformersAdd.performerDataPreparation = PerformersAdd.performerDataPreparation.bind(this);
        this.photoInputChange = this.photoInputChange.bind(this);
        this.send = this.send.bind(this);
        this.photoSend = this.photoSend.bind(this);
        this.dataSend = this.dataSend.bind(this);
        PerformersAdd.toNav = PerformersAdd.toNav.bind(this);
    }
    componentDidMount() {PerformersAdd.toNav()}
    static performerDataPreparation(e) {
        let obj = {};
        for (let i = 0; i < e.target.length - 1; i++) {
            if (e.target[i].type === 'checkbox') obj[e.target[i].name] = e.target[i].checked;
            else obj[e.target[i].name] = e.target[i].value;
            if (e.target[i].name === 'work') obj[e.target[i].name] = JSON.parse(e.target[i].value);
        }
        return obj;
    }
    photoInputChange(e) {this.setState({photos: e.target.files})}
    photoSend(callback) {
        let photos = new FormData();
        for (let x = 0; x < this.state.photos.length; x++) photos.append('photo', this.state.photos[x]);
        fetch('/api/photo/add', {method: 'POST', body: photos})
            .then(res => res.json())
            .then(resJson => callback(resJson));
    }
    dataSend(performerData) {
        console.log(performerData);
        fetch('/api/performers/add', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(performerData)})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => {
                console.log(resJson);
                store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false});
                if (resJson.status === 'ok') this.setState({sendResStatus: resJson.status});
            });
    }
    send(e) {
        e.preventDefault();
        store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: true});
        let performerData = PerformersAdd.performerDataPreparation(e);
        if (this.state.photos.length) this.photoSend(photoIds => {
            performerData.photo = photoIds;
            this.dataSend(performerData);
        });
        else {
            performerData.photo = [];
            this.dataSend(performerData);
        }
    }
    static toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/performers', name: 'Исполнители'}, {path: '/performers/add', name: 'Новый'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: false}, search: {display: true}}});
    }
    render() {
        return (
            <Wrapper>
                <StyledForm onSubmit={this.send}>
                    <InputCont>
                        <InputLabel>Статус</InputLabel>
                        <select name='performerStatus' style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}}>
                            <option value='request'>Заявка</option>
                            <option value='verified'>Аккредитован</option>
                        </select>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Канал привлечения</InputLabel>
                        <StyledInput name='attractionChannel' type='text'/>
                    </InputCont>
                    <SeparatorLine/>
                    <InputCont>
                        <InputLabel>Сканы / фото документов</InputLabel>
                        <input onChange={this.photoInputChange} name='photo' type='file' multiple={true} accept='image/*'/>
                    </InputCont>
                    <SeparatorLine/>
                    <InputCont>
                        <InputLabel>ФИО</InputLabel>
                        <StyledInput required name='name' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Город</InputLabel>
                        <StyledInput name='city' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Адрес</InputLabel>
                        <StyledInput name='address' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Телефон</InputLabel>
                        <StyledInput name='phone' type='tel'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Почта</InputLabel>
                        <StyledInput name='mail' type='email'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Пароль</InputLabel>
                        <StyledInput name='password' type='password'/>
                    </InputCont>

                    <SeparatorLine/>
                    <InputCont>
                        <InputLabel>Виды работ</InputLabel>
                        <PerformersAddWorkInput/>
                    </InputCont>
                    <div style={{display: 'flex', flexDirection: 'column', alignItems: 'flex-start'}}>
                        <div style={{display: 'flex', flexDirection: 'row', alignItems: 'flex-start', flexWrap: 'wrap'}}>
                            <InputCont>
                                <InputLabel>Снаряжение, инструменты, техника</InputLabel>
                                <StyledInput style={{width: '272px'}} name='tool'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Лицензии</InputLabel>
                                <StyledInput style={{width: '272px'}} name='license'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Образование</InputLabel>
                                <StyledInput style={{width: '272px'}} name='education'/>
                            </InputCont>
                        </div>
                        <div style={{display: 'flex', flexDirection: 'row', alignItems: 'flex-start', flexWrap: 'wrap'}}>
                            <InputCont>
                                <InputLabel>Опыт лет</InputLabel>
                                <StyledInput name='experienceYears' type='number'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Опыт подробно</InputLabel>
                                <StyledInput style={{width: '272px'}} name='experienceDetail'/>
                            </InputCont>
                        </div>
                    </div>

                    <SeparatorLine/>
                    <InputCont>
                        <InputLabel>Автомобиль</InputLabel>
                        <StyledInputCheckbox name='car' type='checkbox'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>ИП</InputLabel>
                        <StyledInputCheckbox name='individualEntrepreneur' type='checkbox'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>ООО</InputLabel>
                        <StyledInputCheckbox name='ltd' type='checkbox'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Северск</InputLabel>
                        <StyledInputCheckbox name='passToSeversk' type='checkbox'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Примечания</InputLabel>
                        <StyledInput style={{width: '272px'}} name='other'/>
                    </InputCont>

                    <SeparatorLine/>
                    <BtnsCont><SaveButton/></BtnsCont>
                </StyledForm>
                {this.state.sendResStatus === 'ok' ? <Redirect to={'/performers'}/> : null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    padding: 4px;
    position: relative;
`;
const StyledForm = styled.form`
    padding: 10px;
    padding-top: 40px;
    background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;
const StyledInputCheckbox = styled.input`
    width: 100px;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const SeparatorLine = styled.div`
    width: 100%;
    margin: 40px 0;
    height: 1px;
    background-color: #eeeeee;
`;
const BtnsCont = styled.div`
    width: 100%;
    margin-bottom: 25px;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
`;

const mapStateToProps = store => ({performers: store.performers, dialog: store.dialog});
export default connect(mapStateToProps, null, null, {pure: false})(PerformersAddWithRouter);
