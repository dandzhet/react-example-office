import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../../reducers';
import styled from 'styled-components';

class PerformersEditWorkInput extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.workTypeSelect = this.workTypeSelect.bind(this);
        this.workNameSelect = this.workNameSelect.bind(this);
        this.setInputValue = this.setInputValue.bind(this);
        this.getData = this.getData.bind(this);
        this.toStore = this.toStore.bind(this);
    }
    componentDidMount() {
        console.log(this.props.performers.performersDetailData.data.work);
        this.toStore('PERFORMERS_ADD_WORK_INPUT_DATA', 'performersAddWorkInputData', []);
        //this.getData();
    }
    workTypeSelect(e, id) {
        let data = this.props.performers.performersAddWorkInputData;
        data = data.map((el) => {
            if (el._id === id) {
                el.active = !el.active;
                if (!el.active) {
                    el.workNames = el.workNames.map(elItems => {
                        elItems.active = false;
                        return elItems;
                    });
                }
            }
            return el;
        });
        this.toStore('PERFORMERS_ADD_WORK_INPUT_DATA', 'performersAddWorkInputData', data);
        this.setInputValue([...data]);
    }
    workNameSelect(e, workTypeId, id) {
        let data = this.props.performers.performersAddWorkInputData;
        data = data.map(el => {
            if (el._id === workTypeId) {
                el.workNames = el.workNames.map(elItems => {
                    if (elItems.id === id) elItems.active = !elItems.active;
                    if (elItems.active) el.active = true;
                    return elItems;
                });
            }
            return el;
        });
        this.toStore('PERFORMERS_ADD_WORK_INPUT_DATA', 'performersAddWorkInputData', data);
        this.setInputValue([...data]);
    }
    setInputValue(inptData) {
        let cleanInptData = inptData.map(el => {
            return {
                id: el._id,
                active: el.active,
                items: el.hasOwnProperty('workNames') && el.workNames && el.workNames.length ? el.workNames.map(elItems => ({id: elItems.id, active: elItems.active})) : [],
            };
        });
        let filteredInptData = cleanInptData.filter((el) => {
            if (el.active && el.hasOwnProperty('items') && el.items && el.items.length) el.items = el.items.filter(elItems => elItems.active);
            return el.active;
        });
        let readyData = filteredInptData.map(el => {
            el.items = el.items.map(elItems => ({id: elItems.id}));
            return {id: el.id, items: el.items};
        });
        console.log(readyData);
        this.toStore('PERFORMERS_ADD_WORK_INPUT_VALUE', 'performersAddWorkInputValue', readyData);
    }
    getData() {
        let path = '/api/work';
        fetch(path, {method: 'POST'})
            .then(res => {
                res.json().then((json) => {
                    if (json.data) {
                        json.data = json.data.map((el) => {
                            el.active = false;
                            this.props.performers.performersDetailData.data.work.map((dbWorkEl) => {
                                if (dbWorkEl.id.toString() === el._id.toString()) el.active = true;
                                return dbWorkEl;
                            });
                            if (el.hasOwnProperty('workNames')) {
                                if (el.workNames.length) {
                                    el.workNames = el.workNames.map((elData) => {
                                        elData.active = false;
                                        this.props.performers.performersDetailData.data.work.map((dbWorkEl) => {
                                            dbWorkEl.items.map((dbWorkElItemsEl) => {
                                                if (dbWorkElItemsEl.id.toString() === elData.id.toString()) elData.active = true;
                                                return dbWorkElItemsEl;
                                            });
                                            return dbWorkEl;
                                        });
                                        return elData;
                                    });
                                }
                            }
                            return el;
                        });
                    }
                    this.toStore('PERFORMERS_ADD_WORK_INPUT_DATA', 'performersAddWorkInputData', json.data);
                    this.setInputValue([...json.data]);
                });
            });
    }
    toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
    render() {
        return (
            <Wrapper innerRef={elem => this.wrapRef = elem}>
                <input name='work' type='hidden' value={JSON.stringify(this.props.performers.performersAddWorkInputValue)}/>
                <SelectCont>
                    {this.props.performers.performersAddWorkInputData.map((el) => {
                        return (
                            <SelectCollectionCont key={el._id}>
                                {el.active ? <SelectCollectionTitleActive onClick={(e) => {this.workTypeSelect(e, el._id)}}>{el.name}</SelectCollectionTitleActive> : <SelectCollectionTitle onClick={(e) => {this.workTypeSelect(e, el._id)}}>{el.name}</SelectCollectionTitle>}
                                <SelectCollectionItemsCont>
                                    {el.workNames.map((elItems) => {
                                        return elItems.active ? <SelectCollectionItemActive onClick={(e) => {this.workNameSelect(e, el._id, elItems.id)}} key={elItems.id}>{elItems.name}</SelectCollectionItemActive> : <SelectCollectionItem onClick={(e) => {this.workNameSelect(e, el._id, elItems.id)}} key={elItems.id}>{elItems.name}</SelectCollectionItem>;
                                    })}
                                </SelectCollectionItemsCont>
                            </SelectCollectionCont>
                        );
                    })}
                </SelectCont>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    position: relative;
    width: 400px;
    height: 200px;
    background-color: #ffffff;
    border: 1px solid #eeeeee;
    overflow: scroll;
`;
const SelectCont = styled.div`
    position: relative;
`;
const SelectCollectionCont = styled.div``;
const SelectCollectionTitle = styled.div`
    padding: 5px;
    font-size: 14px;
    color: #212121;
    font-weight: 900;
    cursor: pointer;
    &:hover {
        background-color: #eeeeee;
    }
`;
const SelectCollectionTitleActive = styled.div`
    padding: 5px;
    font-size: 14px;
    color: #ffffff;
    font-weight: 900;
    cursor: pointer;
    background-color: #bdbdbd;
`;
const SelectCollectionItemsCont = styled.div``;
const SelectCollectionItem = styled.div`
    padding: 5px;
    padding-left: 10px;
    font-size: 14px;
    color: #757575;
    cursor: pointer;
    &:hover {
        background-color: #eeeeee;
    }
`;
const SelectCollectionItemActive = styled.div`
    padding: 5px;
    padding-left: 10px;
    font-size: 14px;
    color: #eeeeee;
    cursor: pointer;
    background-color: #bdbdbd;
`;

const mapStateToProps = store => ({performers: store.performers});
export default connect(mapStateToProps, null, null, { pure: false })(PerformersEditWorkInput);
