import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../../reducers';
import {withRouter, Redirect} from 'react-router-dom';
import styled from 'styled-components';
import PerformersEditWorkInput from './performersEditWorkInput';
import DeleteButton from '../buttons/deleteButton';
import SaveButton from '../buttons/saveButton';
//import PerformersAddBrigadeInput from './performersAddBrigadeInput';

const PerformersEditWithRouter = withRouter(props => <PerformersEdit {...props}/>);

class PerformersEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sendResStatus: 'wait',
            data: {
                performerStatus: 'Заявка',
                channelOfAttraction: '',
                name: '',
                city: '',
                address: '',
                phone: '',
                mail: '',
                password: '',
                tool: '',
                experienceYears: '0',
                experienceDetail: '',
                license: '',
                education: '',
                car: false,
                individualEntrepreneur: false,
                ltd: false,
                passToSeversk: false,
                other: '',
            },
            dataLoaded: false,
        };
        this.inputChange = this.inputChange.bind(this);
        this.send = this.send.bind(this);
        this.delete = this.delete.bind(this);
        this.toNav = this.toNav.bind(this);
    }

    componentDidMount() {this.getData()}

    inputChange(e) {
        let data = this.state.data;
        data[e.target.name] = e.target.value;
        this.setState({data: data});
    }

    getData() {
        fetch('/api/performers/' + this.props.match.params.id, {method: 'POST'})
            .then(res => res.json())
            .then(resJson => {
                console.log(resJson);
                store.dispatch({type: 'PERFORMERS_DETAIL_DATA', performersDetailData: resJson});
                this.toNav();
                let reqData = resJson.data;
                this.setState({
                    data: {
                        performerStatus: reqData.hasOwnProperty('performerStatus') ? reqData.performerStatus : '',
                        channelOfAttraction: reqData.hasOwnProperty('channelOfAttraction') ? reqData.channelOfAttraction : '',
                        name: reqData.hasOwnProperty('name') ? reqData.name : '',
                        city: reqData.hasOwnProperty('city') ? reqData.city : '',
                        address: reqData.hasOwnProperty('address') ? reqData.address : '',
                        phone: reqData.hasOwnProperty('phone') ? reqData.phone : '',
                        mail: reqData.hasOwnProperty('mail') ? reqData.mail : '',
                        password: reqData.hasOwnProperty('password') ? reqData.password : '',
                        experienceYears: reqData.hasOwnProperty('experienceYears') && reqData.experienceYears ? reqData.experienceYears : '0',
                        car: reqData.hasOwnProperty('car') ? reqData.car : false,
                        individualEntrepreneur: reqData.hasOwnProperty('individualEntrepreneur') ? reqData.individualEntrepreneur : false,
                        ltd: reqData.hasOwnProperty('ltd') ? reqData.ltd : false,
                        passToSeversk: reqData.hasOwnProperty('passToSeversk') ? reqData.passToSeversk : false,
                        other: reqData.hasOwnProperty('other') ? reqData.other : '',
                    },
                    dataLoaded: true,
                });
            });
    }

    send(e) {
        e.preventDefault();
        let obj = {};
        for (let i = 0; i < e.target.length - 1; i++) {
            if (e.target[i].type === 'checkbox') {obj[e.target[i].name] = e.target[i].checked; continue}
            if (e.target[i].name === 'work') {obj[e.target[i].name] = JSON.parse(e.target[i].value); continue}
            obj[e.target[i].name] = e.target[i].value;
        }
        console.log(obj);
        fetch('/api/performers/' + this.props.match.params.id + '/update', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(obj)})
            .then(res => res.json())
            .then(resJson => {
                console.log(resJson);
                if (resJson.status === 'ok') this.setState({sendResStatus: resJson.status});
            });
    }

    delete(e) {
        e.preventDefault();
        fetch('/api/performers/' + this.props.match.params.id + '/delete', {method: 'POST'})
            .then(res => res.json())
            .then(resJson => {if (resJson.status === 'ok') this.setState({sendResStatus: resJson.status})});
    }

    toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/performers', name: 'Исполнители'}, {path: '/performers/' + this.props.location.pathname.split('/')[2], name: this.props.performers.performersDetailData.data.name}, {path: this.props.location.pathname, name: 'Редактирование'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: true, name: 'Добавить исполнителя +', path: '/performers/add'}, search: {display: true}}});
    }

    render() {
        return (
            <Wrapper>
                {this.state.dataLoaded ?
                    <StyledForm onSubmit={this.send}>
                        <InputCont>
                            <InputLabel>Статус</InputLabel>
                            <select style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}} value={this.state.data.performerStatus} onChange={this.inputChange} name='performerStatus'>
                                <option>Заявка</option>
                                <option>Ожидает работу</option>
                                <option>Выполняет работу</option>
                            </select>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Канал привлечения</InputLabel>
                            <StyledInput value={this.state.data.channelOfAttraction} onChange={this.inputChange} name='channelOfAttraction' type='text'/>
                        </InputCont>

                        <SeparatorLine/>
                        <InputCont>
                            <InputLabel>ФИО</InputLabel>
                            <StyledInput value={this.state.data.name} onChange={this.inputChange} required name='name' type='text'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Город</InputLabel>
                            <StyledInput value={this.state.data.city} onChange={this.inputChange} name='city' type='text'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Адрес</InputLabel>
                            <StyledInput value={this.state.data.address} onChange={this.inputChange} name='address' type='text'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Телефон</InputLabel>
                            <StyledInput value={this.state.data.phone} onChange={this.inputChange} name='phone' type='tel'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Почта</InputLabel>
                            <StyledInput value={this.state.data.mail} onChange={this.inputChange} name='mail' type='email'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Пароль</InputLabel>
                            <StyledInput value={this.state.data.password} onChange={this.inputChange} name='password' type='password'/>
                        </InputCont>

                        <SeparatorLine/>
                        <InputCont>
                            <InputLabel>Виды работ</InputLabel>
                            <PerformersEditWorkInput/>
                        </InputCont>
                        <div style={{display: 'flex', flexDirection: 'column', alignItems: 'flex-start'}}>
                            <div style={{display: 'flex', flexDirection: 'row', alignItems: 'flex-start', flexWrap: 'wrap'}}>
                                <InputCont>
                                    <InputLabel>Снаряжение, инструменты, техника</InputLabel>
                                    <StyledInput value={this.state.data.tool} onChange={this.inputChange} style={{width: '272px'}} name='tool'/>
                                </InputCont>
                                <InputCont>
                                    <InputLabel>Лицензии</InputLabel>
                                    <StyledInput value={this.state.data.license} onChange={this.inputChange} style={{width: '272px'}} name='license'/>
                                </InputCont>
                                <InputCont>
                                    <InputLabel>Образование</InputLabel>
                                    <StyledInput value={this.state.data.education} onChange={this.inputChange} style={{width: '272px'}} name='education'/>
                                </InputCont>
                            </div>
                            <div style={{display: 'flex', flexDirection: 'row', alignItems: 'flex-start', flexWrap: 'wrap'}}>
                                <InputCont>
                                    <InputLabel>Опыт лет</InputLabel>
                                    <StyledInput value={this.state.data.experienceYears} onChange={this.inputChange} name='experienceYears' type='number'/>
                                </InputCont>
                                <InputCont>
                                    <InputLabel>Опыт подробно</InputLabel>
                                    <StyledInput value={this.state.data.experienceDetail} onChange={this.inputChange} style={{width: '272px'}} name='experienceDetail'/>
                                </InputCont>
                            </div>
                        </div>

                        <SeparatorLine/>
                        <InputCont>
                            <InputLabel>Автомобиль</InputLabel>
                            <StyledInputCheckbox checked={this.state.data.car} onChange={this.inputChange} name='car' type='checkbox'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>ИП</InputLabel>
                            <StyledInputCheckbox checked={this.state.data.individualEntrepreneur} onChange={this.inputChange} name='individualEntrepreneur' type='checkbox'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>ООО</InputLabel>
                            <StyledInputCheckbox checked={this.state.data.ltd} onChange={this.inputChange} name='ltd' type='checkbox'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Северск</InputLabel>
                            <StyledInputCheckbox checked={this.state.data.passToSeversk} onChange={this.inputChange} name='passToSeversk' type='checkbox'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Примечания</InputLabel>
                            <StyledInput value={this.state.data.other} onChange={this.inputChange} style={{width: '272px'}} name='other' type='text'/>
                        </InputCont>

                        <SeparatorLine/>
                        <BtnsCont>
                            <SaveButton/>
                            <DeleteButton callback={this.delete}/>
                        </BtnsCont>
                    </StyledForm>
                    : '- - -'}
                {this.state.sendResStatus === 'ok' ? <Redirect to={'/performers'}/> : null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
	padding: 4px;
	position: relative;
`;
const StyledForm = styled.form`
	padding: 10px;
    padding-top: 40px;
    background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
	margin-right: 10px;
	margin-bottom: 10px;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;
const StyledInputCheckbox = styled.input`
	width: 100px;
`;
const InputLabel = styled.div`
	font-size: 12px;
	color: #757575;
`;
const BtnsCont = styled.div`
    width: 100%;
    margin-bottom: 25px;
	display: flex;
	flex-direction: row;
	align-items: center;
`;
const SeparatorLine = styled.div`
    width: 100%;
    margin: 40px 0;
    height: 1px;
    background-color: #eeeeee;
`;

const mapStateToProps = store => ({performers: store.performers});
export default connect(mapStateToProps, null, null, {pure: false})(PerformersEditWithRouter);
