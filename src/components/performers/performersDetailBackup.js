import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../../reducers';
import {withRouter, Redirect} from 'react-router-dom';
import styled from 'styled-components';
import DeleteButton from '../buttons/deleteButton';
import EditButton from '../buttons/editButton';

const PerformersDetailWithRouter = withRouter(props => <PerformersDetail {...props}/>);

class PerformersDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {sendResStatus: 'wait', photos: []};
        this.getData = this.getData.bind(this);
        this.delete = this.delete.bind(this);
        this.toNav = this.toNav.bind(this);
    }
    componentDidMount() {
        store.dispatch({type: 'PERFORMERS_DETAIL_DATA', performersDetailData: {}});
        this.getData();
    }
    getData() {
        fetch('/api/performers/' + this.props.match.params.id, {method: 'POST'})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => {
                console.log(resJson);
                resJson.data.performerStatus = resJson.data.performerStatus === 'request' ? 'Заявка' : resJson.data.performerStatus === 'verified' ? 'Аккредитован' : '- - -';
                store.dispatch({type: 'PERFORMERS_DETAIL_DATA', performersDetailData: resJson});
                this.toNav();
            });
    }
    delete(e) {
        e.preventDefault();
        store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: true});
        fetch('/api/performers/' + this.props.match.params.id + '/delete', {method: 'POST'})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => {
                console.log(resJson);
                store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false});
                if (resJson.status === 'ok') this.setState({sendResStatus: resJson.status});
            });
    }
    toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/performers', name: 'Исполнители'}, {path: this.props.location.pathname, name: this.props.performers.performersDetailData.data.name}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: true, name: 'Добавить исполнителя +', path: '/performers/add'}, search: {display: true,}}});
    }
    render() {
        return (
            <Wrapper>
                {this.props.performers.performersDetailData.hasOwnProperty('data') ?
                    <Data style={{paddingTop: '40px'}}>
                        <DataItem>
                            <DataItemName>Статус</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.performerStatus ? this.props.performers.performersDetailData.data.performerStatus : '- - -'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Канал привлечения</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.hasOwnProperty('attractionChannel') && this.props.performers.performersDetailData.data.attractionChannel ? this.props.performers.performersDetailData.data.attractionChannel : '- - -'}</DataItemValue>
                        </DataItem>
                        <SeparatorLine/>
                        <DataItem>
                            <DataItemName>ФИО</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.name ? this.props.performers.performersDetailData.data.name : '- - -'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Город</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.city ? this.props.performers.performersDetailData.data.city : '- - -'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Адрес</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.address ? this.props.performers.performersDetailData.data.address : '- - -'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Телефон</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.phone ? this.props.performers.performersDetailData.data.phone : '- - -'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Почта</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.mail ? this.props.performers.performersDetailData.data.mail : '- - -'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Пароль</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.password ? this.props.performers.performersDetailData.data.password : '- - -'}</DataItemValue>
                        </DataItem>
                        <SeparatorLine/>
                        <DataItem>
                            <DataItemName>Виды работ</DataItemName>
                            <DataItemValue>
                                {
                                    this.props.performers.performersDetailData.data.work.map((elNames, i) => {
                                        return <DataItemAdditionalValue key={i}> - {elNames === 'roof' ? 'Кровля' : elNames === 'aprons' ? 'Козырьки' : elNames === 'perimeter' ? 'Периметр' : elNames}</DataItemAdditionalValue>
                                    })
                                }
                            </DataItemValue>
                        </DataItem>

                        {
                            /*<DataItem>
                                <DataItemName>Бригада</DataItemName>
                                <DataItemValue>{this.props.performers.performersDetailData.data.brigade.brigadeType ? this.props.performers.performersDetailData.data.brigade.brigadeType : '- - -'}</DataItemValue>
                            </DataItem>*/
                        }

                        <DataItem>
                            <DataItemName>Снаряжение, инструменты, техника</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.tool ? this.props.performers.performersDetailData.data.tool : '- - -'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Лицензии</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.license ? this.props.performers.performersDetailData.data.license : '- - -'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Образование</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.education ? this.props.performers.performersDetailData.data.education : '- - -'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Опыт лет</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.experienceYears ? this.props.performers.performersDetailData.data.experienceYears : '- - -'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Опыт подробно</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.experienceDetail ? this.props.performers.performersDetailData.data.experienceDetail : '- - -'}</DataItemValue>
                        </DataItem>
                        <SeparatorLine/>
                        <DataItem>
                            <DataItemName>Автомобиль</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.car ? 'Да' : 'Нет'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>ИП</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.individualEntrepreneur ? 'Да' : 'Нет'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>ООО</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.ltd ? 'Да' : 'Нет'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Пропуск в Северск</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.passToSeversk ? 'Да' : 'Нет'}</DataItemValue>
                        </DataItem>
                        <DataItem>
                            <DataItemName>Примечания</DataItemName>
                            <DataItemValue>{this.props.performers.performersDetailData.data.other ? this.props.performers.performersDetailData.data.other : '- - -'}</DataItemValue>
                        </DataItem>
                        <SeparatorLine/>
                        <BtnsCont>
                            {this.props.navigation.navigationPaths.length === 2 ?
                                <BtnsCont>
                                    <EditButton path={this.props.navigation.navigationPaths[1].path + '/edit'}/>
                                    <DeleteButton callback={this.delete}/>
                                </BtnsCont>
                                : null}
                        </BtnsCont>
                    </Data>
                    : '- - -'}
                {this.state.sendResStatus === 'ok' ? <Redirect to={'/performers'}/> : null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  padding: 4px;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`;
const Data = styled.div`
  padding: 10px;
  background-color: #ffffff;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  flex-wrap: wrap;
`;
const DataItem = styled.div`
  width: 200px;
  margin-bottom: 20px;
`;
const DataItemName = styled.div`
  font-size: 12px;
  color: #757575;
`;
const DataItemValue = styled.div`
  font-size: 14px;
  color: #212121;
`;
const DataItemAdditionalValue = styled.div`
  padding-left: 10px;
  font-size: 12px;
`;
const SeparatorLine = styled.div`
    width: 100%;
    margin: 40px 0;
    height: 1px;
    background-color: #eeeeee;
`;
const BtnsCont = styled.div`
  width: 100%;
  margin-bottom: 15px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;

const mapStateToProps = store => ({performers: store.performers, navigation: store.navigation});
export default connect(mapStateToProps, null, null, {pure: false})(PerformersDetailWithRouter);
