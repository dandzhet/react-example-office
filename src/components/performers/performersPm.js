import React, { Component } from 'react';
import { connect } from "react-redux";
//import store from './../reducers';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

class PerformersPm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    let toPath = '/performers/' + this.props.id;
    let location = [
      { path: '/performers', name: 'Исполнители' },
      { path: toPath, name: this.props.name }
    ];
    return (
      <Wrapper exact={true} to={{ pathname: toPath, state: location }}>
        <Title>{this.props.name}</Title>
      </Wrapper>
    );
  }
}

const Wrapper = styled(NavLink)`
    width: calc(100% - 40px);
    padding: 20px;
    cursor: pointer;
    text-decoration: none;
    &:hover {
        background-color: #eaeaea;
    }
`;
const Title = styled.div`
    //font-weight: 900;
    font-size: 14px;
    color: #212121;
`;
/*const WorkNamesCont = styled.div`
    padding-top: 10px;
    font-weight: 400;
    font-size: 12px;
    color: #757575;
    display: flex;
    flex-direction: row;
`;
const WorkNames = styled.div`
    display: flex;
    flex-direction: row;
    padding-right: 10px;
`;
const WorkNamesItemsCont = styled.div`
  padding-left: 5px;
  display: flex;
  flex-direction: row;
  font-size: 10px;
`;
const WorkNamesItems = styled.div`
  padding: 0 3px;
`;*/

const mapStateToProps = (store) => {
  return {
    performers: store.performers
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(PerformersPm);
