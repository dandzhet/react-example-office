import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../../reducers';
import {withRouter, Redirect} from 'react-router-dom';
import styled from 'styled-components';
import PerformersAddWorkInput from './performersAddWorkInputTest';
import PerformersAddBrigadeInput from './performersAddBrigadeInput';
import SaveButton from '../buttons/saveButton';

const PerformersAddWithRouter = withRouter(props => <PerformersAdd {...props}/>);

class PerformersAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {sendResStatus: 'wait'};
        this.send = this.send.bind(this);
        PerformersAdd.toNav = PerformersAdd.toNav.bind(this);
    }

    componentDidMount() {PerformersAdd.toNav()}

    send(e) {
        e.preventDefault();
        let obj = {};
        for (let i = 0; i < e.target.length - 1; i++) {
            if (e.target[i].type === 'checkbox') obj[e.target[i].name] = e.target[i].checked;
            else obj[e.target[i].name] = e.target[i].value;
            if (e.target[i].name === 'work') obj[e.target[i].name] = JSON.parse(e.target[i].value);
        }
        console.log(obj);
        fetch('/api/performers/add', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(obj)})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => {
                console.log(resJson);
                if (resJson.status === 'ok') this.setState({sendResStatus: resJson.status});
            });
    }

    static toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/performers', name: 'Исполнители'}, {path: '/performers/add', name: 'Добавление'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: false}, search: {display: true}}});
    }

    render() {
        return (
            <Wrapper>
                <StyledForm onSubmit={this.send}>
                    <InputCont>
                        <InputLabel>Статус</InputLabel>
                        <select name='performerStatus' style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}}>
                            <option>Заявка</option>
                            <option>Аккредитован</option>
                        </select>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Канал привлечения</InputLabel>
                        <StyledInput name='channelOfAttraction' type='text'/>
                    </InputCont>
                    <Separator>Контактная информация</Separator>
                    <InputCont>
                        <InputLabel>ФИО</InputLabel>
                        <StyledInput required name='name' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Город</InputLabel>
                        <StyledInput name='city' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Адрес</InputLabel>
                        <StyledInput name='address' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Телефон</InputLabel>
                        <StyledInput name='phone' type='tel'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Почта</InputLabel>
                        <StyledInput name='mail' type='email'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Пароль</InputLabel>
                        <StyledInput name='password' type='password'/>
                    </InputCont>
                    <Separator>Основная информация</Separator>
                    <InputCont>
                        <InputLabel>Виды работ</InputLabel>
                        <PerformersAddWorkInput/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Снаряжение, инструменты, техника</InputLabel>
                        <StyledInput style={{width: '272px'}} name='tool'/>
                    </InputCont>
                    <InputCont style={{width: '100%'}}>
                        <InputLabel>Бригада</InputLabel>
                        <PerformersAddBrigadeInput/>
                    </InputCont>
                    <Separator>Резюме</Separator>
                    <InputCont style={{width: '100%'}}>
                        <InputLabel>Опыт лет</InputLabel>
                        <StyledInput name='experienceYears' type='number'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Опыт подробно</InputLabel>
                        <StyledInput style={{width: '272px'}} name='experienceDetail'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Лицензии</InputLabel>
                        <StyledInput style={{width: '272px'}} name='license'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Образование</InputLabel>
                        <StyledInput style={{width: '272px'}} name='education'/>
                    </InputCont>
                    <Separator>Дополнительная информация</Separator>
                    <InputCont>
                        <InputLabel>Автомобиль</InputLabel>
                        <StyledInputCheckbox name='car' type='checkbox'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>ИП</InputLabel>
                        <StyledInputCheckbox name='individualEntrepreneur' type='checkbox'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>ООО</InputLabel>
                        <StyledInputCheckbox name='ltd' type='checkbox'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Северск</InputLabel>
                        <StyledInputCheckbox name='passToSeversk' type='checkbox'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Примечания</InputLabel>
                        <StyledInput style={{width: '272px'}} name='other'/>
                    </InputCont>
                    <Separator><SaveButton/></Separator>
                </StyledForm>
                {this.state.sendResStatus === 'ok' ? <Redirect to={'/performers'}/> : null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    padding: 4px;
    position: relative;
`;
const StyledForm = styled.form`
    padding: 10px;
    background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;
const StyledInputCheckbox = styled.input`
    width: 100px;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    padding-top: 40px;
    font-weight: 900;
    color: #212121;
    font-size: 16px;
`;

const mapStateToProps = store => ({performers: store.performers, dialog: store.dialog});
export default connect(mapStateToProps, null, null, {pure: false})(PerformersAddWithRouter);
