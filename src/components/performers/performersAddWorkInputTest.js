import React, { Component } from 'react';
import { connect } from "react-redux";
//import store from '../../reducers';
import styled from 'styled-components';

class PerformersAddWorkInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            snowRoofActive: false,
            snowApronsActive: false,
            snowPerimeterActive: false,
            inputValue: [],
        };
        this.workNameSelect = this.workNameSelect.bind(this);
        this.setInputValue = this.setInputValue.bind(this);
    }
    workNameSelect(e, stateActiveName) {
        let state = this.state;
        state[stateActiveName] = !this.state[stateActiveName];
        let activeNames = [];
        if (state.snowRoofActive) activeNames.push('roof');
        if (state.snowApronsActive) activeNames.push('aprons');
        if (state.snowPerimeterActive) activeNames.push('perimeter');
        state.inputValue = activeNames;
        this.setState(state);
    }
    setInputValue() {}
    render() {
        return (
            <Wrapper>
                <input name='work' type='hidden' value={JSON.stringify(this.state.inputValue)}/>
                <SelectCont>
                    <SelectCollectionCont>
                        <SelectCollectionItemsCont>
                            {this.state.snowRoofActive ? <SelectCollectionItemActive onClick={(e) => {this.workNameSelect(e, 'snowRoofActive')}}>Кровля</SelectCollectionItemActive> : <SelectCollectionItem onClick={(e) => {this.workNameSelect(e, 'snowRoofActive')}}>Кровля</SelectCollectionItem>}
                            {this.state.snowApronsActive ? <SelectCollectionItemActive onClick={(e) => {this.workNameSelect(e, 'snowApronsActive')}}>Козырьки</SelectCollectionItemActive> : <SelectCollectionItem onClick={(e) => {this.workNameSelect(e, 'snowApronsActive')}}>Козырьки</SelectCollectionItem>}
                            {this.state.snowPerimeterActive ? <SelectCollectionItemActive onClick={(e) => {this.workNameSelect(e, 'snowPerimeterActive')}}>Периметр</SelectCollectionItemActive> : <SelectCollectionItem onClick={(e) => {this.workNameSelect(e, 'snowPerimeterActive')}}>Периметр</SelectCollectionItem>}
                        </SelectCollectionItemsCont>
                    </SelectCollectionCont>
                </SelectCont>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    position: relative;
    width: 284px;
    height: 121px;
    background-color: #ffffff;
    border: 1px solid #eeeeee;
    border-radius: 5px;
    overflow: scroll;
`;
const SelectCont = styled.div`
    padding: 5px 0;
    position: relative;
`;
const SelectCollectionCont = styled.div``;
const SelectCollectionItemsCont = styled.div``;
const SelectCollectionItem = styled.div`
    padding: 5px;
    padding-left: 10px;
    font-size: 12px;
    color: #212121;
    cursor: pointer;
    &:hover {
        background-color: #eeeeee;
    }
`;
const SelectCollectionItemActive = styled.div`
    padding: 5px;
    padding-left: 10px;
    font-size: 12px;
    color: #eeeeee;
    cursor: pointer;
    background-color: #bdbdbd;
`;

const mapStateToProps = store => ({performers: store.performers});
export default connect(mapStateToProps, null, null, { pure: false })(PerformersAddWorkInput);
