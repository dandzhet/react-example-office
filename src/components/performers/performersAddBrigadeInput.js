import React, {Component} from 'react';
import {connect} from "react-redux";
//import store from '../../reducers';
import styled from 'styled-components';

class PerformersAddBrigadeInput extends Component {
    constructor(props) {
        super(props);
        this.state = {brigadeTypeSelected: 'Без бригады'};
        this.brigadeSelect = this.brigadeSelect.bind(this);
        this.getData = this.getData.bind(this);
    }

    componentDidMount() {
        this.getData();
    }

    brigadeSelect(e) {
        this.setState({brigadeTypeSelected: e.target.value});
    }

    getData() {

    }

    render() {
        return (
            <Wrapper>
                <InputCont>
                    <InputLabel>Бригада</InputLabel>
                    <select style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}} ref={elem => this.selectRef = elem} onChange={this.brigadeSelect} name='brigade'>
                        <option>Без бригады</option>
                        <option>Бригадир</option>
                        <option>В составе бригады</option>
                    </select>
                </InputCont>
                {
                    this.state.brigadeTypeSelected === 'Бригадир' ?
                        <InputCont>
                            <InputLabel>Чел. в бригаде</InputLabel>
                            <StyledInput type='number' name='numberOfPerformers'/>
                        </InputCont>
                        : null
                }
                {
                    this.state.brigadeTypeSelected === 'В составе бригады' ?
                        <InputCont>
                            <InputLabel>Бригадир</InputLabel>
                            <select style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}}>
                                <option>- - -</option>
                            </select>
                        </InputCont>
                        : null
                }
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: row;
`;
const InputCont = styled.div`
    margin-right: 10px;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;

const mapStateToProps = store => ({performers: store.performers});
export default connect(mapStateToProps)(PerformersAddBrigadeInput);
