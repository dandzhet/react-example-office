import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import styled from 'styled-components';

import Home from './home';
import Feed from './feed';
import Applications from './applications';
import ApplicationsAdd from './applications/applicationAdd';
import ApplicationsDetail from './applications/applicationDetail';
import Stages from './stages';
import Performers from './performers';
import PerformersAdd from './performers/performersAdd';
import PerformersDetail from './performers/performersDetail';
import PerformersEdit from './performers/performersEdit';
import Customers from './customers';
import CustomersAdd from './customers/customerAdd';
import CustomersDetail from './customers/customerDetail';
import CustomersEdit from './customers/customerEdit';
import Objects from './objects';
import ObjectAdd from './objects/objectAdd';
import ObjectDetail from './objects/objectDetail';
import ObjectEdit from './objects/objectEdit';
import Work from './work/workNew';
import WorkSnow from './work/workSnow';
import WorkSnowRoof from './work/workSnowRoof';
import WorkSnowAprons from './work/workSnowAprons';
import WorkSnowPerimeter from './work/workSnowPerimeter';
import Calc from './calc';
import CalcStretchCeiling from './calc/stretchСeiling';
import WellBoringCalc from './calc/wellBoring';
import SnowCalc from './calc/snow';

class Content extends Component {
  render() {
    return (
        <Wrapper>
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route exact path='/feed' component={Feed}/>
                <Route exact path='/applications' component={Applications}/>
                <Route exact path='/applications/add' component={ApplicationsAdd}/>
                <Route exact path='/applications/:id' component={ApplicationsDetail}/>
                <Route exact path='/stages' component={Stages}/>
                <Route exact path='/performers' component={Performers}/>
                <Route exact path='/performers/add' component={PerformersAdd}/>
                <Route exact path='/performers/:id' component={PerformersDetail}/>
                <Route exact path='/performers/:id/edit' component={PerformersEdit}/>
                <Route exact path='/customers' component={Customers}/>
                <Route exact path='/customers/add' component={CustomersAdd}/>
                <Route exact path='/customers/:id' component={CustomersDetail}/>
                <Route exact path='/customers/:id/edit' component={CustomersEdit}/>
                <Route exact path='/objects' component={Objects}/>
                <Route exact path='/objects/add' component={ObjectAdd}/>
                <Route exact path='/objects/:id' component={ObjectDetail}/>
                <Route exact path='/objects/:id/edit' component={ObjectEdit}/>
                <Route exact path='/work' component={Work}/>
                <Route exact path='/work/snow' component={WorkSnow}/>
                <Route exact path='/work/snow/roof' component={WorkSnowRoof}/>
                <Route exact path='/work/snow/aprons' component={WorkSnowAprons}/>
                <Route exact path='/work/snow/perimeter' component={WorkSnowPerimeter}/>
                <Route exact path='/calc' component={Calc}/>
                <Route exact path='/calc/snow' component={SnowCalc}/>
                <Route exact path='/calc/stretchceiling' component={CalcStretchCeiling}/>
                <Route exact path='/calc/wellboring' component={WellBoringCalc}/>
            </Switch>
        </Wrapper>
    );
  }
}

const Wrapper = styled.div`
    position: relative;
    top: 36px;
    left: 180px;
    width: calc(100% - 180px);
    //height: 120vh;
`;

export default Content;