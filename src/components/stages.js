import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../reducers';
import styled from 'styled-components';

class Stages extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.getData = this.getData.bind(this);
    this.toStore = this.toStore.bind(this);
    this.toNav = this.toNav.bind(this);
  }
  componentDidMount() {
    this.toNav();
    //this.getData();
  }
  getData() {
    fetch('/api/applications', { method: 'POST' })
    .then(res => {
      if (res.status !== 200) {
          console.log('Err: ' + res.statusText + ' ' + res.status);
          return;
      }
      res.json().then((json) => {
        console.log(json);
        this.toStore('APPLICATIONS_DATA', 'applicationsData', json.data);
      });
    });
  }
  toNav() {
    let location = [
      { path: '/stages', name: 'Этапы' },
    ];
    let controls = {
      addBtn: {
        display: true,
        name: 'Добавить заявку +',
        path: '/applications/add',
      },
      search: {
        display: true,
      }
    };
    this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
    this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
  }
  toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
  render() {
    return (
      <Wrapper>
        <Container>
            {/*<StageContainer>
                <StageName style={{backgroundColor: '#f5f5f5'}}>
                    #0 Отложенные
                </StageName>
            </StageContainer>*/}
            <StageContainer>
                <StageName style={{backgroundColor: '#e1eeff'}}>
                    #1 Поиск Исполнителя на Осмотр
                </StageName>
            </StageContainer>
            <StageContainer>
                <StageName style={{backgroundColor: '#bedaff'}}>
                    #2 Исполнитель на Осмотр найден
                </StageName>
            </StageContainer>
            {/*<StageContainer>
                <StageName style={{backgroundColor: '#8dbeff'}}>
                    #3 Идет осмотр
                </StageName>
            </StageContainer>*/}
            <StageContainer>
                <StageName style={{backgroundColor: '#8dbeff'}}>
                    #3 Осмотр завершен
                </StageName>
            </StageContainer>
            <StageContainer>
                <StageName style={{backgroundColor: '#fff4df'}}>
                    #4 Поиск Исполнителя на Вып. работ
                </StageName>
            </StageContainer>
            <StageContainer>
                <StageName style={{backgroundColor: '#ffe6b8'}}>
                    #5 Исполнитель на Вып. работ найден
                </StageName>
            </StageContainer>
            {/*<StageContainer>
                <StageName style={{backgroundColor: '#ffd07a'}}>
                    #7 Идет Выполнение работ
                </StageName>
            </StageContainer>*/}
            {/*<StageContainer>
                <StageName style={{backgroundColor: '#ffd07a'}}>
                    #6 Выполнение работ завершено
                </StageName>
            </StageContainer>*/}
            {/*<StageContainer>
                <StageName style={{backgroundColor: '#ff7e7e'}}>
                    #6 Отмененные
                </StageName>
            </StageContainer>*/}
            {/*<StageContainer>
                <StageName style={{backgroundColor: '#baff8f'}}>
                    #7 Завершенные
                </StageName>
            </StageContainer>*/}
        </Container>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
`;
const Container = styled.div`
    width: calc(100% - 8px);
    margin-top: 4px;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: space-between;
    overflow: scroll;
`;
const StageContainer = styled.div`
    width: calc(20% - 4px);
    height: 400px;
    background-color: #ffffff;
    flex-shrink: 0;
`;
const StageName = styled.div`
    padding: 10px 0;
    font-size: 10px;
    font-weight: 900;
    color: #424242;
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`;

const mapStateToProps = (store) => {
  return {
    applications: store.applications
  };
};

export default connect(mapStateToProps)(Stages);
