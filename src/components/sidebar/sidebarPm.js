import React, { Component } from 'react';
import { connect } from "react-redux";
//import store from './../reducers';
import { NavLink, withRouter } from 'react-router-dom';
import styled from 'styled-components';

const SidebarPmWithRouter = withRouter(props => <SidebarPm {...props}/>);

class SidebarPm extends Component {
  constructor(props) {
    super(props);
    this.state = {icon: null};
    this.selectIcon = this.selectIcon.bind(this);
  }
  selectIcon() {
    let icon;
    switch (this.props.name) {
        case 'Главная':
            icon = <i className="fas fa-home"></i>;
            break;
        case 'Оповещения':
            icon = <i className="fas fa-rss-square"></i>;
            break;
        case 'Заявки':
            icon = <i className="fas fa-tasks"></i>;
            break;
        case 'Этапы':
            icon = <i className="fas fa-th-list"></i>;
            break;
        case 'Исполнители':
            icon = <i className="fas fa-people-carry"></i>;
            break;
        case 'Заказчики':
            icon = <i className="fas fa-users"></i>;
            break;
        case 'Объекты':
            icon = <i className="fas fa-city"></i>;
            break;
        case 'Виды работ':
            icon = <i className="fas fa-toolbox"></i>;
            break;
        case 'Расчет':
            icon = <i className="fas fa-ruble-sign"></i>;
            break;
        default:
            break;
    }
    this.setState({icon:icon});
  }
  componentDidMount() {
    this.selectIcon();
  }
  render() {
    let location = [
        { path: this.props.link, name: this.props.name }
    ];
    let exact = false;
    if (this.props.link === '/') exact = true;
    return (
        <Wrapper>
            <StyledLink exact={exact} to={{ pathname: this.props.link, state: location }}>
                <IconCont>{this.state.icon}</IconCont>
                {this.props.name}
            </StyledLink>
        </Wrapper>
    );
  }
}

const Wrapper = styled.div`
    position: relative;
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`;
const StyledLink = styled(NavLink)`
    width: calc(100% - 40px);
    padding: 12px 20px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
    text-decoration: none;
    color: #757575;
    font-size: 12px;
    &:hover {
        color: rgb(175, 175, 175);
    }
    &.active {
        color: #ffffff;
    }
`;
const IconCont = styled.div`
    width: 30px;
`;

const mapStateToProps = (store) => {
  return {
        sidebar: store.sidebar,
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(SidebarPmWithRouter);
