import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import { withRouter, Redirect } from 'react-router-dom';
import styled from 'styled-components';

import SaveButton from '../buttons/saveButton';
import DeleteButton from '../buttons/deleteButton';

const WorkNamesEditWithRouter = withRouter(props => <WorkNamesEdit {...props}/>);

class WorkNamesEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
        sendResStatus: 'wait', 
        data: {
            name: '',
            unit: 'м',
            manHours: '',
            price: '',
            performerPrice: '',
        }
    };
    this.nameChange = this.nameChange.bind(this);
    this.unitChange = this.unitChange.bind(this);
    this.manHoursChange = this.manHoursChange.bind(this);
    this.priceChange = this.priceChange.bind(this);
    this.performerPriceChange = this.performerPriceChange.bind(this);
    this.getData = this.getData.bind(this);
    this.send = this.send.bind(this);
    this.delete = this.delete.bind(this);
    this.toStore = this.toStore.bind(this);
    this.toNav = this.toNav.bind(this);
  }
  componentDidMount() {
    this.getData();
  }
  nameChange(e) {
    let detailData = this.state.data;
    detailData.name = e.target.value;
    this.setState({data: detailData});
  }
  unitChange(e) {
    let detailData = this.state.data;
    detailData.unit = e.target.value;
    this.setState({data: detailData});
  }
  manHoursChange(e) {
    let detailData = this.state.data;
    detailData.manHours = e.target.value;
    this.setState({data: detailData});
  }
  priceChange(e) {
    let detailData = this.state.data;
    detailData.price = e.target.value;
    this.setState({data: detailData});
  }
  performerPriceChange(e) {
    let detailData = this.state.data;
    detailData.performerPrice = e.target.value;
    this.setState({data: detailData});
  }
  getData() {
    let path = '/api/work/' + this.props.match.params.id + '/' + this.props.match.params.workNameId;
    fetch(path, {method: 'POST'})
    .then(res => {
      if (res.status !== 200) {
          console.log('Err: ' + res.statusText + ' ' + res.status);
          return;
      };
      res.json().then((data) => {
        console.log(data);
        this.setState({
            data: {
                name: data.hasOwnProperty('name') ? data.name : '',
                unit: data.hasOwnProperty('unit') ? data.unit : 'м',
                //manHours: data.hasOwnProperty('manHours') ? data.manHours : '',
                price: data.hasOwnProperty('price') ? data.price : '',
                performerPrice: data.hasOwnProperty('performerPrice') ? data.performerPrice : '',
            }
        });
        this.toStore('WORK_DETAIL_DATA', 'workDetailData', data);
        this.toNav();
      });
    });
  }
  send(e) {
    e.preventDefault();
    let obj = {};
    for (let i = 0; i < e.target.length - 1; i++) {
        obj[e.target[i].name] = e.target[i].value;
    }
    let path = '/api/work/' + this.props.match.params.id + '/' + this.props.match.params.workNameId + '/update';
    fetch(path, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(obj)
    }).then(res => {
      if (res.status !== 200) {
          console.log('Err: ' + res.statusText + ' ' + res.status);
          return;
      };
      res.json().then((data) => {
        console.log(data);
        if (data.status === 'ok') {
            this.setState({sendResStatus: data.status});
        }
      });
    });
  }
  delete(e) {
    e.preventDefault();
    let path = '/api/work/' + this.props.match.params.id + '/' + this.props.match.params.workNameId + '/delete';
    fetch(path, {
        method: 'POST',
    }).then(res => {
      if (res.status !== 200) return console.log('Err: ' + res.statusText + ' ' + res.status);
      res.json().then((data) => {
        console.log(data);
        if (data.status === 'ok') this.setState({sendResStatus: data.status});
      });
    })
  }
  toNav() {
    let collectionName = this.props.work.workDetailData.hasOwnProperty('workType') && this.props.work.workDetailData.workType.hasOwnProperty('name') ? this.props.work.workDetailData.workType.name : 'NULL';
    let workName = this.props.work.workDetailData.hasOwnProperty('name') ? this.props.work.workDetailData.name : 'NULL';
    let splitPath = this.props.location.pathname.split('/');
    let location = [
      { path: '/work', name: 'Виды работ' },
      { path: '/work/' + splitPath[2], name: collectionName },
      { path: '/work/' + splitPath[2] + '/' + splitPath[3], name: workName },
      { path: this.props.location.pathname, name: 'Редактирование' },
    ];
    let controls = {
      addBtn: {
        display: true,
        name: 'Добавить наименование работ +',
        path: '/work/' + this.props.match.params.id + '/add',
      },
      search: {
        display: true,
      }
    };
    this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
    this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
  }
  toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
  render() {
    return (
      <Wrapper>
          <StyledForm onSubmit={this.send}>
            <InputCont>
                <InputLabel>Наименование работ</InputLabel>
                <StyledInput required value={this.state.data.name} onChange={this.nameChange} name='name' type='text'></StyledInput>
            </InputCont>
            <InputCont>
                <InputLabel>Еденица измерения</InputLabel>
                <select value={this.state.data.unit} onChange={this.unitChange} style={{width: '140px'}} name='unit'>
                    <option>м</option>
                    <option>м2</option>
                    <option>м3</option>
                    <option>кг</option>
                    <option>Шт</option>
                </select>
            </InputCont>
            <InputCont>
                <InputLabel>Стоимость</InputLabel>
                <StyledInput value={this.state.data.price} onChange={this.priceChange} name='price' type='text'></StyledInput>
            </InputCont>
            <InputCont>
                <InputLabel>Зарплата</InputLabel>
                <StyledInput value={this.state.data.performerPrice} onChange={this.performerPriceChange} name='performerPrice' type='text'></StyledInput>
            </InputCont>
            {/*<InputCont>
                <InputLabel>Человеко-часы</InputLabel>
                <StyledInput value={this.state.data.manHours} onChange={this.manHoursChange} name='manHours' type='text'></StyledInput>
            </InputCont>*/}
            <Separator>
                <SaveButton/>
                <DeleteButton callback={this.delete}/>
            </Separator>
          </StyledForm>
          {this.state.sendResStatus === 'ok' ? <Redirect to={'/work/' + this.props.match.params.id}/> : null}
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
    padding: 4px;
    position: relative;
`;
const StyledForm = styled.form`
  padding: 10px;
  background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const StyledInput = styled.input``;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    padding-top: 20px;
    font-weight: 900;
    color: #212121;
    font-size: 16px;
`;

const mapStateToProps = (store) => {
  return {
    work: store.work,
    dialog: store.dialog
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(WorkNamesEditWithRouter);
