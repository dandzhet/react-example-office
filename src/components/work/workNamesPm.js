import React, { Component } from 'react';
import { connect } from "react-redux";
//import store from './../reducers';
import { NavLink, withRouter } from 'react-router-dom';
import styled from 'styled-components';

const WorkNamesPmWithRouter = withRouter(props => <WorkNamesPm {...props}/>);

class WorkNamesPm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    let toPath = this.props.location.pathname + '/' + this.props.id;
    return (
      <Wrapper exact={true} to={{ pathname: toPath }}>
        <Title>{this.props.name}</Title>
      </Wrapper>
    );
  }
}

const Wrapper = styled(NavLink)`
    width: calc(100% - 40px);
    padding: 20px;
    cursor: pointer;
    text-decoration: none;
    &:hover {
        background-color: #eaeaea;
    }
`;

const Title = styled.div`
    //font-weight: 900;
    font-size: 14px;
    color: #212121;
`;

const mapStateToProps = (store) => {
  return {
    work: store.work
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(WorkNamesPmWithRouter);
