import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import { withRouter, Redirect } from 'react-router-dom';
import styled from 'styled-components';

import DeleteButton from '../buttons/deleteButton';
import EditButton from '../buttons/editButton';

const WorkDetailWithRouter = withRouter(props => <WorkDetail {...props}/>);

class WorkDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {sendResStatus: 'wait'};
    this.getData = this.getData.bind(this);
    this.delete = this.delete.bind(this);
    this.toStore = this.toStore.bind(this);
    this.toNav = this.toNav.bind(this);
  }
  componentDidMount() {
    this.toStore('WORK_DETAIL_DATA', 'workDetailData', {});
    this.getData();
  }
  getData() {
    let path = '/api/work/' + this.props.match.params.id + '/' + this.props.match.params.workNameId;
    fetch(path, {method: 'POST'})
    .then(res => {
      if (res.status !== 200) {
          console.log('Err: ' + res.statusText + ' ' + res.status);
          return;
      };
      res.json().then((data) => {
        console.log(data);
        this.toStore('WORK_DETAIL_DATA', 'workDetailData', data);
        this.toNav();
      });
    });
  }
  delete(e) {
    e.preventDefault();
    let path = '/api/work/' + this.props.match.params.id + '/' + this.props.match.params.workNameId + '/delete';
    fetch(path, {
        method: 'POST',
    }).then(res => {
      if (res.status !== 200) return console.log('Err: ' + res.statusText + ' ' + res.status);
      res.json().then((data) => {
        console.log(data);
        if (data.status === 'ok') this.setState({sendResStatus: data.status});
      });
    })
  }
  toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
  toNav() {
    let collectionName = this.props.work.workDetailData.hasOwnProperty('workType') && this.props.work.workDetailData.workType.hasOwnProperty('name') ? this.props.work.workDetailData.workType.name : 'NULL';
    let workName = this.props.work.workDetailData.hasOwnProperty('name') ? this.props.work.workDetailData.name : 'NULL';
    let splitPath = this.props.location.pathname.split('/');
    let location = [
      { path: '/work', name: 'Виды работ' },
      { path: '/work/' + splitPath[2], name: collectionName },
      { path: this.props.location.pathname, name: workName }
    ];
    let controls = {
      addBtn: {
        display: true,
        name: 'Добавить наименование работ +',
        path: '/work/' + this.props.match.params.id + '/add',
      },
      search: {
        display: true,
      }
    };
    this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
    this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
  }
  render() {
    return (
      <Wrapper>
        {this.props.work.workDetailData ? 
            <Data>
                {/*<DataItem>
                  <DataItemName>Тип работ</DataItemName>
                  <DataItemValue>{this.props.work.workDetailData.hasOwnProperty('workType') ? this.props.work.workDetailData.workType.name : '- - -'}</DataItemValue>
                </DataItem>*/}
                <DataItem>
                  <DataItemName>Наименование работ</DataItemName>
                  <DataItemValue>{this.props.work.workDetailData.name}</DataItemValue>
                </DataItem>
                <DataItem>
                  <DataItemName>Единица измерения</DataItemName>
                  <DataItemValue>{this.props.work.workDetailData.hasOwnProperty('unit') ? this.props.work.workDetailData.unit : '- - -'}</DataItemValue>
                </DataItem>
                <DataItem>
                  <DataItemName>Стоимость (руб.)</DataItemName>
                  <DataItemValue>{this.props.work.workDetailData.hasOwnProperty('price') ? this.props.work.workDetailData.price : '- - -'} </DataItemValue>
                </DataItem>
                <DataItem>
                  <DataItemName>Зарплата (руб.)</DataItemName>
                  <DataItemValue>{this.props.work.workDetailData.hasOwnProperty('performerPrice') ? this.props.work.workDetailData.performerPrice : '- - -'} </DataItemValue>
                </DataItem>
                {/*<DataItem>
                  <DataItemName>Трудозатраты (ч.)</DataItemName>
                  <DataItemValue>{this.props.work.workDetailData.hasOwnProperty('manHours') ? this.props.work.workDetailData.manHours : '- - -'} </DataItemValue>
                </DataItem>*/}
                {this.props.navigation.navigationPaths.length === 3 ? 
                  <BtnsCont>
                    <EditButton path={this.props.navigation.navigationPaths[2].path + '/edit'}/>
                    <DeleteButton callback={this.delete}/>
                  </BtnsCont>
                : null}
            </Data>
        : '- - -'}
        {this.state.sendResStatus === 'ok' ? <Redirect to={'/work/' + this.props.match.params.id}/> : null}
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  width: calc(100% - 8px);
  padding: 4px;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`;
const Data = styled.div`
width: calc(100% - 20px);
padding: 10px;
background-color: #ffffff;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  flex-wrap: wrap;
  `;
const DataItem = styled.div`
width: 200px;
margin-bottom: 20px;
`;
const DataItemName = styled.div`
font-size: 12px;
color: #757575;
`;
const DataItemValue = styled.div`
font-size: 14px;
color: #212121;
`;
const BtnsCont = styled.div`
  width: 100%;
  margin-top: 20px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;

const mapStateToProps = (store) => {
  return {
    work: store.work,
    navigation: store.navigation,
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(WorkDetailWithRouter);
