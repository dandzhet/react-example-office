import React, { Component } from 'react';
import { connect } from "react-redux";
//import store from './../reducers';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

class WorkPm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    let toPath = '/work/' + this.props.id;
    let location = [
      { path: '/work', name: 'Работы' },
      { path: toPath, name: this.props.name }
    ];
    return (
      <Wrapper exact={true} to={{ pathname: toPath, state: location }}>
        <Title>{this.props.name}</Title>
      </Wrapper>
    );
  }
}

const Wrapper = styled(NavLink)`
    width: calc(100% - 40px);
    padding: 20px;
    cursor: pointer;
    text-decoration: none;
    &:hover {
        background-color: #eaeaea;
    }
`;

const Title = styled.div`
    //font-weight: 900;
    font-size: 14px;
    color: #212121;
`;

const mapStateToProps = (store) => {
  return {
    work: store.work
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(WorkPm);
