import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import { withRouter, Redirect } from 'react-router-dom';
import styled from 'styled-components';

import SaveButton from '../buttons/saveButton';

const WorkAddWithRouter = withRouter(props => <WorkAdd {...props}/>);

class WorkAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {sendResStatus: 'wait'};
    this.send = this.send.bind(this);
    this.toStore = this.toStore.bind(this);
    this.toNav = this.toNav.bind(this);
  }
  componentDidMount() {
    this.toNav();
  }
  send(e) {
    e.preventDefault();
    let obj = {};
    for (let i = 0; i < e.target.length - 1; i++) {
        obj[e.target[i].name] = e.target[i].value;
    }
    console.log(obj);
    let path = '/api/work/add';
    fetch(path, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(obj)
    }).then(res => {
      if (res.status !== 200) {
          console.log('Err: ' + res.statusText + ' ' + res.status);
          return;
      };
      res.json().then((data) => {
        console.log(data);
        if (data.status === 'ok') {
            this.setState({sendResStatus: data.status});
            //this.toStore('DIALOG_ACTIVE', 'dialogActive', true);
        }
      });
    });
  }
  toNav() {
    let location = [
      { path: '/work', name: 'Виды работ' },
      { path: '/work/add', name: 'Добавление' }
    ];
    let controls = {
      addBtn: {
        display: false,
        name: '',
        path: '',
      },
      search: {
        display: true,
      }
    };
    this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
    this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
  }
  toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
  render() {
    return (
      <Wrapper>
          <StyledForm onSubmit={this.send}>
            <InputCont>
                <InputLabel>Тип работ</InputLabel>
                <StyledInput required name='name' type='text'/>
                <InputLabel style={{fontSize: '12px'}}>Укажите название раздела видов работ, напрмер "Снег", далее в этом разделе добавьте наименования работ, например "Скатные кровли" с указанием подробной ифнормации</InputLabel>
            </InputCont>
            <Separator><SaveButton/></Separator>
          </StyledForm>
          {this.state.sendResStatus === 'ok' ? <Redirect to="/work"/> : null}
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
    padding: 4px;
    position: relative;
`;
const StyledForm = styled.form`
  padding: 10px;
  background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const StyledInput = styled.input``;
const InputLabel = styled.div`
    font-size: 14px;
    color: #757575;
`;
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    padding-top: 20px;
    font-weight: 900;
    color: #212121;
    font-size: 16px;
`;

const mapStateToProps = (store) => {
  return {
    work: store.work,
    dialog: store.dialog
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(WorkAddWithRouter);
