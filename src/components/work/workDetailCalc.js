import React, { Component } from 'react';
import styled from 'styled-components';

class WorkDetailCalc extends Component {
  constructor(props) {
    super(props);
    this.state = {
        resuilt: 0,
    };
    this.inputChange = this.inputChange.bind(this);
  }
  componentDidMount() {
      let float = parseFloat(this.props.manHours, 10) / parseFloat(this.props.unit, 10);
      this.setState({ resuilt: Math.round(float * 100) / 100 });
  }
  inputChange(e) {
    console.log(e.target.value);
    let newResult;
    if (!e.target.value || e.target.value === 0) {
        newResult = 0;
    } else {
        let manHour = parseFloat(this.props.manHours, 10) / parseFloat(this.props.unit, 10);
        let float = manHour * e.target.value;
        newResult = Math.round(float * 100) / 100;
    }
    this.setState({
        resuilt: newResult
    });
  }
  render() {
    return (
      <Wrapper>
        <InputCont>
            <InputLabel>{this.props.unitType}</InputLabel>
            <StyledInput type="number" defaultValue={1} onChange={this.inputChange}></StyledInput>
        </InputCont>
        <ResultCont>
            <ResultLabel>чел.ч.</ResultLabel>
            <Result>{this.state.resuilt}</Result>
        </ResultCont>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
    margin-top: 40px;
    display: flex;
    flex-direction: row;
`;

const InputCont = styled.div`
    display: flex;
    flex-direction: row;
`;

const InputLabel = styled.div`
    font-size: 14px;
    color: #757575;
`;

const StyledInput = styled.input`
    margin-left: 10px;
`;

const ResultCont = styled.div`
    margin-left 40px;
    display: flex;
    flex-direction: row;
`;

const ResultLabel = styled.div`
    font-size: 14px;
    color: #757575;
`;

const Result = styled.div`
    margin-left: 10px;
`;

export default WorkDetailCalc;
