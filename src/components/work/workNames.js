import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import { NavLink, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import WorkNamesPm from './workNamesPm';

const WorkNamesWithRouter = withRouter(props => <WorkNames {...props}/>);

class WorkNames extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.getData = this.getData.bind(this);
    this.toStore = this.toStore.bind(this);
    this.toNav = this.toNav.bind(this);
  }
  componentDidMount() {
    this.toStore('WORK_NAMES_DATA', 'workNamesData', {});
    this.getData();
  }
  getData() {
    let path = '/api/work/' + this.props.match.params.id;
    fetch(path, {method: 'POST'}).then((res) => {
      if (res.status !== 200) {
        console.log('Err: ' + res.statusText + ' ' + res.status);
        return;
      };
      let contentType = res.headers.get('content-type');
      if (contentType && contentType.includes('application/json')) return res.json();
      throw new TypeError('res is not a JSON');
    })
    .then((json) => {
      console.log(json);
      this.toStore('WORK_NAMES_DATA', 'workNamesData', json);
      this.toNav(); 
    })
    .catch((error) => {console.log(error)});
  }
  toNav() {
    let location = [
      { path: '/work', name: 'Виды работ' },
      { path: this.props.location.pathname, name: this.props.work.workNamesData.name }
    ];
    let controls = {
      addBtn: {
        display: false,
        name: 'Добавить наименование работ +',
        path: '/work/' + this.props.match.params.id + '/add',
      },
      search: {
        display: true,
      }
    };
    this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
    this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
  }
  toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
  render() {
    return (
      <Wrapper>
        {this.props.work.workNamesData && this.props.work.workNamesData.hasOwnProperty('workNames') && this.props.work.workNamesData.workNames.length ? this.props.work.workNamesData.workNames.map((el, i) => {
          return <WorkNamesPm key={i} id={el.id} name={el.name}/>
        }) : <Empty>Пусто</Empty>}
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  width: calc(100% - 8px);
  margin: 4px;
  background-color: #ffffff;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;

const Empty = styled.div`
  padding: 20px;
  color: #757575;
`;

const mapStateToProps = (store) => {
  return {
    work: store.work,
    navigation: store.navigation
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(WorkNamesWithRouter);
