import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import styled from 'styled-components';

class WorkSnowPerimeter extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.getData = this.getData.bind(this);
    this.toStore = this.toStore.bind(this);
    this.toNav = this.toNav.bind(this);
  }
  componentDidMount() {
    //this.toStore('WORK_DETAIL_DATA', 'workDetailData', {});
    //this.getData();
    this.toNav();
  }
  getData() {
    let path = '/api/work/' + this.props.match.params.id + '/' + this.props.match.params.workNameId;
    fetch(path, {method: 'POST'})
    .then(res => {
      if (res.status !== 200) {
          console.log('Err: ' + res.statusText + ' ' + res.status);
          return;
      }
      res.json().then((data) => {
        console.log(data);
        this.toStore('WORK_DETAIL_DATA', 'workDetailData', data);
        this.toNav();
      });
    });
  }
  toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
  toNav() {
    let location = [
      { path: '/work', name: 'Виды работ' },
      { path: '/work/snow', name: 'Снег' },
      { path: '/work/snow/perimeter', name: 'Периметр' }
    ];
    let controls = {
      addBtn: {display: false},
      search: {display: true}
    };
    this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
    this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
  }
  render() {
    return (
      <Wrapper>
            <Data>
                <DataItem>
                  <DataItemName>Наименование работ</DataItemName>
                  <DataItemValue>Периметр</DataItemValue>
                </DataItem>
            </Data>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
    width: calc(100% - 8px);
    padding: 4px;
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
`;
const Data = styled.div`
    width: calc(100% - 20px);
    padding: 10px;
    background-color: #ffffff;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
  `;
const DataItem = styled.div`
    width: 200px;
    margin-bottom: 20px;
`;
const DataItemName = styled.div`
    font-size: 12px;
    color: #757575;
`;
const DataItemValue = styled.div`
    font-size: 14px;
    color: #212121;
`;

const mapStateToProps = (store) => {
  return {
    work: store.work,
    navigation: store.navigation,
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(WorkSnowPerimeter);
