import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

//import WorkNamesPm from './workNamesPm';
//const WorkNamesWithRouter = withRouter(props => <WorkNames {...props}/>);

class WorkSnow extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.getData = this.getData.bind(this);
    this.toStore = this.toStore.bind(this);
    this.toNav = this.toNav.bind(this);
  }
  componentDidMount() {
    //this.toStore('WORK_NAMES_DATA', 'workNamesData', {});
    //this.getData();
    this.toNav(); 
  }
  getData() {
    let path = '/api/work/' + this.props.match.params.id;
    fetch(path, {method: 'POST'}).then((res) => {
      if (res.status !== 200) {
        console.log('Err: ' + res.statusText + ' ' + res.status);
        return;
      }
      let contentType = res.headers.get('content-type');
      if (contentType && contentType.includes('application/json')) return res.json();
      throw new TypeError('res is not a JSON');
    })
    .then((json) => {
      console.log(json);
      this.toStore('WORK_NAMES_DATA', 'workNamesData', json);
      this.toNav(); 
    })
    .catch((error) => {console.log(error)});
  }
  toNav() {
    let location = [
      { path: '/work', name: 'Виды работ' },
      { path: '/work/snow', name: 'Снег' }
    ];
    let controls = {
      addBtn: {display: false},
      search: {display: true}
    };
    this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
    this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
  }
  toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
  render() {
    return (
      <Wrapper>
        <PmCont exact={true} to={{ pathname: '/work/snow/roof' }}>
            <PmTitle>Кровля</PmTitle>
        </PmCont>
        <PmCont exact={true} to={{ pathname: '/work/snow/aprons' }}>
            <PmTitle>Козырьки</PmTitle>
        </PmCont>
        <PmCont exact={true} to={{ pathname: '/work/snow/perimeter' }}>
            <PmTitle>Периметр</PmTitle>
        </PmCont>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
    width: calc(100% - 8px);
    margin: 4px;
    background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
`;
const PmCont = styled(NavLink)`
    width: calc(100% - 40px);
    padding: 20px;
    cursor: pointer;
    text-decoration: none;
    &:hover {
        background-color: #eaeaea;
    }
`;
const PmTitle = styled.div`
    font-size: 14px;
    color: #212121;
`;

const mapStateToProps = (store) => {
  return {
    work: store.work,
    navigation: store.navigation
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(WorkSnow);
