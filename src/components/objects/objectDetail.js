import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import {Redirect, withRouter} from 'react-router-dom';
import styled from 'styled-components';
import DeleteButton from '../buttons/deleteButton';
import EditButton from '../buttons/editButton';

const ObjectDetailWithRouter = withRouter(props => <ObjectDetail {...props}/>);

class ObjectDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {sendResStatus: 'wait', photos: []};
        this.getPhoto = this.getPhoto.bind(this);
        this.getData = this.getData.bind(this);
        this.delete = this.delete.bind(this);
        this.toNav = this.toNav.bind(this);
    }
    componentDidMount() {
        store.dispatch({type: 'OBJECT_DETAIL_DATA', objectDetailData: {}});
        this.getData();
    }
    getPhoto(ids) {
        ids.map(el => fetch('/api/photo/' + el, {method: 'POST'})
                .then(res => res.blob())
                .then(blob => this.setState({photos: [...this.state.photos, (window.URL || window.webkitURL).createObjectURL(blob)]}))
        );
    }
    getData() {
        fetch('/api/objects/' + this.props.match.params.id, {method: 'POST'})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => {
                if (resJson.data.photo.length) this.getPhoto(resJson.data.photo);
                store.dispatch({type: 'OBJECT_DETAIL_DATA', objectDetailData: resJson});
                this.toNav();
            });
    }
    delete(e) {
        e.preventDefault();
        fetch('/api/objects/' + this.props.match.params.id + '/delete', { method: 'POST' })
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => resJson.status === 'ok' ? this.setState({sendResStatus: resJson.status}) : this.setState({sendResStatus: 'err'}));
    }
    toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/objects', name: 'Объекты'}, {path: this.props.location.pathname, name: this.props.objects.objectDetailData.data.address}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: true, name: 'Добавить объект +', path: '/objects/add'}, search: {display: true}}});
    }
    render() {
        return (
            <Wrapper>
                {
                    this.props.objects.objectDetailData.hasOwnProperty('data') ?
                        <Data>
                            <Separator>Основная информация</Separator>
                            <DataItem>
                                <DataItemName>Адрес</DataItemName>
                                <DataItemValue>{this.props.objects.objectDetailData.data.address ? this.props.objects.objectDetailData.data.address : '- - -'}</DataItemValue>
                            </DataItem>
                            <DataItem>
                                <DataItemName>Примечания</DataItemName>
                                <DataItemValue>{this.props.objects.objectDetailData.data.other ? this.props.objects.objectDetailData.data.other : '- - -'}</DataItemValue>
                            </DataItem>

                            {
                                this.state.photos.length ?
                                    <ImgsCont>
                                        {this.state.photos.map((imgEl, imgElI) => <img key={imgElI} style={{width: '100px', marginRight: '10px', cursor: 'pointer'}} src={imgEl} alt='object'/>)}
                                    </ImgsCont>
                                    :
                                    null
                            }

                            <BtnsCont>
                                <EditButton path={this.props.objects.objectDetailData.hasOwnProperty('_id') ? this.props.objects.objectDetailData._id + '/edit' : this.props.location.pathname}/>
                                <DeleteButton callback={this.delete}/>
                            </BtnsCont>
                        </Data>
                    : '- - -'
                }
                {this.state.sendResStatus === 'ok' ? <Redirect to={'/objects'}/> : null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  padding: 4px;
  width: calc(100% - 8px);
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`;
const Data = styled.div`
  padding: 10px;
  width: calc(100% - 20px);
  background-color: #ffffff;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  flex-wrap: wrap;
`;
const DataItem = styled.div`
  width: 200px;
  margin-bottom: 20px;
`;
const DataItemName = styled.div`
  font-size: 12px;
  color: #757575;
`;
const DataItemValue = styled.div`
  font-size: 14px;
  color: #212121;
`;
const Separator = styled.div`
  width: 100%;
  font-size: 16px;
  color: #212121;
  font-weight: 900;
  margin-bottom: 20px;
`;
const ImgsCont = styled.div`
    width: 100%;
`;
const BtnsCont = styled.div`
  width: 100%;
  margin-top: 20px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;

const mapStateToProps = store => ({objects: store.objects, navigation: store.navigation});
export default connect(mapStateToProps, null, null, { pure: false })(ObjectDetailWithRouter);
