import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import { withRouter, Redirect } from 'react-router-dom';
import styled from 'styled-components';
import SaveButton from '../buttons/saveButton';
import DeleteButton from '../buttons/deleteButton';

const ObjectEditWithRouter = withRouter(props => <ObjectEdit {...props}/>);

class ObjectEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sendResStatus: 'wait',
            photos: [],
            address: '',
            other: ''
        };
        this.inputChange = this.inputChange.bind(this);
        this.send = this.send.bind(this);
        ObjectEdit.toNav = ObjectEdit.toNav.bind(this);
        //this.photoInputChange = this.photoInputChange.bind(this);
        //this.photoSend = this.photoSend.bind(this);
    }
    componentDidMount() {this.getData()}
    inputChange(e) {
        let state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
    }
    getData() {
        fetch('/api/objects/' + this.props.match.params.id, {method: 'POST'})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => {
                //if (resJson.data.photo.length) this.getPhoto(resJson.data.photo);
                console.log(resJson);
                store.dispatch({type: 'OBJECT_DETAIL_DATA', objectDetailData: resJson});
                let state = this.state;
                state.address = resJson.data.address;
                state.other = resJson.data.other;
                this.setState(state);
                ObjectEdit.toNav()
            });
    }
    send(e) {
        store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: true});
        e.preventDefault();
        let obj = {address: this.state.address, other: this.state.other};
        fetch('/api/objects/' + this.props.objects.objectDetailData._id + '/update', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(obj)})
            .then(res => res.json())
            .then(resJson => {
                //this.state.photos.length ? this.photoSend(resJson.data._id) : store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false}) && this.setState({sendResStatus: 'ok'})
                console.log(resJson);
                store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false})
                this.setState({sendResStatus: 'ok'});
            });
    }
    delete() {console.log('delete')}
    static toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/objects', name: 'Объекты'}, {path: '/objects/' + this.props.location.pathname.split('/')[2], name: this.props.objects.objectDetailData.data.address}, {path: this.props.location.pathname, name: 'Редактирование'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: false}, search: {display: true}}});
    }
    //photoInputChange(e) {this.setState({photos: e.target.files})}
    /*photoSend(id) {
        let photos = new FormData();
        for (let x = 0; x < this.state.photos.length; x++) photos.append('photo', this.state.photos[x]);
        photos.append('id', id);
        fetch('/api/objects/add/photo', {method: 'POST', body: photos})
            .then(response => response.json())
            .then(json => {
                store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false});
                this.setState({sendResStatus: 'ok'});
            });
    }*/
    render() {
        return (
            <Wrapper>
                <StyledForm onSubmit={this.send}>
                    <InputCont>
                        <InputLabel>Адрес</InputLabel>
                        <StyledInput onChange={this.inputChange} value={this.state.address} required name='address' type='text'/>
                    </InputCont>



                    {/*
                        <InputCont>
                            <InputLabel>Фото</InputLabel>
                            <input onChange={this.photoInputChange} name='photo' type='file' multiple={true} accept='image/*'/>
                        </InputCont>
                    */}



                    <InputCont>
                        <InputLabel>Примечания</InputLabel>
                        <StyledInput onChange={this.inputChange} value={this.state.other} style={{width: '272px'}} name='other' type='text'/>
                    </InputCont>
                    <BtnsCont>
                        <SaveButton/>
                        <DeleteButton callback={this.delete}/>
                    </BtnsCont>
                </StyledForm>
                {this.state.sendResStatus === 'ok' ? <Redirect to={'/objects'}/> : null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    padding: 4px;
    position: relative;
`;
const StyledForm = styled.form`
    padding: 10px;
    background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const BtnsCont = styled.div`
    width: 100%;
    margin: 25px 0;
	display: flex;
	flex-direction: row;
	align-items: center;
`;

const mapStateToProps = store => ({objects: store.objects});
export default connect(mapStateToProps)(ObjectEditWithRouter);
