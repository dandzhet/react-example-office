import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import { withRouter, Redirect } from 'react-router-dom';
import styled from 'styled-components';
import SaveButton from '../buttons/saveButton';

const ObjectAddWithRouter = withRouter(props => <ObjectAdd {...props}/>);

class ObjectAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {sendResStatus: 'wait', photos: []};
        this.photoInputChange = this.photoInputChange.bind(this);
        this.photoSend = this.photoSend.bind(this);
        this.send = this.send.bind(this);
        ObjectAdd.toNav = ObjectAdd.toNav.bind(this);
    }
    componentDidMount() {ObjectAdd.toNav()}
    photoInputChange(e) {this.setState({photos: e.target.files})}
    photoSend(id) {
        let photos = new FormData();
        for (let x = 0; x < this.state.photos.length; x++) photos.append('photo', this.state.photos[x]);
        photos.append('id', id);
        fetch('/api/objects/add/photo', {method: 'POST', body: photos})
            .then(response => response.json())
            .then(json => {
                store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false});
                this.setState({sendResStatus: 'ok'});
            });
    }
    send(e) {
        store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: true});
        e.preventDefault();
        let obj = {};
        for (let i = 0; i < e.target.length - 1; i++) if (e.target[i].name !== 'photo') obj[e.target[i].name] = e.target[i].value;


        // ==================================================
        // ========== SPINNER ===============================
        // ==================================================

        /*store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: true});
        setTimeout(() => {
            this.setState({sendResStatus: 'ok'});
            setTimeout(() => {
                store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false});
            }, 200)
        }, 800)*/

        // ==================================================
        // ==================================================
        // ==================================================


        fetch('/api/objects/add', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(obj)})
            .then(res => res.json())
            .then(resJson => this.state.photos.length ? this.photoSend(resJson.data._id) : store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false}) && this.setState({sendResStatus: 'ok'}));
    }
    static toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/objects', name: 'Объекты'}, {path: '/objects/add', name: 'Новый'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: false}, search: {display: true}}});
    }
    render() {
        return (
            <Wrapper>
                <StyledForm onSubmit={this.send}>
                    <InputCont>
                        <InputLabel>Адрес</InputLabel>
                        <StyledInput required name='address' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Фото</InputLabel>
                        <input onChange={this.photoInputChange} name='photo' type='file' multiple={true} accept='image/*'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Примечания</InputLabel>
                        <StyledInput style={{width: '272px'}} name='other' type='text'/>
                    </InputCont>
                    <Separator><SaveButton/></Separator>
                </StyledForm>
                {this.state.sendResStatus === 'ok' ? <Redirect to={'/objects'}/> : null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    padding: 4px;
    position: relative;
`;
const StyledForm = styled.form`
    padding: 10px;
    background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 16px;
`;

const mapStateToProps = store => ({objects: store.objects});
export default connect(mapStateToProps)(ObjectAddWithRouter);
