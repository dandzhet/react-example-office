import React, {Component} from 'react';
import store from '../../reducers';
import styled from 'styled-components';

class SnowCalc extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'roof', // Тип работ (roof (кровля) || aprons (козырьки) || perimeter (периметр))
            roofCalcData: {
                baseCustomer: '20', // Базовая стоимость для заказчика за м2 (руб)
                basePerformer: '8', // Базовая зарплата для исполнителя за м2 (руб)
                area: '0', // Площадь (м2)
                height: '0', // Высота снежного покрова (см)
                roofType: 'pitched', // Тип кровли (pitched (скатная) || flat (плоская))
                motivation: false, // Мотивационный коэффицент
                night: false, // Ночной коэффицент
                difficult: false, // Коэффицент сложности
            },
            apronsCalcData: {
                area: '0', // Площадь (м2)
                apronsType: 'entry', // Тип козырька (entry (входной) || balcony (балконный))
            },
            perimeterCalcData: {
                baseCustomer: '40', // Базовая стоимость для заказчика за м2 (руб)
                basePerformer: '20', // Базовая зарплата для исполнителя за м2 (руб)
                area: '0', // Площадь (м2)
                distance: '0', // Расстояние от края (м)
            },
            customerCost: '0', // Стоимость для заказчика
            performerPay: '0', // З.п. для исполнителя
        };
        this.dataPreparationForSending = this.dataPreparationForSending.bind(this);
        this.send = this.send.bind(this);
        this.inputChange = this.inputChange.bind(this);
        this.toNav = this.toNav.bind(this);
    }

    dataPreparationForSending() {
        if (this.state.type === 'roof') {
            let calcData = this.state.roofCalcData;
            return {
                type: 'roof', // Тип работ (roof (кровля) || aprons (козырьки) || perimeter (периметр))
                baseCustomer: calcData.baseCustomer ? parseInt(calcData.baseCustomer, 10) : 20, // Базовая стоимость для заказчика за м2 (руб)
                basePerformer: calcData.basePerformer ? parseInt(calcData.basePerformer, 10) : 8, // Базовая зарплата для исполнителя за м2 (руб)
                area: calcData.area ? parseInt(calcData.area, 10) : 0, // Площадь (м2)
                height: calcData.height ? parseInt(calcData.height, 10) : 0, // Высота снежного покрова (см)
                roofType: calcData.roofType || 'pitched', // Тип кровли (pitched (скатная) || flat (плоская))
                motivation: calcData.motivation || false, // Мотивационный коэффицент
                night: calcData.night || false, // Ночной коэффицент
                difficult: calcData.difficult || false, // Коэффицент сложности
            }
        } else if (this.state.type === 'aprons') {
            let calcData = this.state.apronsCalcData;
            return {
                type: 'aprons', // Тип работ (roof (кровля) || aprons (козырьки) || perimeter (периметр))
                area: calcData.area ? parseInt(calcData.area, 10) : 0, // Площадь (м2)
                apronsType: calcData.apronsType || 'entry', // Тип козырька (entry (входной) || balcony (балконный))
            }
        } else if (this.state.type === 'perimeter') {
            let calcData = this.state.perimeterCalcData;
            return {
                type: 'perimeter', // Тип работ (roof (кровля) || aprons (козырьки) || perimeter (периметр))
                baseCustomer: calcData.baseCustomer ? parseInt(calcData.baseCustomer, 10) : 40, // Базовая стоимость для заказчика за м2 (руб)
                basePerformer: calcData.basePerformer ? parseInt(calcData.basePerformer, 10) : 20, // Базовая зарплата для исполнителя за м2 (руб)
                area: calcData.area ? parseInt(calcData.area, 10) : 0, // Площадь (м2)
                distance: calcData.distance ? parseInt(calcData.distance, 10) : 0, // Расстояние от края (м)
            }
        } else return {};
    }

    send() {
        console.log(this.dataPreparationForSending());
        let dataForSend = this.dataPreparationForSending();
        fetch('/api/calc/snow', {
            method: 'POST',
            headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
            body: JSON.stringify(dataForSend)
        })
            .then(res => res.json())
            .then(resJson => {
                console.log(resJson);
                this.setState({customerCost: resJson.data.summ, performerPay: resJson.data.performerSumm})
            });
    }

    inputChange(e, name) {
        let state = this.state;
        if (name === 'type') {
            state.type = e.target.value;
            state.customerCost = '0';
            state.performerPay = '0';
        }
        else {
            if (this.state.type === 'roof') {
                if (e.target.type === 'checkbox') state.roofCalcData[name] = e.target.checked;
                else state.roofCalcData[name] = e.target.value;
            }
            else if (this.state.type === 'aprons') state.apronsCalcData[name] = e.target.value;
            else if (this.state.type === 'perimeter') state.perimeterCalcData[name] = e.target.value;
        }
        this.setState(state);
    }

    componentDidMount() {this.toNav()}

    toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/calc', name: 'Расчет'}, {path: '/calc/snow', name: 'Снег'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: false}, search: {display: true}}})
    }

    render() {
        return (
            <Wrapper>
                <div style={{width: '100%'}}>
                    <InputCont>
                        <InputTitle>Тип работ</InputTitle>
                        <select onChange={e => this.inputChange(e, 'type')} value={this.state.type}>
                            <option value='roof'>Кровля</option>
                            <option value='aprons'>Козырьки</option>
                            <option value='perimeter'>Периметр</option>
                        </select>
                    </InputCont>
                </div>
                {
                    this.state.type === 'roof' ?
                        <div style={{width: '100%'}}>
                            <InputCont>
                                <InputTitle>Базовая стоимость</InputTitle>
                                <input onChange={e => this.inputChange(e, 'baseCustomer')}
                                       value={this.state.roofCalcData.baseCustomer} type="number"/>
                                <InputUnit>руб</InputUnit>
                            </InputCont>
                            <InputCont>
                                <InputTitle>Базовая зп</InputTitle>
                                <input onChange={e => this.inputChange(e, 'basePerformer')}
                                       value={this.state.roofCalcData.basePerformer} type="number"/>
                                <InputUnit>руб</InputUnit>
                            </InputCont>
                            <InputCont>
                                <InputTitle>Площадь</InputTitle>
                                <input onChange={e => this.inputChange(e, 'area')} value={this.state.roofCalcData.area}
                                       type="number"/>
                                <InputUnit>м<sup>2</sup></InputUnit>
                            </InputCont>
                            <InputCont>
                                <InputTitle>Высота снежного покр.</InputTitle>
                                <input onChange={e => this.inputChange(e, 'height')}
                                       value={this.state.roofCalcData.height} type="number"/>
                                <InputUnit>см</InputUnit>
                            </InputCont>
                            <InputCont>
                                <InputTitle>Тип кровли</InputTitle>
                                <select onChange={e => this.inputChange(e, 'roofType')}
                                        value={this.state.roofCalcData.roofType}>
                                    <option value='pitched'>Скатная</option>
                                    <option value='flat'>Плоская</option>
                                </select>
                            </InputCont>
                            <InputCont>
                                <InputTitle>Мотивация</InputTitle>
                                <input onChange={e => this.inputChange(e, 'motivation')}
                                       checked={this.state.roofCalcData.motivation} type="checkbox"/>
                            </InputCont>
                            <InputCont>
                                <InputTitle>Ночь</InputTitle>
                                <input onChange={e => this.inputChange(e, 'night')}
                                       checked={this.state.roofCalcData.night} type="checkbox"/>
                            </InputCont>
                            <InputCont>
                                <InputTitle>Сложная кровля</InputTitle>
                                <input onChange={e => this.inputChange(e, 'difficult')}
                                       checked={this.state.roofCalcData.difficult} type="checkbox"/>
                            </InputCont>
                        </div>
                        : null
                }

                {
                    this.state.type === 'aprons' ?
                        <div style={{width: '100%'}}>
                            <InputCont>
                                <InputTitle>Площадь</InputTitle>
                                <input onChange={e => this.inputChange(e, 'area')}
                                       value={this.state.apronsCalcData.area} type="number"/>
                                <InputUnit>м<sup>2</sup></InputUnit>
                            </InputCont>
                            <InputCont>
                                <InputTitle>Тип козырька</InputTitle>
                                <select onChange={e => this.inputChange(e, 'apronsType')}
                                        value={this.state.apronsCalcData.apronsType}>
                                    <option value='entry'>Входной</option>
                                    <option value='balcony'>Балконный</option>
                                </select>
                            </InputCont>
                        </div>
                        : null
                }

                {
                    this.state.type === 'perimeter' ?
                        <div style={{width: '100%'}}>
                            <InputCont>
                                <InputTitle>Базовая стоимость</InputTitle>
                                <input onChange={e => this.inputChange(e, 'baseCustomer')}
                                       value={this.state.perimeterCalcData.baseCustomer} type="number"/>
                                <InputUnit>руб</InputUnit>
                            </InputCont>
                            <InputCont>
                                <InputTitle>Базовая зп</InputTitle>
                                <input onChange={e => this.inputChange(e, 'basePerformer')}
                                       value={this.state.perimeterCalcData.basePerformer} type="number"/>
                                <InputUnit>руб</InputUnit>
                            </InputCont>
                            <InputCont>
                                <InputTitle>Площадь</InputTitle>
                                <input onChange={e => this.inputChange(e, 'area')}
                                       value={this.state.perimeterCalcData.area} type="number"/>
                                <InputUnit>м<sup>2</sup></InputUnit>
                            </InputCont>
                            <InputCont>
                                <InputTitle>Расстояние от края</InputTitle>
                                <input onChange={e => this.inputChange(e, 'distance')}
                                       value={this.state.perimeterCalcData.distance} type="number"/>
                                <InputUnit>м</InputUnit>
                            </InputCont>
                        </div>
                        : null
                }

                <div style={{width: '100%'}}>
                    <SummBtn onClick={this.send}>Рассчитать</SummBtn>
                </div>
                <div style={{width: '100%'}}>
                    <InputCont>
                        <InputTitle>Сумма для заказчика</InputTitle>
                        <input style={{border: 'none', outline: 'none'}} readOnly={true} value={this.state.customerCost}
                               type="number"/>
                        <InputUnit>руб</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Зп для исполнителя</InputTitle>
                        <input style={{border: 'none', outline: 'none'}} readOnly={true} value={this.state.performerPay}
                               type="number"/>
                        <InputUnit>руб</InputUnit>
                    </InputCont>
                </div>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: calc(100% - 8px);
    margin: 4px;
    background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
`;
const InputCont = styled.div`
    padding: 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
`;
const InputTitle = styled.div`
    width: 150px;
    color: #212121;
    font-size: 12px;
`;
const InputUnit = styled.div`
    margin-left: 5px;
    color: #212121;
    font-size: 12px;
`;
const SummBtn = styled.div`
    margin: 10px;
    padding: 5px
    width: min-content;
    color: #212121;
    font-size: 12px;
    font-weight: 900;
    cursor: pointer;
    border-radius: 50px;
    border: 1px solid;
    &:hover {
        color: #757575;
    }
`;

export default SnowCalc;
