import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../../reducers';
import styled from 'styled-components';

class WellBoringCalc extends Component {
    constructor(props) {
        super(props);
        this.state = {
            calcData: {
                depth: '0', // Глубина (м)
                depthUnitPrice: '1800', // Цена за м бурения
                casing: 'plastic', // Обсадная труба (plastic || metal)
                pumpEquipment: 'none', // Насосное оборудование (Большой список)
                enteringTheHouse: 'none', // Ввод в дом (none || Скважинный адаптер || Кессон (Ж/Б кольца))
                distanceFromTheWellToTheHouse: '0', // Расстояние от скважины до дома (м)
                distanceToCity: '0', // Расстояние до города (км)
                diamondDrillLength: '0', // Длина алмазного сверления (м)
                discount: '0', // Скидка (%)
                explorationDrilling: '0', // Разведывательное бурение (м)
            },
            pumpList: [],
            customerWellSumm: '0', // Стоимость для заказчика (Скважина)
            customerCostPumpEquipment: '0', // Стоимость для заказчика (Насосное оборудование)
            customerEnteringTheHouseSumm: '0', // Стоимость для заказчика (Ввод в дом)
            customerAutomationSumm: '0', // Стоимость для заказчика (Автоматика)
            customerSumm: '0', // СТоимость для заказчика (Все под ключ)
            performerPay: '0', // З.п. для исполнителя
        };
        this.getPumpList = this.getPumpList.bind(this);
        this.dataPreparationForSending = this.dataPreparationForSending.bind(this);
        this.send = this.send.bind(this);
        this.numberInputChange = this.numberInputChange.bind(this);
        this.toStore = this.toStore.bind(this);
        this.toNav = this.toNav.bind(this);
    }
    getPumpList() {
        let path = '/api/calc/wellboring/pumplist';
        fetch(path, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(res => {
            if (res.status !== 200) {
                console.log('Err: ' + res.statusText + ' ' + res.status);
                return;
            }
            res.json().then((data) => {
                console.log(data);
                if (data) {
                    if (data.hasOwnProperty('data')) {
                        let pumpList = data.data;
                        this.setState({pumpList:pumpList});
                    }
                }
            });
        });
    }
    dataPreparationForSending() {
        let calcData = this.state.calcData;
        return {
            depth: calcData.depth ? parseInt(calcData.depth, 10) : 0,
            casing: calcData.casing || 'plastic',
            pumpEquipment: calcData.pumpEquipment || 'none',
            enteringTheHouse: calcData.enteringTheHouse || 'none',
            explorationDrilling: calcData.explorationDrilling || 0,
        };
    }
    send() {
        console.log(this.state.calcData);
        console.log(this.dataPreparationForSending());
        let dataForSend = this.dataPreparationForSending();
        let path = '/api/calc/wellboring';
        fetch(path, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dataForSend)
        }).then(res => {
            if (res.status !== 200) {
                console.log('Err: ' + res.statusText + ' ' + res.status);
                return;
            }
            res.json().then((data) => {
                console.log(data);
                if (data) {
                    if (data.hasOwnProperty('data')) {
                        if (data.data) {
                            this.setState({
                                customerWellSumm: data.data.wellSumm || '0', // Стоимость для заказчика (Скважина)
                                customerCostPumpEquipment: data.data.pumpEquipmentSumm || '0', // Стоимость для заказчика (Насосное оборудование)
                                customerEnteringTheHouseSumm: data.data.enteringTheHouseSumm || '0', // Стоимость для заказчика (Ввод в дом)
                                customerAutomationSumm: data.data.automationSumm || '0', // Стоимость для заказчика (Автоматика)
                                customerSumm: data.data.wellSumm + data.data.pumpEquipmentSumm + data.data.enteringTheHouseSumm + data.data.automationSumm,
                                performerPay: data.data.performerPaySumm || '0', // З.п. для исполнителя
                            });
                        }
                    }
                }
            });
        });
    }
    numberInputChange(e, name) {
        let calcData = this.state.calcData;
        if (name === 'casing') {
            if (e.target.value === 'plastic') calcData.depthUnitPrice = '1800';
            else if (e.target.value === 'metal') calcData.depthUnitPrice = '2000';
        }
        calcData[name] = e.target.value;
        this.setState(calcData);
    }
    componentDidMount() {
        this.toNav();
        this.getPumpList();
    }
    toNav() {
        let location = [
            { path: '/calc', name: 'Расчет' },
            { path: '/calc/wellboring', name: 'Бурение скважин' },
        ];
        let controls = {
            addBtn: {
                display: false,
                name: '',
                path: '',
            },
            search: {
                display: true,
            }
        };
        this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
        this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
    }
    toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
    render() {
        return (
            <Wrapper>
                <div style={{width: '100%'}}>
                    <InputCont>
                        <InputTitle>Глубина</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'depth')} value={this.state.calcData.depth} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Цена за м бурения</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'depthUnitPrice')} value={this.state.calcData.depthUnitPrice} type="number"/>
                        <InputUnit>руб</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Обсадная труба</InputTitle>
                        <select onChange={e => this.numberInputChange(e, 'casing')} value={this.state.calcData.casing}>
                            <option value='plastic'>Пластиковая</option>
                            <option value='metal'>Железная</option>
                        </select>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Насосное оборудование</InputTitle>
                        <select onChange={e => this.numberInputChange(e, 'pumpEquipment')} value={this.state.calcData.pumpEquipment}>
                            <option value='none'>Нет</option>
                            {this.state.pumpList.map((pumpListEl, pumpListI) => <option key={pumpListI} value={pumpListEl}>{pumpListEl}</option>)}
                        </select>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Ввод в дом</InputTitle>
                        <select onChange={e => this.numberInputChange(e, 'enteringTheHouse')} value={this.state.calcData.enteringTheHouse}>
                            <option value='none'>Нет</option>
                            <option value='Скважинный адаптер'>Скважинный адаптер</option>
                            <option value='Кессон (Ж/Б кольца)'>Кессон (Ж/Б кольца)</option>
                        </select>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Расстояние до дома</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'distanceFromTheWellToTheHouse')} value={this.state.calcData.distanceFromTheWellToTheHouse} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Расстояние до города</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'distanceToCity')} value={this.state.calcData.distanceToCity} type="number"/>
                        <InputUnit>км</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Алмазное сверление</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'diamondDrillLength')} value={this.state.calcData.diamondDrillLength} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Развед. бурение</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'explorationDrilling')} value={this.state.calcData.explorationDrilling} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Скидка</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'discount')} value={this.state.calcData.discount} type="number"/>
                        <InputUnit>%</InputUnit>
                    </InputCont>
                </div>
                <div style={{width: '100%'}}>
                    <SummBtn onClick={this.send}>Рассчитать</SummBtn>
                </div>
                <div style={{width: '100%'}}>
                    <InputCont>
                        <InputTitle>Скважина</InputTitle>
                        <input style={{border: 'none', outline: 'none'}} readOnly={true} value={this.state.customerWellSumm} type="number"/>
                        <InputUnit>руб</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Насосное оборудование</InputTitle>
                        <input style={{border: 'none', outline: 'none'}} readOnly={true} value={this.state.customerCostPumpEquipment} type="number"/>
                        <InputUnit>руб</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Ввод в дом</InputTitle>
                        <input style={{border: 'none', outline: 'none'}} readOnly={true} value={this.state.customerEnteringTheHouseSumm} type="number"/>
                        <InputUnit>руб</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Автоматика</InputTitle>
                        <input style={{border: 'none', outline: 'none'}} readOnly={true} value={this.state.customerAutomationSumm} type="number"/>
                        <InputUnit>руб</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Все под ключ</InputTitle>
                        <input style={{border: 'none', outline: 'none'}} readOnly={true} value={this.state.customerSumm} type="number"/>
                        <InputUnit>руб</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Зп для исполнителя</InputTitle>
                        <input style={{border: 'none', outline: 'none'}} readOnly={true} value={this.state.performerPay} type="number"/>
                        <InputUnit>руб</InputUnit>
                    </InputCont>
                </div>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: calc(100% - 8px);
    margin: 4px;
    background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
`;
const InputCont = styled.div`
    padding: 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
`;
const InputTitle = styled.div`
    width: 150px;
    color: #212121;
    font-size: 12px;
`;
const InputUnit = styled.div`
    margin-left: 5px;
    color: #212121;
    font-size: 12px;
`;
const SummBtn = styled.div`
    margin: 10px;
    padding: 5px
    width: min-content;
    color: #212121;
    font-size: 12px;
    font-weight: 900;
    cursor: pointer;
    border-radius: 50px;
    border: 1px solid;
    &:hover {
        color: #757575;
    }
`;
const mapStateToProps = (store) => {
    return {
        work: store.work
    };
};

export default connect(mapStateToProps)(WellBoringCalc);
