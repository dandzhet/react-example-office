import React, {Component} from 'react';
import store from '../../reducers';
import styled from 'styled-components';

class CalcStretchCeiling extends Component {
    constructor(props) {
        super(props);
        this.state = {
            calcData: {
                area: '0',  // площадь (м2)
                perimeter: '0', // периметр (м)
                color: '303', // цвет полотна (Lackfolie color code)
                texture: 'm', // фактура полотна (m (мат) || l (глянец) || s (сатин))
                photoPrint: '0', // площадь фотопечати (м2)
                profile: 'default', // профиль (default || cant || kraab)
                corners: '4', // углы (шт)
                lamp: '0', // светильники (шт)
                chandelier: '0', // люстры (шт)
                pipeBypass: '0', // обводы труб (шт)
                pipeBypassType: 'outside', // тип обводов труб (outside || inside)
                curtain: '0', // гардина (м)
                curtainRodEasy: '0', // карниз (простое прерывание)
                curtainRodHard: '0', // карниз (сложное прерывание)
                curvedSections: '0', // изготовление криволинейных участков (м)
                solderingCloths: '0', // спайка полотен (м)
                tile: '0', // монтаж по кафелю (м)
                soaringCeiling: '0', // парящий потолок (м)
                soaringCeilingLedStripLight: '0', // светодиодная лента для парящего потолка (м)
                secondLevel: '0', // второй уровень (м)
            },
            customerCost: '0', // Стоимость для заказчика
            performerPay: '0', // З.п. для исполнителя
        };
        this.dataPreparationForSending = this.dataPreparationForSending.bind(this);
        this.send = this.send.bind(this);

        this.numberInputChange = this.numberInputChange.bind(this);

        this.toStore = this.toStore.bind(this);
        this.toNav = this.toNav.bind(this);
    }
    dataPreparationForSending() {
        let calcData = this.state.calcData;
        return {
            area: calcData.area ? parseInt(calcData.area, 10) : 0,  // площадь (м2)
            perimeter: calcData.perimeter ? parseInt(calcData.perimeter, 10) : 0, // периметр (м)
            color: calcData.color ? parseInt(calcData.color, 10) : 303, // цвет полотна (Lackfolie color code)
            texture: calcData.texture ? calcData.texture : 'm', // фактура полотна (m (мат) || l (глянец) || s (сатин))
            photoPrint: calcData.photoPrint ? parseInt(calcData.photoPrint, 10) : 0, // площадь фотопечати (м2)
            profile: calcData.profile ? calcData.profile : 'default', // профиль (default || cant || kraab)
            corners: calcData.corners ? parseInt(calcData.corners, 10) : 4, // углы (шт)
            lamp: calcData.lamp ? parseInt(calcData.lamp, 10) : 0, // освещение (шт)
            chandelier: calcData.chandelier ? parseInt(calcData.chandelier, 10) : 0, // освещение (шт)
            pipeBypass:  calcData.pipeBypass ? calcData.pipeBypass : '0', // обвод трубы (шт)
            pipeBypassType:  calcData.pipeBypassType ? calcData.pipeBypassType : 'outside', // тип обвод трубы (outside || inside)
            curtain: calcData.curtain ? parseInt(calcData.curtain, 10) : 0, // гардина (м)
            curtainRodEasy: calcData.curtainRodEasy ? parseInt(calcData.curtainRodEasy, 10) : 0, // карниз (простое прерывание)
            curtainRodHard: calcData.curtainRodHard ? parseInt(calcData.curtainRodHard, 10) : 0, // карниз (сложное прерывание)
            curvedSections: calcData.curvedSections ? parseInt(calcData.curvedSections, 10) : 0, // изготовление криволинейных участков (м)
            solderingCloths: calcData.solderingCloths ? parseInt(calcData.solderingCloths, 10) : 0, // спайка полотен (м)
            tile: calcData.tile ? parseInt(calcData.tile, 10) : 0, // монтаж по кафелю (м)
            soaringCeiling: calcData.soaringCeiling ? parseInt(calcData.soaringCeiling, 10) : 0, // парящий потолок (м)
            soaringCeilingLedStripLight: calcData.soaringCeilingLedStripLight ? parseInt(calcData.soaringCeilingLedStripLight, 10) : 0, // светодиодная лента для парящего потолка (м)
            secondLevel: calcData.secondLevel ? parseInt(calcData.secondLevel, 10) : 0, // второй уровень (м)
        };
    }
    send() {
        console.log(this.state.calcData);
        console.log(this.dataPreparationForSending());
        let dataForSend = this.dataPreparationForSending();
        let path = '/api/calc/stretchceiling';
        fetch(path, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dataForSend)
        }).then(res => {
            if (res.status !== 200) {
                console.log('Err: ' + res.statusText + ' ' + res.status);
                return;
            }
            res.json().then((data) => {
                console.log(data);
                if (data) {
                    if (data.hasOwnProperty('data')) {
                        if (data.data.hasOwnProperty('customerCost')) {
                            this.setState({customerCost:data.data.customerCost});
                        }
                    }
                }
            });
        });
    }
    numberInputChange(e, name) {
        let calcData = this.state.calcData;
        calcData[name] = e.target.value;
        this.setState(calcData);
    }
    componentDidMount() {this.toNav()}
    toNav() {
        let location = [
            { path: '/calc', name: 'Расчет' },
            { path: '/calc/stretchceiling', name: 'Натяжные потолки' },
        ];
        let controls = {
            addBtn: {
                display: false,
                name: '',
                path: '',
            },
            search: {
                display: true,
            }
        };
        this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
        this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
    }
    toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
    render() {
        return (
            <Wrapper>
                <div style={{width: '100%'}}>
                    <InputCont>
                        <InputTitle>Площадь</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'area')} value={this.state.calcData.area} type="number"/>
                        <InputUnit>м<sup>2</sup></InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Периметр</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'perimeter')} value={this.state.calcData.perimeter} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Цвет</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'color')} value={this.state.calcData.color} type="number"/>
                        <InputUnit>Lackfolie color code</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Фактура</InputTitle>
                        <select onChange={e => this.numberInputChange(e, 'texture')} value={this.state.calcData.texture}>
                            <option value='m'>Мат</option>
                            <option value='l'>Глянец</option>
                            <option value='s'>Сатин</option>
                        </select>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Фотопечать</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'photoPrint')} value={this.state.calcData.photoPrint} type="number"/>
                        <InputUnit>м<sup>2</sup></InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Профиль</InputTitle>
                        <select onChange={e => this.numberInputChange(e, 'profile')} value={this.state.calcData.profile}>
                            <option value='default'>Обычный</option>
                            <option value='cant'>Кант</option>
                            <option value='kraab'>Kraab</option>
                        </select>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Углы</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'corners')} value={this.state.calcData.corners} type="number"/>
                        <InputUnit>шт</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Светильники</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'lamp')} value={this.state.calcData.lamp} type="number"/>
                        <InputUnit>шт</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Люстры</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'chandelier')} value={this.state.calcData.chandelier} type="number"/>
                        <InputUnit>шт</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Обводы труб</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'pipeBypass')} value={this.state.calcData.pipeBypass} type="number"/>
                        <InputUnit>шт</InputUnit>
                        <InputTitle style={{marginLeft: '20px'}}>Тип</InputTitle>
                        <select onChange={e => this.numberInputChange(e, 'pipeBypassType')} value={this.state.calcData.pipeBypassType}>
                            <option value='outside'>Наружный</option>
                            <option value='inside'>Внутренний</option>
                        </select>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Монтаж по кафелю</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'tile')} value={this.state.calcData.tile} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Гардина</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'curtain')} value={this.state.calcData.curtain} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Карниз (простое прер.)</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'curtainRodEasy')} value={this.state.calcData.curtainRodEasy} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Карниз (сложное прер.)</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'curtainRodHard')} value={this.state.calcData.curtainRodHard} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Криволинейные участки</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'curvedSections')} value={this.state.calcData.curvedSections} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Спайка полотен</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'solderingCloths')} value={this.state.calcData.solderingCloths} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Парящий потолок</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'soaringCeiling')} value={this.state.calcData.soaringCeiling} type="number"/>
                        <InputUnit>м</InputUnit>
                        <InputTitle style={{marginLeft: '20px'}}>Светодиодная лента</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'soaringCeilingLedStripLight')} value={this.state.calcData.soaringCeilingLedStripLight} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Второй уровень</InputTitle>
                        <input onChange={e => this.numberInputChange(e, 'secondLevel')} value={this.state.calcData.secondLevel} type="number"/>
                        <InputUnit>м</InputUnit>
                    </InputCont>
                </div>
                <div style={{width: '100%'}}>
                    <SummBtn onClick={this.send}>Рассчитать</SummBtn>
                </div>
                <div style={{width: '100%'}}>
                    <InputCont>
                        <InputTitle>Сумма для заказчика</InputTitle>
                        <input style={{border: 'none', outline: 'none'}} readOnly={true} value={this.state.customerCost} type="number"/>
                        <InputUnit>руб</InputUnit>
                    </InputCont>
                    <InputCont>
                        <InputTitle>Зп для исполнителя</InputTitle>
                        <input style={{border: 'none', outline: 'none'}} readOnly={true} value={this.state.performerPay} type="number"/>
                        <InputUnit>руб</InputUnit>
                    </InputCont>
                </div>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  width: calc(100% - 8px);
  margin: 4px;
  background-color: #ffffff;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;
const InputCont = styled.div`
    padding: 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
`;
const InputTitle = styled.div`
    width: 150px;
    color: #212121;
    font-size: 12px;
`;
const InputUnit = styled.div`
    margin-left: 5px;
    color: #212121;
    font-size: 12px;
`;
const SummBtn = styled.div`
    margin: 10px;
    padding: 5px
    width: min-content;
    color: #212121;
    font-size: 12px;
    font-weight: 900;
    cursor: pointer;
    border-radius: 50px;
    border: 1px solid;
    &:hover {
        color: #757575;
    }
`;

export default CalcStretchCeiling;
