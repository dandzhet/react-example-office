import React, { Component } from 'react';
import { connect } from "react-redux";
//import store from './../reducers';
import { NavLink, withRouter } from 'react-router-dom'
import styled from 'styled-components';

const TopbarNaigationWithRouter = withRouter(props => <TopbarNaigation {...props}/>);

class TopbarNaigation extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {

  }
  render() {
    return (
      <Wrapper>
        {this.props.navigation.navigationPaths.length ? 
          this.props.navigation.navigationPaths.map((el, i) => {
            return (
              <LinkCont key={el.path}>
                <StyledLink exact={true} to={{ pathname: el.path }}>{el.name}</StyledLink>
                {i !== this.props.navigation.navigationPaths.length - 1 ? <i className="fas fa-angle-right" style={{padding: '0 10px', fontSize: '10px', color: '#757575'}}></i> : null}
              </LinkCont>
            )
          }) 
        : null}
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  color: #212121;
  padding-left: 10px;
  display: flex;
  flex-direction: row;
`;

const LinkCont = styled.div`
  display: flex;
  flex-direction: row;
`;

const StyledLink = styled(NavLink)`
  cursor: pointer;
  text-decoration: none;
  color: #424242;
  font-weight: 900;
  font-size: 10px;
  &:hover {
      color: #757575;
  }
`;

const mapStateToProps = (store) => {
  return {
    navigation: store.navigation
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(TopbarNaigationWithRouter);
