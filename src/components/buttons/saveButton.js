import React, { Component } from 'react';
import styled from 'styled-components';

class SaveButton extends Component {
    render() {
        return (
            <SaveBtn style={{/*marginTop: '40px'*/ marginRight: '10px'}} type='submit'>
                <Text>
                    Сохранить
                </Text>
                <i style={{fontSize: '16px'}} className="fas fa-save"/>
            </SaveBtn>
        );
    }
}

const SaveBtn = styled.button`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    background: #424242;
    padding: 10px;
    cursor: pointer;
    border: none;
    outline: none;
    border-radius: 50px;
    color: #ffffff;
    transition: .2s;
    font-size: 12px;
    &:hover {
        opacity: 0.8;
    }
`;
const Text = styled.div`
    margin-right: 10px;
`;

export default SaveButton;