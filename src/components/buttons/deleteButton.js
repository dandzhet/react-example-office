import React, { Component } from 'react';
import styled from 'styled-components';

class DeleteButton extends Component {
    render() {
        return (
            <DelBtn style={{/*marginTop: '40px'*/}} onClick={this.props.callback}>
                <Text>Удалить</Text>
                <i className="far fa-trash-alt" style={{fontSize: '14px'}}/>
            </DelBtn>
        );
    }
}

const DelBtn = styled.button`
    text-decoration: none;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    padding: 10px;
    cursor: pointer;
    outline: none;
    border: 1px solid #212121;
    border-radius: 50px;
    color: #212121;
    transition: .2s;
    font-size: 12px;
    &:hover {
        opacity: 0.4;
    }
`;
const Text = styled.div`
    margin-right: 10px;
`;

export default DeleteButton;