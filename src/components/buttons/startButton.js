import React, { Component } from 'react';
import styled from 'styled-components';

class StartButton extends Component {
    render() {
        return (
            <StartBtn style={{marginTop: '40px'}} onClick={this.props.callback}>
                <Text>
                    Приступить
                </Text>
                 <i style={{fontSize: '16px'}} className="far fa-play-circle"/>
            </StartBtn>
        );
    }
}

const StartBtn = styled.button`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    background: #7bbb6a;
    padding: 10px;
    cursor: pointer;
    border: none;
    outline: none;
    border-radius: 50px;
    color: #ffffff;
    transition: .2s;
    font-size: 12px;
    &:hover {
        opacity: 0.8;
    }
`;
const Text = styled.div`
    margin-right: 10px;
`;

export default StartButton;