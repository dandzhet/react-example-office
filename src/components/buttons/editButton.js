import React, { Component } from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

class EditButton extends Component {
    render() {
        return (
            <EditBtn style={{/*marginTop: '40px', */
                marginRight: '10px'}} exact={true} to={{ pathname: this.props.path }}>
                <EditBtnText>Редактировать</EditBtnText>
                <i className="fas fa-pen" style={{fontSize: '14px'}}/>
            </EditBtn>
        );
    }
}

const EditBtn = styled(NavLink)`
    text-decoration: none;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    background: #424242;
    padding: 10px;
    cursor: pointer;
    border: none;
    outline: none;
    border-radius: 50px;
    color: #ffffff;
    transition: .2s;
    font-size: 12px;
    &:hover {
        opacity: 0.8;
    }
`;
const EditBtnText = styled.div`
    margin-right: 10px;
`;

export default EditButton;