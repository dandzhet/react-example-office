import React, {Component} from 'react';
import {connect} from "react-redux";
import store from './../reducers';
import styled from 'styled-components';
import CustomersPm from './customers/customersPm';

class Customers extends Component {
    constructor(props) {
        super(props);
        this.getData = this.getData.bind(this);
        Customers.toNav = Customers.toNav.bind(this);
    }
    componentDidMount() {
        store.dispatch({type: 'CUSTOMERS_DATA', customersData: {}});
        Customers.toNav();
        this.getData();
    }
    getData() {
        fetch('/api/customers', {method: 'POST'})
            .then(res => res.json())
            .then(resJson => store.dispatch({type: 'CUSTOMERS_DATA', customersData: resJson}));
    }
    static toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/customers', name: 'Заказчики'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: true, name: 'Добавить заказчика +', path: '/customers/add'}, search: {display: true}}});
    }
    render() {
        return (
            <Wrapper>
                {this.props.customers.customersData.hasOwnProperty('data') && this.props.customers.customersData.data.length ? this.props.customers.customersData.data.map((el, i) => <CustomersPm key={i} id={el._id} name={el.data.name}/>) : <Empty>Пусто</Empty>}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  width: calc(100% - 8px);
  margin: 4px;
  background-color: #ffffff;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;
const Empty = styled.div`
  padding: 20px;
  color: #757575;
`;

const mapStateToProps = store => ({customers: store.customers});
export default connect(mapStateToProps)(Customers);
