import React, {Component} from 'react';
import {connect} from "react-redux";
import store from './../reducers';
import styled from 'styled-components';

class Feed extends Component {
    constructor(props) {
        super(props);
        Feed.toNav = Feed.toNav.bind(this);
    }
    componentDidMount() {Feed.toNav()}
    static toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/', name: 'Оповещения'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: false, name: '', path: ''}, search: {display: true}}});
    }
    render() {return <Wrapper>- - -</Wrapper>}
}

const Wrapper = styled.div`
    width: calc(100% - 8px);
    margin: 4px;
    background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
`;

const mapStateToProps = store => ({/*list: store.list,*/});
export default connect(mapStateToProps, null, null, {pure: false})(Feed);