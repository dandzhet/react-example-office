import React, { Component } from 'react';
import { connect } from "react-redux";
import store from './../reducers';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

import WorkPm from './work/workPm';

class Work extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.getData = this.getData.bind(this);
    this.toStore = this.toStore.bind(this);
    this.toNav = this.toNav.bind(this);
  }
  componentDidMount() {
    this.toStore('WORK_DATA', 'workData', {});
    this.getData();
    this.toNav();
  }
  getData() {
    fetch('/api/work', {method: 'POST'})
    .then(res => {
      if (res.status !== 200) {
          console.log('Err: ' + res.statusText + ' ' + res.status);
          return;
      };
      res.json().then((data) => {
        console.log(data);
        this.toStore('WORK_DATA', 'workData', data);
      });
    });
  }
  toNav() {
    let location = [
      { path: '/work', name: 'Виды работ' },
    ];
    let controls = {
      addBtn: {
        display: false,
        name: 'Добавить вид работ +',
        path: '/work/add',
      },
      search: {
        display: true,
      }
    };
    this.toStore('NAVIGATION_PATHS', 'navigationPaths', location);
    this.toStore('TOPBAR_CONTROLS', 'topbarControls', controls);
  }
  toStore(type, action, data) { store.dispatch({ type: type, [action]: data }) }
  render() {
    return (
      <Wrapper>
        {this.props.work.workData && this.props.work.workData.hasOwnProperty('data') && this.props.work.workData.data.length ? this.props.work.workData.data.map((el) => {
            return <WorkPm key={el._id} id={el._id} name={el.name}/>
        }) : <Empty>Пусто</Empty>}
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  width: calc(100% - 8px);
  margin: 4px;
  background-color: #ffffff;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;

const Empty = styled.div`
  padding: 20px;
  color: #757575;
`;

const mapStateToProps = (store) => {
  return {
    work: store.work
  };
};

export default connect(mapStateToProps)(Work);
