import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../../reducers';
import {withRouter, Redirect} from 'react-router-dom';
import styled from 'styled-components';
import DeleteButton from '../buttons/deleteButton';
import SaveButton from '../buttons/saveButton';

const CustomersEditWithRouter = withRouter(props => <CustomerEdit {...props}/>);

class CustomerEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sendResStatus: 'wait',
            data: {
                name: '',
                fullName: '',
                urAddress: '',
                factAddress: '',
                site: '',
                ogrn: '',
                inn: '',
                kpp: '',
                okpo: '',
                rs: '',
                bank: '',
                ks: '',
                bik: '',
                directorName: '',
                
                /*contactName: '',
                contactPhone: '',
                contactMail: '',
                contactPost: '',*/
                
                contact: {
                    name: '',
                    phone: '',
                    mail: '',
                    post: ''
                },
                
                other: ''
            }
        };
        this.inputChange = this.inputChange.bind(this);
        this.getData = this.getData.bind(this);
        this.send = this.send.bind(this);
        this.delete = this.delete.bind(this);
        this.toNav = this.toNav.bind(this);
    }

    componentDidMount() {
        store.dispatch({type: 'CUSTOMERS_DETAIL_DATA', customersDetailData: {}});
        this.getData();
    }

    inputChange(e) {
        let data = this.state.data;
        switch (e.target.name) {
            case 'contactName':
                data.contact.name = e.target.value;
                break;
            case 'contactPhone':
                data.contact.phone = e.target.value;
                break;
            case 'contactMail':
                data.contact.mail = e.target.value;
                break;
            case 'contactPost':
                data.contact.post = e.target.value;
                break;
            default:
                data[e.target.name] = e.target.value;
        }
        this.setState({data: data});
    }

    getData() {
        fetch('/api/customers/' + this.props.match.params.id, {method: 'POST'})
            .then(res => res.json())
            .then(resJson => {
                console.log(resJson);
                store.dispatch({type: 'CUSTOMERS_DETAIL_DATA', customersDetailData: resJson});
                this.toNav();
                let stateData = {
                    name: resJson.data.hasOwnProperty('name') ? resJson.data.name : '',
                    fullName: resJson.data.hasOwnProperty('fullName') ? resJson.data.fullName : '',
                    urAddress: resJson.data.hasOwnProperty('urAddress') ? resJson.data.urAddress : '',
                    factAddress: resJson.data.hasOwnProperty('factAddress') ? resJson.data.factAddress : '',
                    site: resJson.data.hasOwnProperty('site') ? resJson.data.site : '',
                    ogrn: resJson.data.hasOwnProperty('ogrn') ? resJson.data.ogrn : '',
                    inn: resJson.data.hasOwnProperty('inn') ? resJson.data.inn : '',
                    kpp: resJson.data.hasOwnProperty('kpp') ? resJson.data.kpp : '',
                    okpo: resJson.data.hasOwnProperty('okpo') ? resJson.data.okpo : '',
                    rs: resJson.data.hasOwnProperty('rs') ? resJson.data.rs : '',
                    bank: resJson.data.hasOwnProperty('bank') ? resJson.data.bank : '',
                    ks: resJson.data.hasOwnProperty('ks') ? resJson.data.ks : '',
                    bik: resJson.data.hasOwnProperty('bik') ? resJson.data.bik : '',
                    directorName: resJson.data.hasOwnProperty('directorName') ? resJson.data.directorName : '',
                    other: resJson.data.hasOwnProperty('other') ? resJson.data.other : '',

                    /*contactName: resJson.data.hasOwnProperty('contact') && resJson.data.contact.hasOwnProperty('name') ? resJson.data.contact.name : '',
                    contactPhone: resJson.data.hasOwnProperty('contact') && resJson.data.contact.hasOwnProperty('phone') ? resJson.data.contact.phone : '',
                    contactMail: resJson.data.hasOwnProperty('contact') && resJson.data.contact.hasOwnProperty('mail') ? resJson.data.contact.mail : '',
                    contactPost: resJson.data.hasOwnProperty('contact') && resJson.data.contact.hasOwnProperty('post') ? resJson.data.contact.post : '',*/
                };

                stateData.contact = resJson.data.hasOwnProperty('contact') ?
                    {
                        name: resJson.data.contact.hasOwnProperty('name') ? resJson.data.contact.name : '',
                        phone: resJson.data.contact.hasOwnProperty('phone') ? resJson.data.contact.phone : '',
                        mail: resJson.data.contact.hasOwnProperty('mail') ? resJson.data.contact.mail : '',
                        post: resJson.data.contact.hasOwnProperty('post') ? resJson.data.contact.post : ''
                    }
                    :
                    {
                        name: '',
                        phone: '',
                        mail: '',
                        post: ''
                    };
                this.setState({data: stateData});
            });
    }

    send(e) {
        e.preventDefault();
        /*let obj = {};
        for (let i = 0; i < e.target.length - 1; i++) obj[e.target[i].name] = e.target[i].value;
        console.log(obj);*/
        let obj = this.state.data;
        console.log(obj);
        fetch('/api/customers/' + this.props.match.params.id + '/update', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(obj)})
            .then(res => res.json())
            .then(resJson => this.setState({sendResStatus: resJson.status}));
    }

    delete(e) {
        e.preventDefault();
        fetch('/api/customers/' + this.props.match.params.id + '/delete', {method: 'POST'})
            .then(res => res.json())
            .then(resJson => this.setState({sendResStatus: resJson.status}));
    }

    toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/customers', name: 'Заказчики'}, {path: '/customers/' + this.props.location.pathname.split('/')[2], name: this.props.customers.customersDetailData.data.name}, {path: this.props.location.pathname, name: 'Редактирование'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: true, name: 'Добавить заказчика +', path: '/customers/add'}, search: {display: true}}});
    }

    render() {
        return (
            <Wrapper>
                <StyledForm onSubmit={this.send}>
                    <Separator>Основная информация</Separator>
                    <InputCont>
                        <InputLabel>Название</InputLabel>
                        <StyledInput value={this.state.data.name} onChange={this.inputChange} required name='name'
                                     type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Полное название</InputLabel>
                        <StyledInput value={this.state.data.fullName} onChange={this.inputChange} name='fullName'
                                     type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Юр. адрес</InputLabel>
                        <StyledInput value={this.state.data.urAddress} onChange={this.inputChange} name='urAddress'
                                     type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Фактич. адрес</InputLabel>
                        <StyledInput value={this.state.data.factAddress} onChange={this.inputChange} name='factAddress'
                                     type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Сайт</InputLabel>
                        <StyledInput value={this.state.data.site} onChange={this.inputChange} name='site' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>ОГРН</InputLabel>
                        <StyledInput value={this.state.data.ogrn} onChange={this.inputChange} name='ogrn' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>ИНН</InputLabel>
                        <StyledInput value={this.state.data.inn} onChange={this.inputChange} name='inn' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>КПП</InputLabel>
                        <StyledInput value={this.state.data.kpp} onChange={this.inputChange} name='kpp' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>ОКПО</InputLabel>
                        <StyledInput value={this.state.data.okpo} onChange={this.inputChange} name='okpo' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Р/с</InputLabel>
                        <StyledInput value={this.state.data.rs} onChange={this.inputChange} name='rs' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Банк</InputLabel>
                        <StyledInput value={this.state.data.bank} onChange={this.inputChange} name='bank' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>К/с</InputLabel>
                        <StyledInput value={this.state.data.ks} onChange={this.inputChange} name='ks' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>БИК</InputLabel>
                        <StyledInput value={this.state.data.bik} onChange={this.inputChange} name='bik' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>ФИО директора</InputLabel>
                        <StyledInput value={this.state.data.directorName} onChange={this.inputChange} name='directorName' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Примечания</InputLabel>
                        <StyledInput value={this.state.data.other} onChange={this.inputChange} style={{width: '270px'}} name='other'/>
                    </InputCont>
                    
                    
                    <Separator>Контактное лицо</Separator>
                    
                    
                    {/*
                        <InputCont>
                            <InputLabel>ФИО</InputLabel>
                            <StyledInput value={this.state.data.contactName} onChange={this.inputChange} name='contactName' type='text'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Телефон</InputLabel>
                            <StyledInput value={this.state.data.contactPhone} onChange={this.inputChange} name='contactPhone' type='tel'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Почта</InputLabel>
                            <StyledInput value={this.state.data.contactMail} onChange={this.inputChange} name='contactMail' type='email'/>
                        </InputCont>
                        <InputCont>
                            <InputLabel>Должность</InputLabel>
                            <StyledInput value={this.state.data.contactPost} onChange={this.inputChange} name='contactPost' type='text'/>
                        </InputCont>
                    */}
                    <InputCont>
                        <InputLabel>ФИО</InputLabel>
                        <StyledInput value={this.state.data.contact.name} onChange={this.inputChange} name='contactName' type='text'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Телефон</InputLabel>
                        <StyledInput value={this.state.data.contact.phone} onChange={this.inputChange} name='contactPhone' type='tel'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Почта</InputLabel>
                        <StyledInput value={this.state.data.contact.mail} onChange={this.inputChange} name='contactMail' type='email'/>
                    </InputCont>
                    <InputCont>
                        <InputLabel>Должность</InputLabel>
                        <StyledInput value={this.state.data.contact.post} onChange={this.inputChange} name='contactPost' type='text'/>
                    </InputCont>


                    <BtnsCont>
                        <SaveButton/>
                        <DeleteButton callback={this.delete}/>
                    </BtnsCont>
                </StyledForm>
                {this.state.sendResStatus === 'ok' ? <Redirect to={'/customers'}/> : null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    padding: 4px;
    position: relative;
`;
const StyledForm = styled.form`
    padding: 10px;
    background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 16px;
`;
const BtnsCont = styled.div`
    width: 100%;
    margin-top: 25px;
    margin-bottom: 25px;
	display: flex;
	flex-direction: row;
	align-items: center;
`;

const mapStateToProps = store => ({customers: store.customers, dialog: store.dialog});
export default connect(mapStateToProps, null, null, {pure: false})(CustomersEditWithRouter);
