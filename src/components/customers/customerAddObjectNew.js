import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import styled from 'styled-components';

class CustomerAddObjectNew extends Component {
    constructor(props) {
        super(props);
        this.inputChange = this.inputChange.bind(this);
    }
    inputChange(e) {
        let storeObject = this.props.customers.customersAddObject;
        if (e.target.name !== 'photos') storeObject.new[e.target.name] = e.target.value;
        else storeObject.new[e.target.name] = e.target.files;
        store.dispatch({type: 'CUSTOMERS_ADD_OBJECT', customersAddObject: storeObject});
    }
    render() {
        return (
            <Wrapper>
                <InputCont>
                    <InputLabel>Адрес</InputLabel>
                    <StyledInput required name='address' type='text' onChange={this.inputChange}/>
                </InputCont>
                <InputCont>
                    <InputLabel>Фото</InputLabel>
                    <input name='photos' type='file' multiple={true} accept='image/*' onChange={this.inputChange}/>
                </InputCont>
                <InputCont>
                    <InputLabel>Примечания</InputLabel>
                    <StyledInput style={{width: '272px'}} name='other' type='text' onChange={this.inputChange}/>
                </InputCont>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;

const mapStateToProps = store => ({customers: store.customers});
export default connect(mapStateToProps)(CustomerAddObjectNew);
