import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import styled from 'styled-components';

class CustomerAddObjectDb extends Component {
    constructor(props) {
        super(props);
        this.state = {objects: [], selected: 'none'};
        this.selectChange = this.selectChange.bind(this);
    }
    componentDidMount() {
        fetch('/api/objects/', {method: 'POST'})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => {
                console.log(resJson);
                this.setState({objects: resJson.data})
            });
    }
    selectChange(e) {
        let storeObject = this.props.customers.customersAddObject;
        storeObject.db = e.target.value;
        store.dispatch({type: 'CUSTOMERS_ADD_OBJECT', customersAddObject: storeObject});
    }
    render() {
        return (
            <Wrapper>
                <select onChange={this.selectChange} style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}} name='object'>
                    <option value='none'>- - -</option>
                    {this.state.objects.map((el, i) => <option value={el._id} key={i}>{el.data.address}</option>)}
                </select>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;

const mapStateToProps = store => ({customers: store.customers});
export default connect(mapStateToProps)(CustomerAddObjectDb);
