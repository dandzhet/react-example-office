import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import { withRouter, Redirect } from 'react-router-dom';
import styled from 'styled-components';

import DeleteButton from '../buttons/deleteButton';
import EditButton from '../buttons/editButton';

import CustomerDetailUr from './customerDetailUr';
import CustomerDetailFiz from './customerDetailFiz';

const CustomersDetailWithRouter = withRouter(props => <CustomerDetail {...props}/>);

class CustomerDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {sendResStatus: 'wait'};
    this.getData = this.getData.bind(this);
    this.delete = this.delete.bind(this);
  }
  componentDidMount() {
    store.dispatch({ type: 'CUSTOMERS_DETAIL_DATA', customersDetailData: {} });
    this.getData();
  }
  getData() {
    let path = '/api/customers/' + this.props.match.params.id;
    fetch(path, {method: 'POST'})
    .then(res => {
      if (res.status !== 200) {
          console.log('Err: ' + res.statusText + ' ' + res.status);
          return;
      }
      res.json().then((data) => {
        if (data) {
            console.log(data);
            store.dispatch({ type: 'CUSTOMERS_DETAIL_DATA', customersDetailData: data });
            let location = [
                { path: '/customers', name: 'Заказчики' },
                { path: this.props.location.pathname, name: data.data.name }
              ];
            let controls = {
                addBtn: {display: true, name: 'Добавить заказчика +', path: '/customers/add'},
                search: {display: true}
            };
            store.dispatch({ type: 'TOPBAR_CONTROLS', topbarControls: controls });
            store.dispatch({ type: 'NAVIGATION_PATHS', navigationPaths: location });
        }
      });
    });
  }
  delete(e) {
    e.preventDefault();
    let path = '/api/customers/' + this.props.match.params.id + '/delete';
    fetch(path, { method: 'POST' })
    .then(res => {
      if (res.status !== 200) return console.log('Err: ' + res.statusText + ' ' + res.status);
      res.json().then((data) => {
        console.log(data);
        if (data.status === 'ok') this.setState({sendResStatus: data.status});
      });
    })
  }
  render() {
    return (
      <Wrapper>
          {this.props.customers.customersDetailData.type === 'ur' ?
              <DataCont>
                  <CustomerDetailUr/>
                  <BtnsCont>
                      {this.props.navigation.navigationPaths.length === 2 ?
                          <BtnsCont>
                              <EditButton path={this.props.navigation.navigationPaths[1].path + '/edit'}/>
                              <DeleteButton callback={this.delete}/>
                          </BtnsCont>
                          : null}
                  </BtnsCont>
              </DataCont>
          :null}
          {this.props.customers.customersDetailData.type === 'fiz' ?
              <DataCont>
                  <CustomerDetailFiz/>
                  <BtnsCont>
                      {this.props.navigation.navigationPaths.length === 2 ?
                          <BtnsCont>
                              <EditButton path={this.props.navigation.navigationPaths[1].path + '/edit'}/>
                              <DeleteButton callback={this.delete}/>
                          </BtnsCont>
                          : null}
                  </BtnsCont>
              </DataCont>
          :null}
        {this.state.sendResStatus === 'ok' ? <Redirect to={'/customers'}/> : null}
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  padding: 4px;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`;
const DataCont = styled.div`
    padding: 10px;
    background-color: #ffffff;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const BtnsCont = styled.div`
    width: 100%;
    margin-top: 20px;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
`;

const mapStateToProps = (store) => {
  return {
    customers: store.customers,
    navigation: store.navigation,
  };
};

export default connect(mapStateToProps, null, null, { pure: false })(CustomersDetailWithRouter);
