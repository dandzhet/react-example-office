import React, { Component } from 'react';
import { connect } from "react-redux";
import styled from 'styled-components';

class CustomerDetailFiz extends Component {
    //constructor(props) {super(props)}
    render() {
        return (
            <Wrapper>
                <DataCont>
                    <Separator>Основная информация</Separator>
                    <ItemCont>
                        <ItemName>ФИО</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('name') && this.props.customers.customersDetailData.data.name ? this.props.customers.customersDetailData.data.name : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Почта</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('phone') && this.props.customers.customersDetailData.data.phone ? this.props.customers.customersDetailData.data.phone : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Почта</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('mail') && this.props.customers.customersDetailData.data.mail ? this.props.customers.customersDetailData.data.mail : '- - -'}</ItemValue>
                    </ItemCont>
                    <Separator style={{marginTop: '40px'}}>Дополнительная информация</Separator>
                    <ItemCont>
                        <ItemName>Примечания</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('other') && this.props.customers.customersDetailData.data.other ? this.props.customers.customersDetailData.data.other : '- - -'}</ItemValue>
                    </ItemCont>
                </DataCont>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
`;
const DataCont = styled.div`
    padding: 10px;
    background-color: #ffffff;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const Separator = styled.div`
    width: 100%;
    font-size: 16px;
    color: #212121;
    font-weight: 900;
    margin-bottom: 20px;
`;
const ItemCont = styled.div`
    width: 150px;
    margin-bottom: 20px;
`;
const ItemName = styled.div`
    font-size: 12px;
    color: #757575;
`;
const ItemValue = styled.div`
    font-size: 14px;
    color: #212121;
`;

const mapStateToProps = (store) => {
    return {
        customers: store.customers,
    };
};

export default connect(mapStateToProps)(CustomerDetailFiz);
