import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

class CustomersPm extends Component {
  //constructor(props) {super(props)}
  render() {
    return (
      <Wrapper exact={true} to={{ pathname: '/customers/' + this.props.id }}>
        <Title>{this.props.name}</Title>
      </Wrapper>
    );
  }
}

const Wrapper = styled(NavLink)`
    width: calc(100% - 40px);
    padding: 20px;
    cursor: pointer;
    text-decoration: none;
    &:hover {
        background-color: #eaeaea;
    }
`;
const Title = styled.div`
    font-size: 14px;
    color: #212121;
`;

export default CustomersPm;
