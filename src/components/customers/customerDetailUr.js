import React, { Component } from 'react';
import { connect } from "react-redux";
import styled from 'styled-components';

class CustomerDetailUr extends Component {
    //constructor(props) {super(props)}
    render() {
        return (
            <Wrapper>
                <DataCont>
                    <Separator>Основная информация</Separator>
                    <ItemCont>
                        <ItemName>Название</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('name') && this.props.customers.customersDetailData.data.name ? this.props.customers.customersDetailData.data.name : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Полное название</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('fullName') && this.props.customers.customersDetailData.data.fullName ? this.props.customers.customersDetailData.data.fullName : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Юридический адрес</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('urAddress') && this.props.customers.customersDetailData.data.urAddress ? this.props.customers.customersDetailData.data.urAddress : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Фактический адрес</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('factAddress') && this.props.customers.customersDetailData.data.factAddress ? this.props.customers.customersDetailData.data.factAddress : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Сайт</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('site') && this.props.customers.customersDetailData.data.site ? this.props.customers.customersDetailData.data.site : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Почта</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('mail') && this.props.customers.customersDetailData.data.mail ? this.props.customers.customersDetailData.data.mail : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>ОГРН</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('ogrn') && this.props.customers.customersDetailData.data.ogrn ? this.props.customers.customersDetailData.data.ogrn : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>ИНН</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('inn') && this.props.customers.customersDetailData.data.inn ? this.props.customers.customersDetailData.data.inn : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>КПП</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('kpp') && this.props.customers.customersDetailData.data.kpp ? this.props.customers.customersDetailData.data.kpp : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>ОКПО</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('okpo') && this.props.customers.customersDetailData.data.okpo ? this.props.customers.customersDetailData.data.okpo : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Р/с</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('rs') && this.props.customers.customersDetailData.data.rs ? this.props.customers.customersDetailData.data.rs : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Банк</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('bank') && this.props.customers.customersDetailData.data.bank ? this.props.customers.customersDetailData.data.bank : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>К/с</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('ks') && this.props.customers.customersDetailData.data.ks ? this.props.customers.customersDetailData.data.ks : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>БИК</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('bik') && this.props.customers.customersDetailData.data.bik ? this.props.customers.customersDetailData.data.bik : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>ФИО директора</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('directorName') && this.props.customers.customersDetailData.data.directorName ? this.props.customers.customersDetailData.data.directorName : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Примечания</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('other') && this.props.customers.customersDetailData.data.other ? this.props.customers.customersDetailData.data.other : '- - -'}</ItemValue>
                    </ItemCont>
                    <Separator style={{marginTop: '40px'}}>Контактное лицо</Separator>
                    <ItemCont>
                        <ItemName>ФИО</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('contact') && this.props.customers.customersDetailData.data.contact.hasOwnProperty('name') && this.props.customers.customersDetailData.data.contact.name ? this.props.customers.customersDetailData.data.contact.name : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Телефон</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('contact') && this.props.customers.customersDetailData.data.contact.hasOwnProperty('phone') && this.props.customers.customersDetailData.data.contact.phone ? this.props.customers.customersDetailData.data.contact.phone : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Почта</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('contact') && this.props.customers.customersDetailData.data.contact.hasOwnProperty('mail') && this.props.customers.customersDetailData.data.contact.mail ? this.props.customers.customersDetailData.data.contact.mail : '- - -'}</ItemValue>
                    </ItemCont>
                    <ItemCont>
                        <ItemName>Должность</ItemName>
                        <ItemValue>{this.props.customers.customersDetailData.data.hasOwnProperty('contact') && this.props.customers.customersDetailData.data.contact.hasOwnProperty('post') && this.props.customers.customersDetailData.data.contact.post ? this.props.customers.customersDetailData.data.contact.post : '- - -'}</ItemValue>
                    </ItemCont>
                </DataCont>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
`;
const DataCont = styled.div`
    padding: 10px;
    background-color: #ffffff;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const Separator = styled.div`
    width: 100%;
    font-size: 16px;
    color: #212121;
    font-weight: 900;
    margin-bottom: 20px;
`;
const ItemCont = styled.div`
    width: 150px;
    margin-bottom: 20px;
`;
const ItemName = styled.div`
    font-size: 12px;
    color: #757575;
`;
const ItemValue = styled.div`
    font-size: 14px;
    color: #212121;
`;

const mapStateToProps = (store) => {
    return {
        customers: store.customers,
    };
};

export default connect(mapStateToProps)(CustomerDetailUr);
