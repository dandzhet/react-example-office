import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../../reducers';
import {withRouter, Redirect} from 'react-router-dom';
import styled from 'styled-components';
import SaveButton from '../buttons/saveButton';
import CustomerAddObject from './customerAddObject';

const CustomersAddWithRouter = withRouter(props => <CustomerAdd {...props}/>);

class CustomerAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {type: 'ur'};
        this.customerTypeSelect = this.customerTypeSelect.bind(this);
        this.objectSend = this.objectSend.bind(this);
        this.objectPhotoSend = this.objectPhotoSend.bind(this);
        this.objectAddCustomerId = this.objectAddCustomerId.bind(this);
        this.customerSend = this.customerSend.bind(this);
        CustomerAdd.customertDataPreparation = CustomerAdd.customertDataPreparation.bind(this);
        this.send = this.send.bind(this);
        CustomerAdd.toNav = CustomerAdd.toNav.bind(this);
    }
    componentDidMount() {
        store.dispatch({type: 'CUSTOMERS_ADD_DATA', customersAddData: []});
        CustomerAdd.toNav();
    }
    customerTypeSelect(e) {this.setState({type: e.target.value})}
    objectSend(newObject, obj, callback) {
        fetch('/api/objects/add', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(newObject)})
            .then(resObjects => resObjects.json())
            .then(resObjectsJson => {
                obj.objects = [resObjectsJson.data._id];
                this.props.customers.customersAddObject.new.photos.length ? this.objectPhotoSend(resObjectsJson.data._id, obj, callback) : callback(obj);
            });
    }
    objectPhotoSend(id, obj, callback) {
        let photos = new FormData();
        for (let x = 0; x < this.props.customers.customersAddObject.new.photos.length; x++) photos.append('photo', this.props.customers.customersAddObject.new.photos[x]);
        photos.append('id', id);
        fetch('/api/objects/add/photo', {method: 'POST', body: photos})
            .then(res => res.json())
            .then(() => callback(obj));
    }
    objectAddCustomerId(objectId, customerId, callback) {
        let obj = {customers: [customerId]};
        fetch('/api/objects/' + objectId + '/update', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(obj)})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => callback(resJson));
    }
    customerSend(obj, callback) {
        fetch('/api/customers/add', {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json'}, body: JSON.stringify(obj)})
            .then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json())
            .then(resJson => callback(resJson))
    }
    static customertDataPreparation(e) {
        let obj = {};
        for (let i = 0; i < e.target.length - 1; i++) obj[e.target[i].name] = e.target[i].value;
        obj.objects = [];
        obj.contact = {name: obj.contactName, phone: obj.contactPhone, mail: obj.contactMail, post: obj.contactPost};
        delete obj.contactName;
        delete obj.contactPhone;
        delete obj.contactMail;
        delete obj.contactPost;
        return obj;
    }
    send(e) {
        e.preventDefault();
        store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: true});
        let obj = CustomerAdd.customertDataPreparation(e);

        // add object from database
        if (this.props.customers.customersAddObject.source === 'db') {
            obj.objects = [this.props.customers.customersAddObject.db];
            this.customerSend(obj, (customerData) => {
                this.objectAddCustomerId(this.props.customers.customersAddObject.db, customerData.data._id, () => {
                    store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false});
                    this.setState({sendResStatus: 'ok'})
                });
            });
        }

        // add new object
        else if (this.props.customers.customersAddObject.source === 'new') {
            let newObject = {address: this.props.customers.customersAddObject.new.address, other: this.props.customers.customersAddObject.new.other};
            this.objectSend(newObject, obj, this.customerSend);
        }

        // customer without object
        else this.customerSend(obj, () => {
                store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false});
                this.setState({sendResStatus: 'ok'})
            });
    }
    static toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/customers', name: 'Заказчики'}, {path: '/customers/add', name: 'Добавление'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: false}, search: {display: true}}});
    }
    render() {
        return (
            <Wrapper>
                <StyledForm onSubmit={this.send}>
                    <Separator>Основная информация</Separator>
                    <InputCont>
                        <InputLabel>Тип заказчика</InputLabel>
                        <select onChange={this.customerTypeSelect} value={this.state.type} style={{width: '200px'}} name='type'>
                            <option value='ur'>Юр. лицо</option>
                            <option value='fiz'>Физ. лицо</option>
                        </select>
                    </InputCont>

                    {this.state.type === 'ur' ?
                        <Block>
                            <InputCont>
                                <InputLabel>Название</InputLabel>
                                <StyledInput required name='name' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Полное название</InputLabel>
                                <StyledInput name='fullName' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Юр. адрес</InputLabel>
                                <StyledInput name='urAddress' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Фактич. адрес</InputLabel>
                                <StyledInput name='factAddress' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Сайт</InputLabel>
                                <StyledInput name='site' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>ОГРН</InputLabel>
                                <StyledInput name='ogrn' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>ИНН</InputLabel>
                                <StyledInput name='inn' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>КПП</InputLabel>
                                <StyledInput name='kpp' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>ОКПО</InputLabel>
                                <StyledInput name='okpo' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Р/с</InputLabel>
                                <StyledInput name='rs' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Банк</InputLabel>
                                <StyledInput name='bank' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>К/с</InputLabel>
                                <StyledInput name='ks' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>БИК</InputLabel>
                                <StyledInput name='bik' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>ФИО директора</InputLabel>
                                <StyledInput name='directorName' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Примечания</InputLabel>
                                <StyledInput style={{width: '272px'}} name='other' type='text'/>
                            </InputCont>
                            <Separator>Контактное лицо</Separator>
                            <InputCont>
                                <InputLabel>ФИО</InputLabel>
                                <StyledInput name='contactName' type='text'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Телефон</InputLabel>
                                <StyledInput name='contactPhone' type='tel'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Почта</InputLabel>
                                <StyledInput name='contactMail' type='email'/>
                            </InputCont>
                            <InputCont>
                                <InputLabel>Должность</InputLabel>
                                <StyledInput name='contactPost' type='text'/>
                            </InputCont>
                            <CustomerAddObject/>
                            <Separator><SaveButton/></Separator>
                        </Block>
                        : null
                    }

                    {
                        this.state.type === 'fiz' ?
                            <Block>
                                <InputCont>
                                    <InputLabel>ФИО</InputLabel>
                                    <StyledInput required name='name' type='text'/>
                                </InputCont>
                                <InputCont>
                                    <InputLabel>Телефон</InputLabel>
                                    <StyledInput name='phone' type='tel'/>
                                </InputCont>
                                <InputCont>
                                    <InputLabel>Почта</InputLabel>
                                    <StyledInput name='mail' type='email'/>
                                </InputCont>
                                <InputCont>
                                    <InputLabel>Примечания</InputLabel>
                                    <StyledInput style={{width: '272px'}} name='other' type='text'/>
                                </InputCont>
                                <CustomerAddObject/>
                                <Separator><SaveButton/></Separator>
                            </Block>
                            : null
                    }

                </StyledForm>
                {this.state.sendResStatus === 'ok' ? <Redirect to={'/customers'}/> : null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    padding: 4px;
    position: relative;
`;
const StyledForm = styled.form`
    padding: 10px;
    background-color: #ffffff;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const Block = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const StyledInput = styled.input`
    padding: 5px;
    border-radius: 5px;
    outline: none;
    border: 1px solid #eeeeee;
`;
const InputLabel = styled.div`
    font-size: 12px;
    color: #757575;
`;
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 16px;
`;

const mapStateToProps = store => ({customers: store.customers, dialog: store.dialog});
export default connect(mapStateToProps, null, null, {pure: false})(CustomersAddWithRouter);
