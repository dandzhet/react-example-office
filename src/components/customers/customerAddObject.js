import React, { Component } from 'react';
import { connect } from "react-redux";
import store from '../../reducers';
import styled from 'styled-components';
import CustomerAddObjectNew from './customerAddObjectNew';
import CustomerAddObjectDb from './customerAddObjectDb';

class CustomerAddObject extends Component {
    constructor(props) {
        super(props);
        this.sourceSelect = this.sourceSelect.bind(this);
    }
    sourceSelect(e) {
        let storeObject = this.props.customers.customersAddObject;
        storeObject.source = e.target.value;
        store.dispatch({type: 'CUSTOMERS_ADD_OBJECT', customersAddObject: storeObject});
    }
    render() {
        return (
            <Wrapper>
                <Separator>Объект</Separator>
                <InputCont>
                    <select style={{width: '137px', height: '25px', border: '1px solid #eeeeee', outline: 'none', backgroundColor: '#ffffff'}} value={this.props.customers.customersAddObject.source} onChange={this.sourceSelect} name='source'>
                        <option value='none'>Не добавлять</option>
                        <option value='new'>Новый</option>
                        <option value='db'>Из базы</option>
                    </select>
                </InputCont>
                {this.props.customers.customersAddObject.source === 'db' ? <CustomerAddObjectDb/> :null}
                {this.props.customers.customersAddObject.source === 'new' ? <CustomerAddObjectNew/> :null}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    width: 100%;
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
const InputCont = styled.div`
    margin-right: 10px;
    margin-bottom: 10px;
`;
const Separator = styled.div`
    width: 100%;
    padding: 10px 0;
    font-weight: 900;
    color: #212121;
    font-size: 16px;
`;

const mapStateToProps = store => ({customers: store.customers});
export default connect(mapStateToProps)(CustomerAddObject);
