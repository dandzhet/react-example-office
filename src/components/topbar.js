import React, {Component} from 'react';
import {connect} from "react-redux";
import {NavLink} from 'react-router-dom'
import styled from 'styled-components';
import TopbarNaigation from './topbar/topbarNavigation';

class Topbar extends Component {
    render() {
        return (
            <Wrapper>
                <TopbarNaigation/>
                <Controls>
                    {
                        this.props.topbar.topbarControls.addBtn.display ?
                            <AddBtn exact={true} to={{pathname: this.props.topbar.topbarControls.addBtn.path}}>
                                <i className="fas fa-plus" style={{color: '#ffffff', fontSize: '16px'}}/>
                            </AddBtn>
                            :
                            null
                    }
                    {
                        this.props.topbar.topbarControls.search.display ?
                            <SearchCont>
                                <input style={{border: 'none', outline: 'none', padding: '5px 10px', backgroundColor: 'transparent'}} type='text' placeholder='Поиск'/>
                                <i className="fas fa-search" style={{color: '#afafaf', fontSize: '12px', paddingRight: '10px', cursor: 'pointer'}}/>
                            </SearchCont>
                            :
                            null
                    }
                </Controls>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  position: fixed;
  z-index: 1;
  left: 184px;
  width: calc(100% - 188px);
  height: 32px;
  border-top: 4px solid #eeeeee;
  border-bottom: 4px solid #eeeeee;
  background-color: #ffffff;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const Controls = styled.div`
  height: 32px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
`;
const SearchCont = styled.div`
  height: 31px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const AddBtn = styled(NavLink)`
  margin-top: -5px;
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 34px;
  width: 14px;
  padding: 0 10px;
  background-color: #424242;
  border: 4px solid #eeeeee;
  border-radius: 50px;
  cursor: pointer;
  text-decoration: none;
  transition: .2s;
  &:hover {
    background-color: #757575;
    color: #eeeeee;
  }
`;

const mapStateToProps = store => ({sidebar: store.sidebar, topbar: store.topbar});
export default connect(mapStateToProps, null, null, {pure: false})(Topbar);
