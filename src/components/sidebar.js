import React, {Component} from 'react';
import {connect} from "react-redux";
import styled from 'styled-components';
import SidebarPm from './sidebar/sidebarPm';

class Sidebar extends Component {
    render() {
        return (
            <Wrapper>
                <LogoCont>
                    <LogoW>W</LogoW><LogoOrker>orker</LogoOrker>
                </LogoCont>
                <PmCont>
                    <SidebarPm link='/' name='Главная'/>
                    <SidebarPm link='/feed' name='Оповещения'/>
                    <SidebarPm link='/applications' name='Заявки'/>
                    {/*<SidebarPm link='/stages' name='Этапы'/>*/}
                    <SidebarPm link='/performers' name='Исполнители'/>
                    <SidebarPm link='/customers' name='Заказчики'/>
                    <SidebarPm link='/objects' name='Объекты'/>
                    {/*<SidebarPm link='/work' name='Виды работ'/>*/}
                    <SidebarPm link='/calc' name='Расчет'/>
                </PmCont>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  position: fixed;
  z-index: 2;
  width: 180px;
  height: 100vh;
  background-color: #424242;
  display: flex;
  flex-direction: column;
`;
const LogoCont = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  color: rgb(230,61,50);
`;
const LogoW = styled.div`
  font-size: 18px;
  font-weight: 900;
`;
const LogoOrker = styled.div`
  font-size: 14px;
`;
const PmCont = styled.div`
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const mapStateToProps = store => ({/*list: store.list,*/});
export default connect(mapStateToProps, null, null, {pure: false})(Sidebar);
