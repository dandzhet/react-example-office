import React, {Component} from 'react';
import {connect} from "react-redux";
import store from '../reducers';
import styled from 'styled-components';
import ApplicationsPm from './applications/applicationsPm';

class Applications extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.getData = this.getData.bind(this);
    }
    componentDidMount() {
        this.getData();
        Applications.toNav();
    }
    getData() {
        store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: true});
        fetch('/api/applications', {method: 'POST'}).then(res => res.status !== 200 ? console.log('Err: ' + res.statusText + ' ' + res.status) : res.json()).then(resJson => this.preparingDataForDisplay(resJson))
    }
    static getCustomerData(id) {return fetch('/api/customers/' + id, {method: 'POST'}).then(customerRes => customerRes.json()).then(customerResJson => customerResJson)}
    static getObjectData(id) {return fetch('/api/objects/' + id, {method: 'POST'}).then(objectRes => objectRes.json()).then(objectResJson => objectResJson)}
    preparingDataForDisplay(rawData) {
        console.log(rawData);
        let dataPromises = rawData.data.map(el => {
            if (el.startingStage === 'look') {
                return Promise.all([Applications.getCustomerData(el.stages.lookInit.customer), Applications.getObjectData(el.stages.lookInit.object)])
                    .then(res => ({
                        id: el._id,
                        customer: res[0],
                        object: res[1],
                        startingStage: el.startingStage,
                        currentStage: el.currentStage,
                        dateOfCreateon: el.dates.create,
                        updateDate: el.dates.update,
                    }))
            }
            else if (el.startingStage === 'exec') {
                return Promise.all([Applications.getCustomerData(el.stages.execInit.customer), Applications.getObjectData(el.stages.execInit.object)])
                    .then(res => ({
                        id: el._id,
                        customer: res[0],
                        object: res[1],
                        startingStage: el.startingStage,
                        currentStage: el.currentStage,
                        dateOfCreateon: el.dates.create,
                        updateDate: el.dates.update,
                    }))
            }
            return el;
        });
        Promise.all(dataPromises).then(results => {
            console.log(results);
            store.dispatch({type: 'DIALOG_ACTIVE', dialogActive: false});
            store.dispatch({type: 'APPLICATIONS_DATA', applicationsData: results});
        });
    }
    static toNav() {
        store.dispatch({type: 'NAVIGATION_PATHS', navigationPaths: [{path: '/applications', name: 'Заявки'}]});
        store.dispatch({type: 'TOPBAR_CONTROLS', topbarControls: {addBtn: {display: true, name: 'Добавить заявку +', path: '/applications/add'}, search: {display: true}}})
    }
    render() {
        return (
            <Wrapper>
                <Container>
                    {
                        this.props.applications.applicationsData && this.props.applications.applicationsData.length ?
                            <ColumnNamesContainer>
                                <ColumnNameItem>Заказчик</ColumnNameItem>
                                <ColumnNameItem>Объект</ColumnNameItem>
                                <ColumnNameItem>Этап</ColumnNameItem>
                                <ColumnNameItem>Дата создания</ColumnNameItem>
                                <ColumnNameItem>Дата изменения</ColumnNameItem>
                            </ColumnNamesContainer>
                            : null
                    }
                    {
                        this.props.applications.applicationsData && this.props.applications.applicationsData.length ?
                            this.props.applications.applicationsData.map((el, i) => <ApplicationsPm key={i} id={el.id} customer={el.customer.data.name} customerObject={el.object.data.address} dateOfCreateon={el.dateOfCreateon} updateDate={el.updateDate} currentStageName={el.currentStage}/>)
                            : <Empty>Пусто</Empty>
                    }
                </Container>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;
const Container = styled.div`
  width: calc(100% - 8px);
  margin-top: 4px;
  overflow: hidden;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  background-color: #ffffff;
`;
const ColumnNamesContainer = styled.div`
  width: calc(100% - 40px);
  border-bottom: 4px solid #eeeeee;
  padding: 20px;
  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const ColumnNameItem = styled.div`
  font-size: 10px;
  color: #757575;
  width: 20%;
`;
const Empty = styled.div`
  padding: 20px;
  color: #757575;
`;

const mapStateToProps = store => ({applications: store.applications});
export default connect(mapStateToProps)(Applications);
