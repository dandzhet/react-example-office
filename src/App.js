import React, {Component} from 'react';
import {connect} from "react-redux";
import styled from 'styled-components';

import Dialog from './components/dialog';
import Sidebar from './components/sidebar';
import Topbar from './components/topbar';
import Content from './components/content';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <Wrapper>
                <Dialog/>
                <Sidebar/>
                <Topbar/>
                <Content/>
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;

const mapStateToProps = (store) => {
    return {
        dialog: store.dialog
    };
};

export default connect(mapStateToProps, null, null, {pure: false})(App);