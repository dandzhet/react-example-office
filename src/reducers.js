import { createStore, combineReducers } from 'redux';

// ========================================================================================================================================================================================================
// ================================================== DIALOG ==============================================================================================================================================
// ========================================================================================================================================================================================================

const dialogReducer = (state = {dialogActive: false}, action) => {
    switch (action.type) {
        case 'DIALOG_ACTIVE':
            return Object.assign({}, state, {dialogActive: action.dialogActive});
        default:
            return state;
    }
};

// ========================================================================================================================================================================================================
// ================================================== SIDEBAR =============================================================================================================================================
// ========================================================================================================================================================================================================

const sidebarReducer = (state = {sidebarPmActive: 'Главная'}, action) => {
    switch (action.type) {
        case 'SIDEBAR_PM_ACTIVE':
            return Object.assign({}, state, {sidebarPmActive: action.sidebarPmActive});
        default:
            return state;
    }
};

// ========================================================================================================================================================================================================
// ================================================== TOPBAR ==============================================================================================================================================
// ========================================================================================================================================================================================================

let topbarReducerInit = {
    topbarControls: {
        addBtn: {
            display: false,
            name: '',
            path: '/',
        },
        search: {
            display: false,
        }
    }
};

const topbarReducer = (state = topbarReducerInit, action) => {
    switch (action.type) {
        case 'TOPBAR_CONTROLS':
            return Object.assign({}, state, {topbarControls: action.topbarControls});
        default:
            return state;
    }
};

// ========================================================================================================================================================================================================
// ================================================== NAVIGATION ==========================================================================================================================================
// ========================================================================================================================================================================================================

const navigationReducer = (state = {navigationPaths: []}, action) => {
    switch (action.type) {
        case 'NAVIGATION_PATHS':
            return Object.assign({}, state, {navigationPaths: action.navigationPaths});
        default:
            return state;
    }
};

// ========================================================================================================================================================================================================
// ================================================== OBJECTS =============================================================================================================================================
// ========================================================================================================================================================================================================

const objectsReducer = (state = {objectListData: [], objectDetailData: {}}, action) => {
    switch (action.type) {
        case 'OBJECT_LIST_DATA':
            return Object.assign({}, state, {objectListData: action.objectListData});
        case 'OBJECT_DETAIL_DATA':
            return Object.assign({}, state, {objectDetailData: action.objectDetailData});
        default:
            return state;
    }
};

// ========================================================================================================================================================================================================
// ================================================== WORK ================================================================================================================================================
// ========================================================================================================================================================================================================

const workReducer = (state = {workData: {}, workNamesData: {}, workDetailData: {}}, action) => {
    switch (action.type) {
        case 'WORK_DATA':
            return Object.assign({}, state, {workData: action.workData});
        case 'WORK_NAMES_DATA':
            return Object.assign({}, state, {workNamesData: action.workNamesData});
        case 'WORK_DETAIL_DATA':
            return Object.assign({}, state, {workDetailData: action.workDetailData});
        default:
            return state;
    }
};

// ========================================================================================================================================================================================================
// ================================================== APPLICATIONS ========================================================================================================================================
// ========================================================================================================================================================================================================

let applicationsReducerInitialState = {
    applicationsData: {},
    applicationsDetailData: {},
    applicationsAddType: '- - -',
    applicationsAddWorkInputData: [],
    applicationsAddWorkInputValue: [],
    applicationsAddCustomerInputData: [],
    applicationsAddCustomerInputValue: '',
    applicationsAddCustomerDetailData: {},
    applicationsAddPerformerInputData: [],
    applicationsAddPerformerInputValue: [],

    applicationsAddCustomer: {
        source: 'db',
        new: {
            type: 'ur',
        },
        db: ''
    },
    applicationsAddObject: {
        source: 'db',
        new: {},
        db: ''
    },
    applicationsAddLookWork: [],
    applicationsAddDetail: {
        startDate: '',
        customerCost: '',
        performerPay: '',
        other: ''
    },

    applicationsDetailToExecData: {
        startDate: '',
        customerCost: '0',
        performerPay: '0',
        other: ''
    }
};

const applicationsReducer = (state = applicationsReducerInitialState, action) => {
    switch (action.type) {
        case 'APPLICATIONS_DATA':
            return Object.assign({}, state, {applicationsData: action.applicationsData});
        case 'APPLICATIONS_DETAIL_DATA':
            return Object.assign({}, state, {applicationsDetailData: action.applicationsDetailData});
        case 'APPLICATIONS_ADD_TYPE':
            return Object.assign({}, state, {applicationsAddType: action.applicationsAddType});
        case 'APPLICATIONS_ADD_WORK_INPUT_DATA':
            return Object.assign({}, state, {applicationsAddWorkInputData: action.applicationsAddWorkInputData});
        case 'APPLICATIONS_ADD_WORK_INPUT_VALUE':
            return Object.assign({}, state, {applicationsAddWorkInputValue: action.applicationsAddWorkInputValue});
        case 'APPLICATIONS_ADD_CUSTOMER_INPUT_DATA':
            return Object.assign({}, state, {applicationsAddCustomerInputData: action.applicationsAddCustomerInputData});
        case 'APPLICATIONS_ADD_CUSTOMER_INPUT_VALUE':
            return Object.assign({}, state, {applicationsAddCustomerInputValue: action.applicationsAddCustomerInputValue});
        case 'APPLICATIONS_ADD_CUSTOMER_DETAIL_DATA':
            return Object.assign({}, state, {applicationsAddCustomerDetailData: action.applicationsAddCustomerDetailData});
        case 'APPLICATIONS_ADD_PERFORMER_INPUT_DATA':
            return Object.assign({}, state, {applicationsAddPerformerInputData: action.applicationsAddPerformerInputData});
        case 'APPLICATIONS_ADD_PERFORMER_INPUT_VALUE':
            return Object.assign({}, state, {applicationsAddPerformerInputValue: action.applicationsAddPerformerInputValue});

        case 'APPLICATIONS_ADD_CUSTOMER':
            return Object.assign({}, state, {applicationsAddCustomer: action.applicationsAddCustomer});
        case 'APPLICATIONS_ADD_OBJECT':
            return Object.assign({}, state, {applicationsAddObject: action.applicationsAddObject});
        case 'APPLICATIONS_ADD_LOOK_WORK':
            return Object.assign({}, state, {applicationsAddLookWork: action.applicationsAddLookWork});
        case 'APPLICATIONS_ADD_EXEC_WORK':
            return Object.assign({}, state, {applicationsAddExecWork: action.applicationsAddExecWork});
        case 'APPLICATIONS_ADD_DETAIL':
            return Object.assign({}, state, {applicationsAddDetail: action.applicationsAddDetail});

        case 'APPLICATIONS_DETAIL_TO_EXEC_DATA':
            return Object.assign({}, state, {applicationsDetailToExecData: action.applicationsDetailToExecData});

        default:
            return state;
    }
};

// ========================================================================================================================================================================================================
// ================================================== PERFORMERS ==========================================================================================================================================
// ========================================================================================================================================================================================================

const performersReducer = (state = {performersData: {}, performersDetailData: {}, performersAddWorkInputData: [], performersAddWorkInputValue: []}, action) => {
    switch (action.type) {
        case 'PERFORMERS_DATA':
            return Object.assign({}, state, {performersData: action.performersData});
        case 'PERFORMERS_DETAIL_DATA':
            return Object.assign({}, state, {performersDetailData: action.performersDetailData});
        case 'PERFORMERS_ADD_WORK_INPUT_DATA':
            return Object.assign({}, state, {performersAddWorkInputData: action.performersAddWorkInputData});
        case 'PERFORMERS_ADD_WORK_INPUT_VALUE':
            return Object.assign({}, state, {performersAddWorkInputValue: action.performersAddWorkInputValue});
        default:
            return state;
    }
};

// ========================================================================================================================================================================================================
// ================================================== CUSTOMERS ===========================================================================================================================================
// ========================================================================================================================================================================================================

let customersReducerInitialState = {
    customersData: {},
    customersDetailData: {type: '', data: {}},
    customersAddData: [],
    customersAddWork: [],

    customersAddObject: {
        source: 'none',
        new: {
            address: '',
            photos: [],
            other: '',
        },
        db: '',
    },
};

const customersReducer = (state = customersReducerInitialState, action) => {
    switch (action.type) {
        case 'CUSTOMERS_DATA':
            return Object.assign({}, state, {customersData: action.customersData});
        case 'CUSTOMERS_DETAIL_DATA':
            return Object.assign({}, state, {customersDetailData: action.customersDetailData});
        case 'CUSTOMERS_ADD_DATA':
            return Object.assign({}, state, {customersAddData: action.customersAddData});
        case 'CUSTOMERS_ADD_WORK':
            return Object.assign({}, state, {customersAddWork: action.customersAddWork});

        case 'CUSTOMERS_ADD_OBJECT':
            return Object.assign({}, state, {customersAddObject: action.customersAddObject});

        default:
            return state;
    }
};

// ========================================================================================================================================================================================================
// ================================================== COMBINE & EXPORT ====================================================================================================================================
// ========================================================================================================================================================================================================

const reducers = combineReducers({
    dialog: dialogReducer,
    sidebar: sidebarReducer,
    topbar: topbarReducer,
    navigation: navigationReducer,
    objects: objectsReducer,
    work: workReducer,
    applications: applicationsReducer,
    performers: performersReducer,
    customers: customersReducer,
});
const store = createStore(reducers);
export default store;